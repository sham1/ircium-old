/* ircium-user.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-user.h"

struct _IrciumUser
{
	GObject parent_instance;

	IrciumSession *session;

	gchar *nick;
};

G_DEFINE_TYPE (IrciumUser, ircium_user, G_TYPE_OBJECT)

enum {
	PROP_0,

	PROP_SESSION,
	PROP_NICK,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumUser *
ircium_user_new (IrciumSession *session,
                 const gchar   *nick)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	g_return_val_if_fail (nick != NULL, NULL);

	return g_object_new (IRCIUM_TYPE_USER,
			     "session", session,
			     "nick", nick,
			     NULL);
}

static void
ircium_user_dispose (GObject *object)
{
	IrciumUser *self = (IrciumUser *)object;

	g_clear_object (&self->session);

	G_OBJECT_CLASS (ircium_user_parent_class)->dispose (object);
}

static void
ircium_user_finalize (GObject *object)
{
	IrciumUser *self = (IrciumUser *)object;

	g_clear_pointer (&self->nick, g_free);

	G_OBJECT_CLASS (ircium_user_parent_class)->finalize (object);
}

static void
ircium_user_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
	IrciumUser *self = IRCIUM_USER (object);

	switch (prop_id)
	{
	case PROP_SESSION:
		g_value_set_object (value, self->session);
		break;
	case PROP_NICK:
		g_value_set_string (value, self->nick);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_user_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
	IrciumUser *self = IRCIUM_USER (object);

	switch (prop_id)
	{
	case PROP_SESSION:
		g_clear_object (&self->session);
		self->session = g_value_dup_object (value);
		break;
	case PROP_NICK:
		g_clear_pointer (&self->nick, g_free);
		self->nick = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_user_class_init (IrciumUserClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = ircium_user_dispose;
	object_class->finalize = ircium_user_finalize;
	object_class->get_property = ircium_user_get_property;
	object_class->set_property = ircium_user_set_property;

	properties[PROP_SESSION] =
		g_param_spec_object ("session",
				     "session",
				     "The session",
				     IRCIUM_TYPE_SESSION,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_NICK] =
		g_param_spec_string ("nick",
				     "nick",
				     "The nick",
				     NULL,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_user_init (IrciumUser *self)
{
}

const gchar *
ircium_user_get_nick (IrciumUser *user)
{
	g_return_val_if_fail (IRCIUM_IS_USER (user), NULL);
	return user->nick;
}

void
ircium_user_set_nick (IrciumUser  *user,
                      const gchar *nick)
{
	g_return_if_fail (IRCIUM_IS_USER (user));
	g_return_if_fail (nick != NULL);

	g_clear_pointer (&user->nick, g_free);
	user->nick = g_strdup (nick);
	g_object_notify_by_pspec (G_OBJECT (user), properties[PROP_NICK]);
}
