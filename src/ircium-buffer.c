/* ircium-buffer.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-buffer-private.h"
#include "ircium-message.h"

G_DEFINE_ABSTRACT_TYPE (IrciumBuffer, ircium_buffer, G_TYPE_OBJECT)

static GListModel *ircium_buffer_get_contents_real (IrciumBuffer *buffer);
static gchar *ircium_buffer_get_title_real (IrciumBuffer *buffer);
static void ircium_buffer_add_line_real (IrciumBuffer     *buffer,
                                         IrciumBufferLine *line);
static void ircium_buffer_handle_entry_real (IrciumBuffer *buffer,
                                             gchar        *entry_text);
static void ircium_buffer_set_session_real (IrciumBuffer *buffer,
                                            gpointer      session);
static gboolean ircium_buffer_has_topic_real (IrciumBuffer *buffer);
static gchar *ircium_buffer_get_topic_real (IrciumBuffer *buffer);
static void ircium_buffer_set_topic_real (IrciumBuffer *buffer,
                                          gchar        *topic);
static void ircium_buffer_set_topic_metadata_real (IrciumBuffer *buffer,
                                                   const gchar  *nick,
                                                   GDateTime    *time);

static void
ircium_buffer_class_init (IrciumBufferClass *klass)
{
	klass->get_contents = ircium_buffer_get_contents_real;
	klass->get_title = ircium_buffer_get_title_real;
	klass->add_line = ircium_buffer_add_line_real;
	klass->handle_entry = ircium_buffer_handle_entry_real;
	klass->set_session = ircium_buffer_set_session_real;
	klass->has_topic = ircium_buffer_has_topic_real;
	klass->get_topic = ircium_buffer_get_topic_real;
	klass->set_topic = ircium_buffer_set_topic_real;
	klass->set_topic_metadata = ircium_buffer_set_topic_metadata_real;
}

static void
ircium_buffer_init (IrciumBuffer *self)
{
}

/**
 * ircium_buffer_get_title:
 *
 * @buffer: The buffer
 *
 * Returns: (transfer none): The #GListModel containing the buffer's contents
 */
GListModel *
ircium_buffer_get_contents (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER (buffer), NULL);
	return IRCIUM_BUFFER_GET_CLASS (buffer)->get_contents (buffer);
}

/**
 * ircium_buffer_get_title:
 *
 * @buffer: The buffer
 *
 * Returns: (transfer none): The user-visible title of this buffer
 */
gchar *
ircium_buffer_get_title (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER (buffer), NULL);
	return IRCIUM_BUFFER_GET_CLASS (buffer)->get_title (buffer);
}

/**
 * ircium_buffer_add_line:
 *
 * @buffer: The buffer
 * @line: (not nullable): The #IrciumBufferLine you want to add to the buffer
 */
void
ircium_buffer_add_line (IrciumBuffer     *buffer,
                        IrciumBufferLine *line)
{
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	g_return_if_fail (IRCIUM_IS_BUFFER_LINE (line));
	IRCIUM_BUFFER_GET_CLASS (buffer)->add_line (buffer, line);
}

void
ircium_buffer_handle_entry (IrciumBuffer *buffer,
                            gchar        *entry_text)
{
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	g_return_if_fail (entry_text != NULL);
	IRCIUM_BUFFER_GET_CLASS (buffer)->handle_entry (buffer, entry_text);
}

void
ircium_buffer_set_session (IrciumBuffer  *buffer,
                           IrciumSession *session)
{
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	g_return_if_fail (IRCIUM_IS_SESSION (session));
	IRCIUM_BUFFER_GET_CLASS (buffer)->set_session (buffer, session);
}

gboolean
ircium_buffer_has_topic (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER (buffer), FALSE);
	return IRCIUM_BUFFER_GET_CLASS (buffer)->has_topic (buffer);
}

gchar *
ircium_buffer_get_topic (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER (buffer), NULL);
	return IRCIUM_BUFFER_GET_CLASS (buffer)->get_topic (buffer);
}

void
ircium_buffer_set_topic (IrciumBuffer *buffer,
                         gchar        *topic)
{
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	g_return_if_fail (topic != NULL);
	IRCIUM_BUFFER_GET_CLASS (buffer)->set_topic (buffer, topic);
}

static void
ircium_buffer_add_line_real (IrciumBuffer     *buffer,
                             IrciumBufferLine *line)
{
	g_critical ("A subclass of IrciumBuffer should implement add_line");
}

static GListModel *
ircium_buffer_get_contents_real (IrciumBuffer *buffer)
{
	g_critical ("A subclass of IrciumBuffer should implement get_contents");
	return NULL;
}

static gchar *
ircium_buffer_get_title_real (IrciumBuffer *buffer)
{
	g_critical ("A subclass of IrciumBuffer should implement get_title");
	return NULL;
}

static void
ircium_buffer_handle_entry_real (IrciumBuffer *buffer,
                                 gchar        *entry_text)
{
	g_critical ("A subclass of IrciumBuffer should implement handle_entry");
}

static void
ircium_buffer_set_session_real (IrciumBuffer *buffer,
                                gpointer      session)
{
	g_critical ("A subclass of IrciumBuffer should implement set_session");
}

static gboolean
ircium_buffer_has_topic_real (IrciumBuffer *buffer)
{
	g_critical ("A subclass of IrciumBuffer should implement has_topic");
	return FALSE;
}

static gchar *
ircium_buffer_get_topic_real (IrciumBuffer *buffer)
{
	g_critical ("A subclass of IrciumBuffer should implement get_topic");
	return NULL;
}

static void
ircium_buffer_set_topic_real (IrciumBuffer *buffer,
                              gchar        *topic)
{
	g_critical ("A subclass of IrciumBuffer should implement set_topic");
}

static void
ircium_buffer_set_topic_metadata_real (IrciumBuffer *buffer,
                                       const gchar  *nick,
                                       GDateTime    *time)
{
	g_critical ("A subclass of IrciumBuffer "
		    "should implement set_topic_metadata");
}

gboolean
ircium_buffer_is_input_line_command (const gchar *line)
{
	if (*line != '/') return FALSE;
	line++;
	if (*line == '/' || *line == ' ') return FALSE;
	return TRUE;
}

const gchar **
ircium_buffer_parse_input_line_command (const gchar *line)
{
	if (!ircium_buffer_is_input_line_command (line)) return NULL;

	const gchar *cmd = ++line;
	for (; *line != ' '; ++line);
	cmd = get_string_between ((guint8 *) cmd,
				  (guint8 *) line++);

	while (*line == ' ') ++line;

	const gchar *args = NULL;
	if (*line != '\0') {
		args = line;
		while (*line++ != '\0');
		args = get_string_between ((guint8 *) args,
					   (guint8 *) line);
	}

	const gchar **ret = g_malloc0_n (args != NULL ? 3 : 2, sizeof (*ret));
	ret[0] = cmd;
	ret[1] = args;
	return ret;
}

gboolean
ircium_buffer_common_command_executor (const gchar   **command,
                                       IrciumSession  *session)
{
	gboolean success = FALSE;
	gchar *cmd = g_utf8_strdown (command[0], -1);
	const gchar *args = command[1];

	if (g_strcmp0 (cmd, "raw") == 0) {
		gchar *msg = g_strdup_printf ("%s\r\n", args);
		gsize msg_len = strlen (msg);

		GBytes *msg_bytes = g_bytes_new_take (msg, msg_len);
		GByteArray *msg_arr = g_bytes_unref_to_array (msg_bytes);

		IrciumMessage *serialized_msg = ircium_message_parse (msg_arr,
								      TRUE);

		ircium_session_send_message (session, serialized_msg);

		g_byte_array_unref (msg_arr);
		success = TRUE;
		goto out;
	}

	if (g_strcmp0 (cmd, "join") == 0) {
		success = TRUE;
		if (args == NULL) {
			// TODO: Show user hints about using /join
			goto out;
		}

		gchar **arguments = g_strsplit (args, " ", -1);

		gsize args_count = 0;
		for (gsize i = 0; i < g_strv_length (arguments); ++i) {
			gchar *arg = arguments[i];
			if (*arg) ++args_count;
		}

		if (args_count < 1 || args_count > 2) {
			// TODO: Show user hints about using /join
			goto out;
		}

		gchar *channel = NULL;
		gchar *key = NULL;

		for (gsize i = 0; i < g_strv_length (arguments); ++i) {
			gchar *arg = arguments[i];
			if (*arg) {
				if (channel == NULL) {
					channel = arg;
					continue;
				}
				if (key == NULL) {
					key = arg;
					continue;
				}
			}
		}

		g_autoptr (GPtrArray) params = NULL;
		params = g_ptr_array_new_with_free_func (g_free);

		g_ptr_array_add (params, g_strdup (channel));
		if (key) g_ptr_array_add (params, g_strdup (key));

		IrciumMessage *msg = ircium_message_new (NULL,
							 NULL,
							 "JOIN",
							 params);

		ircium_session_send_message (session, msg);

		g_strfreev (arguments);
		goto out;
	}

	if (g_strcmp0 (cmd, "msg") == 0) {
		success = TRUE;
		if (args == NULL) {
			// TODO: Hint the user about the usage of /msg
			goto out;
		}

		g_auto (GStrv) arguments = g_strsplit (args, " ", 2);
		if (g_strv_length (arguments) < 2) {
			// TODO: Hint the user about the usage of /msg
			goto out;
		}
		gchar *dest = g_strdup (arguments[0]);

		gchar *text = NULL;
		gchar *text_head = arguments[1];
		while (*text_head == ' ') {
			++text_head;
		}

		if (*text_head == '\0') {
			// TODO: Hint the user about the usage of /msg
			g_free (dest);
			goto out;
		}
		text = g_strdup (text_head);

		GPtrArray *params = g_ptr_array_new_with_free_func (g_free);
		g_ptr_array_add (params, dest);
		g_ptr_array_add (params, text);

		IrciumMessage *msg = ircium_message_new (NULL,
							 NULL,
							 "PRIVMSG",
							 params);
		ircium_session_send_message (session, msg);

		g_ptr_array_unref (params);

		goto out;
	}

out:
	g_free (cmd);
	return success;
}

void
ircium_buffer_collapse_slash (gchar **msg)
{
	gchar *str = *msg;
	if (*str == '/' && *(str + 1) == '/') {
		gsize len = strlen (str);
		gchar *new_str = g_malloc0_n (len, sizeof(*new_str));
		new_str[0] = '/';
		memcpy (new_str + 1, str + 2, len - 2);
		g_free (str);
		*msg = new_str;
	}
}

void
ircium_buffer_set_topic_metadata (IrciumBuffer *buffer,
                                  const gchar  *nick,
                                  GDateTime    *time)
{
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	g_return_if_fail (nick != NULL);
	g_return_if_fail (time != NULL);
	IRCIUM_BUFFER_GET_CLASS (buffer)->set_topic_metadata (buffer,
							      nick,
							      time);

}
