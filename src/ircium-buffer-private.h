/* ircium-buffer-private.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "ircium-buffer.h"
#include "ircium-session.h"

G_BEGIN_DECLS

gboolean ircium_buffer_is_input_line_command (const gchar *line);
const gchar **ircium_buffer_parse_input_line_command (const gchar *line);
gboolean ircium_buffer_common_command_executor (const gchar   **command,
                                                IrciumSession  *session);

void ircium_buffer_collapse_slash (gchar **msg);

void ircium_buffer_set_session (IrciumBuffer  *buffer,
                                IrciumSession *session);

G_END_DECLS
