/* ircium-network-settings-panel.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include "ircium-window.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_NETWORK_SETTINGS_PANEL \
	(ircium_network_settings_panel_get_type())

G_DECLARE_FINAL_TYPE (IrciumNetworkSettingsPanel,
		      ircium_network_settings_panel,
		      IRCIUM, NETWORK_SETTINGS_PANEL,
		      GtkBox)

IrciumNetworkSettingsPanel *ircium_network_settings_panel_new (void);

void ircium_network_settings_panel_set_window (IrciumNetworkSettingsPanel *self,
                                               IrciumWindow               *win);

G_END_DECLS
