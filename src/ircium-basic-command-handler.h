/* ircium-basic-command-handler.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "ircium-command-handler.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_BASIC_COMMAND_HANDLER \
	(ircium_basic_command_handler_get_type())

G_DECLARE_FINAL_TYPE (IrciumBasicCommandHandler,
		      ircium_basic_command_handler,
		      IRCIUM, BASIC_COMMAND_HANDLER,
		      IrciumCommandHandler)

IrciumBasicCommandHandler *ircium_basic_command_handler_new (void);

G_END_DECLS
