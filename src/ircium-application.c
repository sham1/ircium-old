/* ircium-application.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-application.h"
#include "ircium-window.h"
#include <uuid.h>

struct _IrciumApplication
{
	GtkApplication parent_instance;

	GSettings *settings;
	GListStore *accounts_list;
	GPtrArray *account_ids;
};

G_DEFINE_TYPE (IrciumApplication,
	       ircium_application,
	       GTK_TYPE_APPLICATION)

IrciumApplication *
ircium_application_new (void)
{
	return g_object_new (IRCIUM_TYPE_APPLICATION,
			     "application-id",
			     "com.gitlab.sham1.Ircium",
			     NULL);
}

static void
ircium_application_activate (GApplication *app)
{
	IrciumApplication *self = (IrciumApplication *) app;
	IrciumWindow *win = (IrciumWindow *)
		ircium_window_new (GTK_APPLICATION (self));

	gtk_widget_show_all (GTK_WIDGET (win));
}

static void
ircium_application_dispose (GObject *object)
{
	IrciumApplication *self = (IrciumApplication *) object;

	g_clear_object (&self->settings);
	g_clear_object (&self->accounts_list);
	g_clear_pointer (&self->account_ids, g_ptr_array_unref);

	G_OBJECT_CLASS (ircium_application_parent_class)->dispose (object);
}

static void
ircium_application_class_init (IrciumApplicationClass *klass)
{
	GApplicationClass *application_class = (GApplicationClass *) klass;

	application_class->activate = ircium_application_activate;

	GObjectClass *object_class = (GObjectClass *) klass;

	object_class->dispose = ircium_application_dispose;
}

static void
ircium_application_init (IrciumApplication *self)
{
	self->settings = g_settings_new ("com.gitlab.sham1.Ircium");
	self->accounts_list = g_list_store_new (G_TYPE_SETTINGS);
	self->account_ids = g_ptr_array_new_with_free_func (g_free);

	gchar **accounts = g_settings_get_strv (self->settings, "accounts");
	for (gsize i = 0; i < g_strv_length (accounts); ++i) {
		gchar *account_id = accounts[i];
		g_ptr_array_add (self->account_ids, g_strdup (account_id));
		gchar *account_path =
			g_strdup_printf (
				"/com/gitlab/sham1/Ircium/Accounts/%s/",
					 account_id);
		GSettings *account_settings =
			g_settings_new_with_path
				("com.gitlab.sham1.Ircium.Account",
				 account_path);

		g_list_store_append (self->accounts_list, account_settings);

		g_free (account_path);
		g_object_unref (account_settings);
	}

	g_strfreev (accounts);
}

static gchar *
gen_account_uuid (void);

static gchar **
ptr_arr_to_strv (GPtrArray *ptr_arr);

GSettings *
ircium_application_create_new_account (IrciumApplication *app)
{
	g_return_val_if_fail (IRCIUM_IS_APPLICATION (app), NULL);

	gchar *account_uuid = gen_account_uuid ();
	gchar *account_path =
		g_strdup_printf ("/com/gitlab/sham1/Ircium/Accounts/%s/",
				 account_uuid);

	GSettings *new_setting = g_settings_new_with_path
		("com.gitlab.sham1.Ircium.Account",
		 account_path);
	g_object_set_data_full (G_OBJECT (new_setting),
				"uuid",
				g_strdup (account_uuid),
				g_free);

	g_ptr_array_add (app->account_ids, account_uuid);
	gchar **accounts_arr = ptr_arr_to_strv (app->account_ids);

	g_settings_set_strv (app->settings,
			     "accounts",
			     (const gchar **) accounts_arr);
	g_list_store_append (app->accounts_list, new_setting);

	g_object_unref (new_setting);

	g_free (account_path);
	g_strfreev (accounts_arr);

	return new_setting;
}

GListModel *
ircium_application_get_account_list (IrciumApplication *app)
{
	g_return_val_if_fail (IRCIUM_IS_APPLICATION (app), NULL);

	return G_LIST_MODEL (app->accounts_list);
}

static gchar *
gen_account_uuid (void)
{
	uuid_t u;
	gchar name[37];

	uuid_generate (u);
	uuid_unparse (u, name);

	return g_strdup (name);
}

static gchar **
ptr_arr_to_strv (GPtrArray *ptr_arr)
{
	gsize count = ptr_arr->len + 1;
	gchar **ret = g_malloc0_n (count, sizeof (*ret));

	for (gsize i = 0; i < ptr_arr->len; ++i) {
		gchar *str = ptr_arr->pdata[i];
		ret[i] = g_strdup (str);
	}

	return ret;
}

void
ircium_application_remove_account (IrciumApplication *app,
                                   GSettings         *account)
{
	gssize i = -1;
	GListModel *model = G_LIST_MODEL (app->accounts_list);
	for (gsize j = 0; j < g_list_model_get_n_items (model); ++j) {
		GSettings *curr_settings = g_list_model_get_item (model, j);
		if (account == curr_settings) {
			i = j;
			break;
		}
	}

	if (i != -1) {
		g_list_store_remove (app->accounts_list, i);
		g_ptr_array_remove_index (app->account_ids, i);

		gchar **accounts_arr = ptr_arr_to_strv (app->account_ids);

		g_settings_set_strv (app->settings,
				     "accounts",
				     (const gchar **) accounts_arr);

		g_strfreev (accounts_arr);

		g_settings_reset (account, "uuid");
		g_settings_reset (account, "visual-name");
		g_settings_reset (account, "server-address");
		g_settings_reset (account, "server-port");
		g_settings_reset (account, "server-tls");
		g_settings_reset (account, "nickname");
		g_settings_reset (account, "username");
		g_settings_reset (account, "realname");
	}
}
