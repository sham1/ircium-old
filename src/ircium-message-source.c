/* ircium-message-source.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-message-source.h"
#include "ircium-message.h"

struct _IrciumMessageSource
{
	GObject parent_instance;

	// FALSE if it's a server, TRUE if a client
	// (i.e. would match mask "*!*@*"), or a service.
	gboolean is_user;

	// <nick>!<user>@<host>
	gchar *nick;
	gchar *user;
	gchar *host;

	// <server hostname>
	gchar *server_host;
};

G_DEFINE_TYPE (IrciumMessageSource, ircium_message_source, G_TYPE_OBJECT)

static gboolean
is_user (const gchar *source)
{
	// Not the best heuristic in the world, but it'll suffice.
	gboolean has_bang = FALSE;
	gboolean has_at = FALSE;
	for (const gchar *iter = source; *iter != '\0'; ++iter) {
		const gchar c = *iter;
		if (c == '!') {
			// The '!' only is in user masks, but y'know.
			// Just to be safe.
			if (!has_bang) {
				has_bang = TRUE;
			} else {
				return FALSE;
			}
		}
		if (c == '@') {
			// This ensures that '@' comes after '!'.
			if (has_bang && !has_at) {
				has_at = TRUE;
			} else {
				return FALSE;
			}
		}
	}
	return has_bang && has_at;
}

static void parse_user (IrciumMessageSource *self,
                        const gchar         *source);

IrciumMessageSource *
ircium_message_source_new (const gchar *source)
{
	if (source == NULL || *source == '\0') return NULL;
	IrciumMessageSource *self =
		g_object_new (IRCIUM_TYPE_MESSAGE_SOURCE, NULL);
	self->is_user = is_user (source);
	if (self->is_user) {
		parse_user (self, source);
	} else {
		self->server_host = g_strdup (source);
	}
	return self;
}

static void
ircium_message_source_finalize (GObject *object)
{
	IrciumMessageSource *self = (IrciumMessageSource *)object;

	g_clear_pointer (&self->nick, g_free);
	g_clear_pointer (&self->user, g_free);
	g_clear_pointer (&self->host, g_free);

	g_clear_pointer (&self->server_host, g_free);

	G_OBJECT_CLASS (ircium_message_source_parent_class)->finalize (object);
}

static void
ircium_message_source_class_init (IrciumMessageSourceClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ircium_message_source_finalize;
}

static void
ircium_message_source_init (IrciumMessageSource *self)
{
}

gboolean
ircium_message_source_is_user (const IrciumMessageSource *self)
{
	return self->is_user;
}

const gchar *
ircium_message_source_get_nick (const IrciumMessageSource *self)
{
	return self->nick;
}

const gchar *
ircium_message_source_get_user (const IrciumMessageSource *self)
{
	return self->user;
}

const gchar *
ircium_message_source_get_host (const IrciumMessageSource *self)
{
	return self->host;
}

const gchar *
ircium_message_source_get_server_host (const IrciumMessageSource *self)
{
	return self->server_host;
}

static void
parse_user (IrciumMessageSource *self,
            const gchar         *source)
{
	const gchar *head = source;
	const gchar *iter = source;
	for (; *iter != '\0'; ++iter) {
		const gchar c = *iter;
		if (c == '!') {
			self->nick = get_string_between ((const guint8 *) head,
							 (const guint8 *) iter);
			head = iter + 1;
		}
		if (c == '@') {
			self->user = get_string_between ((const guint8 *) head,
							 (const guint8 *) iter);
			head = iter + 1;
		}
	}
	self->host = get_string_between ((const guint8 *) head,
					 (const guint8 *) iter);
}
