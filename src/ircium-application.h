/* ircium-application.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define IRCIUM_TYPE_APPLICATION (ircium_application_get_type())

G_DECLARE_FINAL_TYPE (IrciumApplication,
		      ircium_application,
		      IRCIUM, APPLICATION,
		      GtkApplication)

IrciumApplication *ircium_application_new (void);

GSettings *ircium_application_create_new_account (IrciumApplication *app);
GListModel *ircium_application_get_account_list (IrciumApplication *app);
void ircium_application_remove_account (IrciumApplication *app,
                                        GSettings         *account);

G_END_DECLS
