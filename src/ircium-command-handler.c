/* ircium-command-handler.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-command-handler.h"

G_DEFINE_ABSTRACT_TYPE (IrciumCommandHandler,
			ircium_command_handler,
			G_TYPE_OBJECT)

static void
ircium_command_handler_register_handlers_real (IrciumCommandHandler *handler,
					       IrciumCommandReactor *reactor);

static void
ircium_command_handler_unregister_handlers_real (IrciumCommandHandler *handler,
                                                 IrciumCommandReactor *reactor);

static void
ircium_command_handler_class_init (IrciumCommandHandlerClass *klass)
{
	klass->register_handlers =
		ircium_command_handler_register_handlers_real;
	klass->unregister_handlers =
		ircium_command_handler_unregister_handlers_real;
}

void
ircium_command_handler_register_handlers (IrciumCommandHandler *handler,
                                          IrciumCommandReactor *reactor)
{
	g_return_if_fail (IRCIUM_IS_COMMAND_HANDLER (handler));
	g_return_if_fail (IRCIUM_IS_COMMAND_REACTOR (reactor));

	IRCIUM_COMMAND_HANDLER_GET_CLASS (handler)->
		register_handlers (handler, reactor);
}

void
ircium_command_handler_unregister_handlers (IrciumCommandHandler *handler,
                                            IrciumCommandReactor *reactor)
{
	g_return_if_fail (IRCIUM_IS_COMMAND_HANDLER (handler));
	g_return_if_fail (IRCIUM_IS_COMMAND_REACTOR (reactor));

	IRCIUM_COMMAND_HANDLER_GET_CLASS (handler)->
		unregister_handlers (handler, reactor);
}

static void
ircium_command_handler_register_handlers_real (IrciumCommandHandler *handler,
					       IrciumCommandReactor *reactor)
{
	g_critical ("A command handler should override register_handlers");
}

static void
ircium_command_handler_unregister_handlers_real (IrciumCommandHandler *handler,
                                                 IrciumCommandReactor *reactor)
{
	g_critical ("A command handler should override unregister_handlers");
}

static void
ircium_command_handler_init (IrciumCommandHandler *self)
{
}
