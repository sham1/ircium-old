/* ircium-session-private.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "ircium-session.h"
#include "ircium-connection-private.h"
#include "ircium-user.h"

G_BEGIN_DECLS

IrciumSession *ircium_session_new (IrciumConnection *conn);

GHashTable *ircium_session_get_isupport (IrciumSession *session);
void ircium_session_set_isupport_val (GHashTable *table,
                                      gchar      *param,
                                      gchar      *value);

void ircium_session_add_buffer (IrciumSession *session,
                                IrciumBuffer  *buffer);

void ircium_session_remove_buffer (IrciumSession *session,
                                   IrciumBuffer  *buffer);

IrciumUser *ircium_session_get_self (IrciumSession *session);

IrciumUser *ircium_session_find_user_nick (IrciumSession *session,
                                           const gchar   *nick);

void ircium_session_add_user (IrciumSession *session,
                              IrciumUser    *user);

void ircium_session_garbage_collect_users (IrciumSession *session);

G_END_DECLS
