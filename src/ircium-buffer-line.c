/* ircium-buffer-line.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-buffer-line.h"

struct _IrciumBufferLine
{
	GObject parent_instance;

	gboolean sender_is_known;
	gboolean sender_is_user;
	gchar *sender_nick;

	GDateTime *time;
	gchar *contents;
	IrciumBufferLineType type;

	IrciumBufferLineFlag flags;
};

G_DEFINE_TYPE (IrciumBufferLine, ircium_buffer_line, G_TYPE_OBJECT)

enum {
	PROP_0,

	PROP_SENDER_IS_KNOWN,
	PROP_SENDER_IS_USER,
	PROP_SENDER_NICK,
	PROP_TIME,
	PROP_CONTENTS,
	PROP_TYPE,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumBufferLine *
ircium_buffer_line_new_with_sender (IrciumBufferLineType       type,
                                    IrciumBufferLineFlag       flags,
                                    const IrciumMessageSource *sender,
                                    const GDateTime           *time_received,
                                    const gchar               *content)
{
	gboolean is_user = ircium_message_source_is_user (sender);
	const gchar *sender_nick =
		is_user ? ircium_message_source_get_nick (sender) : "";
	IrciumBufferLine *ret = g_object_new (IRCIUM_TYPE_BUFFER_LINE,
					      "type", type,
					      "sender_known", TRUE,
					      "sender_user", is_user,
					      "sender_nick", sender_nick,
					      "time", time_received,
					      "contents", content,
					      NULL);
	ret->flags = flags;
	return ret;
}

IrciumBufferLine *
ircium_buffer_line_new_with_nick (IrciumBufferLineType  type,
                                  IrciumBufferLineFlag  flags,
                                  const gchar          *sender,
                                  const GDateTime      *time_received,
                                  const gchar          *content)
{
	IrciumBufferLine *ret = g_object_new (IRCIUM_TYPE_BUFFER_LINE,
					      "type", type,
					      "sender_known", TRUE,
					      "sender_user", TRUE,
					      "sender_nick", sender,
					      "time", time_received,
					      "contents", content,
					      NULL);
	ret->flags = flags;
	return ret;
}

const gboolean
ircium_buffer_line_is_sender_known (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line), FALSE);
	return line->sender_is_known;
}

const gboolean
ircium_buffer_line_is_sender_user (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line), FALSE);
	return line->sender_is_user;
}

const gchar *
ircium_buffer_line_get_sender_nick (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line), NULL);
	return line->sender_nick;
}

static void
ircium_buffer_line_finalize (GObject *object)
{
	IrciumBufferLine *self = (IrciumBufferLine *)object;

	g_clear_pointer (&self->sender_nick, g_free);
	g_clear_pointer (&self->time, g_date_time_unref);
	g_clear_pointer (&self->contents, g_free);

	G_OBJECT_CLASS (ircium_buffer_line_parent_class)->finalize (object);
}

static void
ircium_buffer_line_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
	IrciumBufferLine *self = IRCIUM_BUFFER_LINE (object);

	switch (prop_id)
	{
	case PROP_SENDER_IS_KNOWN:
		g_value_set_boolean (value, self->sender_is_known);
		break;
	case PROP_SENDER_IS_USER:
		g_value_set_boolean (value, self->sender_is_user);
		break;
	case PROP_SENDER_NICK:
		g_value_set_string (value, self->sender_nick);
		break;
	case PROP_TIME:
		g_value_set_boxed (value, self->time);
		break;
	case PROP_CONTENTS:
		g_value_set_string (value, self->contents);
		break;
	case PROP_TYPE:
		g_value_set_int (value, self->type);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_buffer_line_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
	IrciumBufferLine *self = IRCIUM_BUFFER_LINE (object);

	switch (prop_id)
	{
	case PROP_SENDER_IS_KNOWN:
		self->sender_is_known = g_value_get_boolean (value);
		break;
	case PROP_SENDER_IS_USER:
		self->sender_is_user = g_value_get_boolean (value);
		break;
	case PROP_SENDER_NICK:
		g_clear_pointer (&self->sender_nick, g_free);
		self->sender_nick = g_value_dup_string (value);
		break;
	case PROP_TIME:
		self->time = g_value_dup_boxed (value);
		break;
	case PROP_CONTENTS:
		self->contents = g_value_dup_string (value);
		break;
	case PROP_TYPE:
		self->type = g_value_get_int (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_buffer_line_class_init (IrciumBufferLineClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ircium_buffer_line_finalize;
	object_class->get_property = ircium_buffer_line_get_property;
	object_class->set_property = ircium_buffer_line_set_property;

	properties[PROP_SENDER_IS_KNOWN] =
		g_param_spec_boolean ("sender_known",
				      "sender_known",
				      "If the sender is known",
				      FALSE,
				      G_PARAM_READWRITE |
				      G_PARAM_CONSTRUCT_ONLY);
	properties[PROP_SENDER_IS_USER] =
		g_param_spec_boolean ("sender_user",
				      "sender_user",
				      "If the sender is a user (not server)",
				      FALSE,
				      G_PARAM_READWRITE |
				      G_PARAM_CONSTRUCT_ONLY);
	properties[PROP_SENDER_NICK] =
		g_param_spec_string ("sender_nick",
				     "sender_nick",
				     "The sender of the message",
				     "",
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_TIME] =
		g_param_spec_boxed ("time",
				    "time",
				    "The time of this message",
				    G_TYPE_DATE_TIME,
				    G_PARAM_READWRITE |
				    G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_CONTENTS] =
		g_param_spec_string ("contents",
				     "contents",
				     "The contents of the message",
				     "",
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_TYPE] =
		g_param_spec_int ("type",
				  "type",
				  "The type of the buffer line",
				  0,
				  N_IRCIUM_BUFFER_LINE_TYPES,
				  IRCIUM_BUFFER_LINE_TYPE_NORMAL,
				  G_PARAM_READWRITE |
				  G_PARAM_CONSTRUCT_ONLY);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_buffer_line_init (IrciumBufferLine *self)
{
}

const GDateTime *
ircium_buffer_line_get_time (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line), NULL);
	return line->time;
}


const gchar *
ircium_buffer_line_get_contents (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line), NULL);
	return line->contents;
}

const IrciumBufferLineType
ircium_buffer_line_get_line_type (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line),
			      IRCIUM_BUFFER_LINE_TYPE_NORMAL);
	return line->type;
}

const IrciumBufferLineFlag
ircium_buffer_line_get_flags (IrciumBufferLine *line)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_LINE (line),
			      0);
	return line->flags;
}

void
ircium_buffer_line_add_flags (IrciumBufferLine     *line,
                              IrciumBufferLineFlag  new_flags)
{
	g_return_if_fail (IRCIUM_IS_BUFFER_LINE (line));
	line->flags |= new_flags;
}

void
ircium_buffer_line_remove_flags (IrciumBufferLine     *line,
                                 IrciumBufferLineFlag  flags)
{
	g_return_if_fail (IRCIUM_IS_BUFFER_LINE (line));
	IrciumBufferLineFlag mask = ~(flags);
	line->flags &= mask;
}
