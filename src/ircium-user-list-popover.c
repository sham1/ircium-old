/* ircium-user-list-popover.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-user-list-popover.h"

#include "ircium-channel-user.h"

struct _IrciumUserListPopover
{
	GtkPopover parent_instance;

	GtkListBox *user_list;
};

G_DEFINE_TYPE (IrciumUserListPopover, ircium_user_list_popover, GTK_TYPE_POPOVER)

enum {
	PROP_0,
	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumUserListPopover *
ircium_user_list_popover_new (void)
{
	return g_object_new (IRCIUM_TYPE_USER_LIST_POPOVER, NULL);
}

static void
ircium_user_list_popover_finalize (GObject *object)
{
	IrciumUserListPopover *self = (IrciumUserListPopover *)object;

	G_OBJECT_CLASS (ircium_user_list_popover_parent_class)->finalize (object);
}

static void
ircium_user_list_popover_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
	IrciumUserListPopover *self = IRCIUM_USER_LIST_POPOVER (object);

	switch (prop_id)
	  {
	  default:
	    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	  }
}

static void
ircium_user_list_popover_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
	IrciumUserListPopover *self = IRCIUM_USER_LIST_POPOVER (object);

	switch (prop_id)
	  {
	  default:
	    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	  }
}

static void
ircium_user_list_popover_class_init (IrciumUserListPopoverClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	gtk_widget_class_set_template_from_resource
		(widget_class,
		 "/com/gitlab/sham1/Ircium/ircium_user_list_popover.ui");

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumUserListPopover,
					      user_list);

	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ircium_user_list_popover_finalize;
	object_class->get_property = ircium_user_list_popover_get_property;
	object_class->set_property = ircium_user_list_popover_set_property;
}

static void
ircium_user_list_popover_init (IrciumUserListPopover *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
}

static GtkWidget *
create_user_entry_cb (gpointer item,
                      gpointer user_data)
{
	IrciumChannelUser *user = item;
	GtkWidget *ret = gtk_label_new (ircium_channel_user_get_nick (user));
	g_object_bind_property (user, "nick",
				ret, "label",
				G_BINDING_SYNC_CREATE);
	return ret;
}

void
ircium_user_list_popover_set_list (IrciumUserListPopover *popover,
                                   GListModel            *users)
{
	gtk_list_box_bind_model (popover->user_list,
				 users,
				 create_user_entry_cb,
				 NULL, NULL);
}
