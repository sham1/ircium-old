/* ircium-window.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-window.h"
#include "ircium-connection.h"
#include "ircium-network-settings-panel.h"
#include "ircium-message.h"
#include "ircium-buffer-view.h"
#include "ircium-format.h"
#include <glib/gi18n.h>
#include "ircium-application.h"
#include "ircium-user-list-popover.h"
#include "ircium-channel-buffer.h"

#define HANDY_USE_UNSTABLE_API
#include <handy.h>

typedef enum
{
	IRCIUM_WINDOW_STATE_WELCOME_NOSERVERS,
	IRCIUM_WINDOW_STATE_WELCOME_SERVERS,
	IRCIUM_WINDOW_STATE_NEW_SERVER,
	IRCIUM_WINDOW_STATE_SERVER_OPTIONS,
	IRCIUM_WINDOW_STATE_CHAT_VIEW,
} IrciumWindowState;

struct IrciumWindowSignalHandlers {
	gulong connection_name_changed;
	gulong connection_address_changed;
	gulong connection_port_changed;
	gulong connection_tls_changed;
	gulong connection_nickname_changed;

	gulong connection_minimum_viable_changed;

	gulong session_buffers_changed;

	gulong buffer_topic_changed;
};

struct _IrciumWindow
{
	GtkApplicationWindow parent_instance;

	GtkStack *header_stack;
	GtkStack *content_stack;

	GtkStack *buffer_stack;

	GQueue *state_stack;

	IrciumConnection *curr_connection;

	IrciumNetworkSettingsPanel *server_opts;

	// The simple new server settings
	GtkEntry *new_server_simple_name;
	GtkEntry *new_server_simple_address;
	GtkEntry *new_server_simple_port;
	GtkSwitch *new_server_simple_tls;
	GtkEntry *new_server_simple_nick;
	GtkButton *new_server_create;

	struct IrciumWindowSignalHandlers handlers;

	IrciumSession *session;
	GCancellable *conn_cancellable;

	GtkRevealer *topic_revealer;
	GtkLabel *topic_label;
	GtkToggleButton *show_topic;
	GtkLabel *topic_status;
	GtkStack *topic_stack;
	GtkEntry *topic_entry;
	GtkToggleButton *topic_edit_toggle;

	IrciumBuffer *active_buffer;

	GPtrArray *cert_except_store;

	// Server list stuff
	GtkListBox *server_list;
	GtkButton *server_list_add_server;
	GtkButton *server_list_remove;
	GtkButton *server_list_connect;
	GtkButton *server_list_more_opts;

	// User list
	IrciumUserListPopover *user_list_popover;
	GtkToggleButton *show_users;

	// Responsive stuff
	HdyLeaflet *chat_leaflet;
	HdyLeaflet *header_leaflet;
	GtkButton *chat_view_go_back;
	GtkHeaderBar *buffer_list_headerbar;
	GtkHeaderBar *buffer_view_headerbar;
};

G_DEFINE_TYPE (IrciumWindow, ircium_window, GTK_TYPE_APPLICATION_WINDOW)

enum {
	PROP_0,

	PROP_CONNECTION,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void ircium_window_go_back_state (IrciumWindow *win);

static void ircium_window_change_state (IrciumWindow      *win,
                                        IrciumWindowState  state);

static void ircium_window_register_connection_signals (IrciumWindow *self);
static void ircium_window_unregister_connection_signals (IrciumWindow *self);
static void ircium_window_unregister_session_handlers (IrciumWindow *self,
						       gboolean is_dispose);
static void setup_app (IrciumWindow      *self,
                       IrciumApplication *app);

static gboolean
accept_certificate_cb (GTlsConnection       *conn,
                       GTlsCertificate      *peer_cert,
                       GTlsCertificateFlags  errors,
                       gpointer              user_data);

GtkWidget *
ircium_window_new (GtkApplication *app)
{
	g_return_val_if_fail (IRCIUM_IS_APPLICATION (app), NULL);

	IrciumWindow *win = g_object_new (IRCIUM_TYPE_WINDOW,
					  "application", app,
					  NULL);

	setup_app (win, (IrciumApplication *) app);

	return GTK_WIDGET (win);
}

static void
ircium_window_dispose (GObject *obj)
{
	IrciumWindow *win = (IrciumWindow *) obj;

	if (win->state_stack != NULL) {
		g_queue_free_full (win->state_stack,
				   (GDestroyNotify) g_variant_unref);
		win->state_stack = NULL;
	}

	// Unregister IrciumConnection signal handlers and freeing it.
	ircium_window_unregister_connection_signals (win);
	g_clear_object (&win->curr_connection);
	ircium_window_unregister_session_handlers (win, TRUE);
	g_clear_object (&win->session);
	if (win->conn_cancellable != NULL &&
	    !g_cancellable_is_cancelled (win->conn_cancellable)) {
		g_cancellable_cancel (win->conn_cancellable);
	}
	g_clear_object (&win->conn_cancellable);

	if (win->active_buffer != NULL) {
		if (win->handlers.buffer_topic_changed != 0) {
			g_signal_handler_disconnect
				(win->active_buffer,
				 win->handlers.buffer_topic_changed);
			win->handlers.buffer_topic_changed = 0;
		}
		g_clear_object (&win->active_buffer);
	}

	g_clear_object (&win->user_list_popover);

	g_clear_pointer (&win->cert_except_store, g_ptr_array_unref);

	G_OBJECT_CLASS (ircium_window_parent_class)->dispose (obj);
}

static void
show_users_toggled_cb (IrciumWindow    *win,
                       GtkToggleButton *button)
{
	gboolean state = gtk_toggle_button_get_active (button);
	gtk_widget_set_visible (GTK_WIDGET (win->user_list_popover), state);
}

static void
user_popup_closed_cb (GtkPopover   *popover,
                      IrciumWindow *win)
{
	gtk_toggle_button_set_active (win->show_users, FALSE);
}

static void buffers_changed_cb (GListModel *list,
                                guint       position,
                                guint       removed,
                                guint       added,
                                gpointer    user_data);

static void
connected_cb (IrciumConnection *conn,
              GAsyncResult     *res,
              IrciumWindow     *win)
{
	g_autoptr (GError) error = NULL;
	win->session = ircium_connection_connect_finish (conn, res, &error);
	if (win->session == NULL) {
		if (error->domain != G_TLS_ERROR) {
			g_debug ("Couldn't connect to server: %s",
				 error->message);
		}
		return;
	}
	ircium_window_change_state (win, IRCIUM_WINDOW_STATE_CHAT_VIEW);

	ircium_window_unregister_session_handlers (win, FALSE);
	win->handlers.session_buffers_changed =
		g_signal_connect (ircium_session_get_buffer_list (win->session),
				  "items-changed",
				  G_CALLBACK (buffers_changed_cb),
				  win);
	buffers_changed_cb (ircium_session_get_buffer_list (win->session),
			    0,
			    0,
			    g_list_model_get_n_items (
				ircium_session_get_buffer_list (win->session)),
			    win);
}

static void
start_connection (IrciumWindow *win)
{
	g_clear_object (&win->conn_cancellable);
	win->conn_cancellable = g_cancellable_new ();

	ircium_connection_connect_async (win->curr_connection,
					 win->conn_cancellable,
					 NULL,
					 (GAsyncReadyCallback) connected_cb,
					 win);
}

static void
on_new_server_create_clicked (IrciumWindow *win,
                              GtkButton    *button)
{
	GSettings *new_account =
		ircium_application_create_new_account
			(IRCIUM_APPLICATION (gtk_window_get_application
					     (GTK_WINDOW (win))));

	ircium_connection_set_settings (win->curr_connection, new_account);

	g_object_unref (new_account);

	start_connection (win);
}

static void
reset_new_server_gui (IrciumWindow *self)
{
	// Even though we know for a fact that this only gets called
	// when we set a new server, let's make this at least appear
	// more useful.
	IrciumConnection *conn = self->curr_connection;
	{
		const gchar *txt = ircium_connection_get_name (conn);
		gtk_entry_set_text (self->new_server_simple_name,
				    txt != NULL ? txt : "");
	}

	{
		const gchar *txt = ircium_connection_get_address (conn);
		gtk_entry_set_text (self->new_server_simple_address,
				    txt != NULL ? txt : "");
	}

	{
		const guint16 port = ircium_connection_get_port (conn);
		gchar *txt = g_strdup_printf ("%"G_GUINT16_FORMAT, port);
		gtk_entry_set_text (self->new_server_simple_port,
				    port != 0 ? txt : "");
		g_free (txt);
	}

	{
		const gboolean tls = ircium_connection_get_tls (conn);
		gtk_switch_set_active (self->new_server_simple_tls, tls);
	}

	{
		const gchar *txt = ircium_connection_get_nickname (conn);
		gtk_entry_set_text (self->new_server_simple_nick,
				    txt != NULL ? txt : "");
	}

	{
		const gboolean is_viable =
			ircium_connection_is_minimum_viable (conn);
		gtk_widget_set_sensitive (GTK_WIDGET (self->new_server_create),
					  is_viable);
	}
}

static void
set_connection (IrciumWindow     *win,
                IrciumConnection *conn)
{
	if (win->curr_connection != NULL) {
		ircium_window_unregister_connection_signals (win);
		g_clear_object (&win->curr_connection);
		g_object_notify_by_pspec (G_OBJECT (win),
					  properties[PROP_CONNECTION]);
	}

	if (conn == NULL) return;

	win->curr_connection = conn;

	ircium_connection_set_accept_cert_handler (win->curr_connection,
						   accept_certificate_cb,
						   g_object_ref (win),
						   g_object_unref);
	ircium_window_register_connection_signals (win);
	g_object_notify_by_pspec (G_OBJECT (win), properties[PROP_CONNECTION]);
}

static void
welcome_add_server_clicked_cb (IrciumWindow *win,
                               GtkButton    *button)
{
	set_connection (win, ircium_connection_new ());
	reset_new_server_gui (win);
	ircium_window_change_state (win,
				    IRCIUM_WINDOW_STATE_NEW_SERVER);
}

static void
server_options_clicked_cb (IrciumWindow *win,
                           GtkButton    *button)
{
	ircium_window_change_state (win,
				    IRCIUM_WINDOW_STATE_SERVER_OPTIONS);
}

static void
back_clicked_cb (IrciumWindow *win,
                 GtkButton    *button)
{
	ircium_window_go_back_state (win);
}

static void
on_topic_close_clicked (IrciumWindow *win,
                        GtkButton    *button)
{
	gtk_toggle_button_set_active (win->show_topic, FALSE);
}

static void
ircium_window_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
	IrciumWindow *self = IRCIUM_WINDOW (object);

	switch (prop_id)
	{
	case PROP_CONNECTION:
		g_value_set_object (value, self->curr_connection);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_window_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
	IrciumWindow *self = IRCIUM_WINDOW (object);

	switch (prop_id)
	{
	case PROP_CONNECTION:
		self->curr_connection = g_value_dup_object (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

// New server dialog signal handlers
static void
on_new_server_simple_name_changed (IrciumWindow *self,
                                   GtkEditable  *editable);

static void
on_new_server_simple_port_changed (IrciumWindow *self,
                                   GtkEditable  *editable);

static void
on_new_server_simple_address_changed (IrciumWindow *self,
                                      GtkEditable  *editable);

static void
on_new_server_simple_tls_changed (IrciumWindow *self,
                                  GParamSpec   *pspec,
                                  GObject      *obj);

static void
on_new_server_simple_nick_changed (IrciumWindow *self,
                                   GtkEditable  *editable);

static gboolean
on_window_delete_event (IrciumWindow *win,
                        GdkEvent     *event,
                        gpointer      user_data);

static void on_buffer_stack_visible_child_notify (GObject    *obj,
                                                  GParamSpec *pspec,
                                                  gpointer    user_data);

static void on_server_list_row_selected (GtkListBox    *box,
                                         GtkListBoxRow *row,
                                         gpointer       user_data);

static void on_server_list_row_activated (GtkListBox    *box,
                                          GtkListBoxRow *row,
                                          IrciumWindow  *win);

static void on_server_list_connect_clicked (GtkButton    *button,
                                            IrciumWindow *win);

static void on_server_list_more_opts_clicked (GtkButton    *button,
                                              IrciumWindow *win);

static void on_server_list_remove_clicked (GtkButton    *button,
                                           IrciumWindow *win);

static void on_chat_view_go_back_clicked (IrciumWindow *win,
                                          GtkButton    *button);

static void on_topic_edit_toggle_toggled (IrciumWindow    *win,
                                          GtkToggleButton *button);

static void on_show_topic_toggled (IrciumWindow    *win,
                                   GtkToggleButton *button);

static void on_topic_entry_activate (IrciumWindow *win, GtkEntry *entry);

static void
ircium_window_class_init (IrciumWindowClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	object_class->dispose = ircium_window_dispose;
	object_class->get_property = ircium_window_get_property;
	object_class->set_property = ircium_window_set_property;

	GtkWidgetClass *widget_class = (GtkWidgetClass *) klass;

	gtk_widget_class_set_template_from_resource
		(widget_class, "/com/gitlab/sham1/Ircium/ircium.ui");

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      header_stack);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      content_stack);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      buffer_stack);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      server_opts);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      new_server_simple_name);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      new_server_simple_address);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      new_server_simple_port);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      new_server_simple_tls);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      new_server_simple_nick);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      new_server_create);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      topic_revealer);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      topic_label);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      topic_status);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      show_topic);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      topic_entry);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      topic_stack);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      topic_edit_toggle);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      server_list);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      server_list_add_server);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      server_list_remove);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      server_list_connect);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      server_list_more_opts);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      show_users);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      chat_leaflet);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      header_leaflet);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      chat_view_go_back);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      buffer_list_headerbar);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumWindow,
					      buffer_view_headerbar);

	gtk_widget_class_bind_template_callback (widget_class,
						 show_users_toggled_cb);
	gtk_widget_class_bind_template_callback (widget_class,
						 welcome_add_server_clicked_cb);
	gtk_widget_class_bind_template_callback (widget_class,
						 back_clicked_cb);
	gtk_widget_class_bind_template_callback (widget_class,
						 server_options_clicked_cb);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_new_server_create_clicked);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_window_delete_event);

	gtk_widget_class_bind_template_callback (widget_class,
						 on_topic_edit_toggle_toggled);

	gtk_widget_class_bind_template_callback (widget_class,
						 on_server_list_row_selected);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_server_list_row_activated);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_server_list_connect_clicked);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_server_list_more_opts_clicked);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_server_list_remove_clicked);

	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_buffer_stack_visible_child_notify);

	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_new_server_simple_name_changed);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_new_server_simple_address_changed);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_new_server_simple_port_changed);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_new_server_simple_tls_changed);
	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_new_server_simple_nick_changed);

	gtk_widget_class_bind_template_callback (widget_class,
						 on_topic_close_clicked);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_show_topic_toggled);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_chat_view_go_back_clicked);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_topic_entry_activate);

	g_type_ensure (IRCIUM_TYPE_NETWORK_SETTINGS_PANEL);

	properties[PROP_CONNECTION] =
		g_param_spec_object ("connection",
				     "connection",
				     "Current connection",
				     IRCIUM_TYPE_CONNECTION,
				     G_PARAM_READABLE);
	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_window_init (IrciumWindow *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));

	GtkCssProvider *default_style =
		gtk_css_provider_new ();
	gtk_css_provider_load_from_resource
		(default_style,
		 "/com/gitlab/sham1/Ircium/ircium-buffer-style.css");
	GdkScreen *screen = gdk_screen_get_default ();
	gtk_style_context_add_provider_for_screen
		(screen,
		 GTK_STYLE_PROVIDER (default_style),
		 GTK_STYLE_PROVIDER_PRIORITY_FALLBACK);
	g_object_unref (default_style);

	self->state_stack = g_queue_new ();

	self->user_list_popover =
		g_object_ref_sink (ircium_user_list_popover_new ());
	gtk_widget_show_all (GTK_WIDGET (self->user_list_popover));
	gtk_widget_set_visible (GTK_WIDGET (self->user_list_popover), FALSE);
	gtk_popover_set_relative_to (GTK_POPOVER (self->user_list_popover),
				     GTK_WIDGET (self->show_users));
	g_signal_connect (self->user_list_popover,
			  "closed",
			  G_CALLBACK (user_popup_closed_cb),
			  self);

	ircium_network_settings_panel_set_window (self->server_opts, self);
	self->cert_except_store = g_ptr_array_new_with_free_func (g_free);
	// TODO: Load certs from a database or something.

	g_object_bind_property (self->chat_leaflet, "visible-child-name",
				self->header_leaflet, "visible-child-name",
				G_BINDING_SYNC_CREATE |
				G_BINDING_BIDIRECTIONAL);

	g_object_bind_property (self->header_leaflet, "folded",
				self->chat_view_go_back, "visible",
				G_BINDING_SYNC_CREATE);

	g_object_bind_property (self->chat_leaflet,
				"folded",
				self->buffer_list_headerbar,
				"show-close-button",
				G_BINDING_SYNC_CREATE);

	g_object_bind_property (self->show_topic,
				"active",
				self->topic_revealer,
				"reveal_child",
				G_BINDING_SYNC_CREATE);
}

static GtkStackTransitionType
get_state_transition (IrciumWindowState state)
{
	GtkStackTransitionType ret = GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT;

	switch (state) {
	case IRCIUM_WINDOW_STATE_SERVER_OPTIONS:
		ret = GTK_STACK_TRANSITION_TYPE_SLIDE_UP;
		break;
	default:
		break;
	}

	return ret;
}

static void
set_state_stacks (IrciumWindow           *win,
                  const gchar            *stack_name,
                  GtkStackTransitionType  trans_type)
{
	gtk_stack_set_visible_child_full (win->content_stack,
					  stack_name,
					  trans_type);
	gtk_stack_set_visible_child_full (win->header_stack,
					  stack_name,
					  GTK_STACK_TRANSITION_TYPE_CROSSFADE);
}

static const gchar *
get_stack_name (IrciumWindowState state)
{
	switch (state) {
	case IRCIUM_WINDOW_STATE_WELCOME_NOSERVERS:
		return "welcome_noservers";
	case IRCIUM_WINDOW_STATE_WELCOME_SERVERS:
		return "welcome_servers";
	case IRCIUM_WINDOW_STATE_NEW_SERVER:
		return "new_server";
	case IRCIUM_WINDOW_STATE_SERVER_OPTIONS:
		return "server_opts";
	case IRCIUM_WINDOW_STATE_CHAT_VIEW:
		return "chat_view";
	default:
		return NULL;
	}
}

static void
ircium_window_change_state (IrciumWindow      *win,
                            IrciumWindowState  state)
{
	const gchar *stack_name = get_stack_name (state);
	GtkStackTransitionType trans_type = get_state_transition (state);

	set_state_stacks (win, stack_name, trans_type);
	g_queue_push_head (win->state_stack,
			   g_variant_new_int32 (state));
}

static GtkStackTransitionType
invert_transition (GtkStackTransitionType trans_type)
{
	switch (trans_type) {
	case GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT:
		return GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT;
	case GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT:
		return GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT;
	case GTK_STACK_TRANSITION_TYPE_SLIDE_UP:
		return GTK_STACK_TRANSITION_TYPE_SLIDE_DOWN;
	case GTK_STACK_TRANSITION_TYPE_SLIDE_DOWN:
		return GTK_STACK_TRANSITION_TYPE_SLIDE_UP;
	default:
		return GTK_STACK_TRANSITION_TYPE_NONE;
	}
}

static void
ircium_window_go_back_state (IrciumWindow *win)
{
	if (g_queue_get_length (win->state_stack) > 1) {
		GVariant *curr_state = g_queue_pop_head (win->state_stack);
		g_return_if_fail (curr_state != NULL);
		IrciumWindowState cstate = g_variant_get_int32 (curr_state);
		g_variant_unref (curr_state);

		GVariant *prev_state = g_queue_peek_head (win->state_stack);
		g_return_if_fail (prev_state != NULL);
		IrciumWindowState pstate = g_variant_get_int32 (prev_state);

		GtkStackTransitionType trans_type =
			get_state_transition (cstate);
		trans_type = invert_transition (trans_type);
		const gchar *prev_stack_name = get_stack_name (pstate);

		set_state_stacks (win, prev_stack_name, trans_type);
	}
}

/**
 * ircium_window_get_connection:
 *
 * Gives a new reference to the current connection. Free with g_object_unref().
 *
 * Returns: (transfer full): the connection
 */
IrciumConnection *
ircium_window_get_connection (IrciumWindow *self)
{
	return self->curr_connection != NULL ?
		g_object_ref (self->curr_connection) :
		NULL;
}

static void
on_new_server_simple_port_changed (IrciumWindow *self,
                                   GtkEditable  *editable)
{
	GtkEntry *port_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (port_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (port_entry));

	GtkStyleContext *ctx =
		gtk_widget_get_style_context (GTK_WIDGET (port_entry));

	guint16 text_len = gtk_entry_get_text_length (port_entry);
	if (text_len == 0) {
		// Even though we are removing the error style,
		// we will still unset the port.
		if (gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_remove_class (ctx, "error");
		}
		ircium_connection_unset_port (self->curr_connection);
		return;
	}

	const gchar *content = gtk_entry_get_text (port_entry);
	if (!g_utf8_validate (content, -1, NULL)) {
		if (!gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_add_class (ctx, "error");
		}
		return;
	}
	gboolean valid = TRUE;
	glong content_len = g_utf8_strlen (content, -1);
	gulong assumed_port_num = 0;
	for (glong i = 0; i < content_len; ++i) {
		gunichar c = g_utf8_get_char (content);
		content = g_utf8_next_char (content);

		if (!g_unichar_isdigit (c)) {
			valid = FALSE;
			break;
		}
		assumed_port_num =
			assumed_port_num * 10 + g_unichar_digit_value (c);
		// The port will be invalid if the digits give us a larger
		// number than 2^16, since (2^16) - 1 is the largest possible
		// port number because ports are 16-bit unsigned.
		if (assumed_port_num > G_MAXUINT16) {
			valid = FALSE;
			break;
		}
	}
	// Port 0 also cannot be used because it's reserved.
	if (assumed_port_num == 0) valid = FALSE;
	if (valid) {
		if (gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_remove_class (ctx, "error");
		}

		if (should_matter) {
			guint16 port_num = (guint16) assumed_port_num;
			ircium_connection_set_port (self->curr_connection,
						    port_num);
		}
	} else {
		if (!gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_add_class (ctx, "error");
		}
		// We unset the connection port to indicate an invalid state.
		if (should_matter)
			ircium_connection_unset_port (self->curr_connection);
	}
}

static void
on_connection_name_changed_cb (GObject      *obj,
                               GParamSpec   *pspec,
                               IrciumWindow *self);
static void
on_connection_address_changed_cb (GObject      *obj,
                                  GParamSpec   *pspec,
                                  IrciumWindow *self);
static void
on_connection_port_changed_cb (GObject      *obj,
                               GParamSpec   *pspec,
                               IrciumWindow *self);
static void
on_connection_tls_changed_cb (GObject      *obj,
                              GParamSpec   *pspec,
                              IrciumWindow *self);
static void
on_connection_nick_changed_cb (GObject      *obj,
                               GParamSpec   *pspec,
                               IrciumWindow *self);
static void
on_connection_minimum_changed_cb (GObject      *obj,
                                  GParamSpec   *pspec,
                                  IrciumWindow *self);

static void
ircium_window_register_connection_signals (IrciumWindow *self)
{
	// Some sanity tests
	if (self->curr_connection == NULL) {
		g_critical ("%s should be called only if we have a conneciton",
			    __func__);
	}
	if (self->handlers.connection_port_changed != 0) {
		g_critical ("%s should be only called once per connection",
			    __func__);
		ircium_window_unregister_connection_signals (self);
	}

	self->handlers.connection_name_changed =
		g_signal_connect (self->curr_connection,
				  "notify::name",
				  G_CALLBACK (on_connection_name_changed_cb),
				  self);
	self->handlers.connection_address_changed =
		g_signal_connect (self->curr_connection,
				  "notify::hostname",
				  G_CALLBACK (on_connection_address_changed_cb),
				  self);
	self->handlers.connection_port_changed =
		g_signal_connect (self->curr_connection,
				  "notify::port",
				  G_CALLBACK (on_connection_port_changed_cb),
				  self);
	self->handlers.connection_tls_changed =
		g_signal_connect (self->curr_connection,
				  "notify::tls",
				  G_CALLBACK (on_connection_tls_changed_cb),
				  self);
	self->handlers.connection_nickname_changed =
		g_signal_connect (self->curr_connection,
				  "notify::nickname",
				  G_CALLBACK (on_connection_nick_changed_cb),
				  self);
	self->handlers.connection_minimum_viable_changed =
		g_signal_connect (self->curr_connection,
				  "notify::minimum",
				  G_CALLBACK (on_connection_minimum_changed_cb),
				  self);
}

static void
ircium_window_unregister_connection_signals (IrciumWindow *self)
{
	if (self->handlers.connection_name_changed != 0) {
		g_signal_handler_disconnect
			(self->curr_connection,
			 self->handlers.connection_name_changed);
		self->handlers.connection_name_changed = 0;
	}
	if (self->handlers.connection_address_changed != 0) {
		g_signal_handler_disconnect
			(self->curr_connection,
			 self->handlers.connection_address_changed);
		self->handlers.connection_address_changed = 0;
	}
	if (self->handlers.connection_port_changed != 0) {
		g_signal_handler_disconnect
			(self->curr_connection,
			 self->handlers.connection_port_changed);
		self->handlers.connection_port_changed = 0;
	}
	if (self->handlers.connection_tls_changed != 0) {
		g_signal_handler_disconnect
			(self->curr_connection,
			 self->handlers.connection_tls_changed);
		self->handlers.connection_tls_changed = 0;
	}
	if (self->handlers.connection_nickname_changed != 0) {
		g_signal_handler_disconnect
			(self->curr_connection,
			 self->handlers.connection_nickname_changed);
		self->handlers.connection_nickname_changed = 0;
	}
	if (self->handlers.connection_minimum_viable_changed != 0) {
		g_signal_handler_disconnect
			(self->curr_connection,
			 self->handlers.connection_minimum_viable_changed);
		self->handlers.connection_minimum_viable_changed = 0;
	}
}

static void
on_connection_name_changed_cb (GObject      *obj,
                               GParamSpec   *pspec,
                               IrciumWindow *self)
{
	GVariant *var = g_queue_peek_head (self->state_stack);
	IrciumWindowState state = g_variant_get_int32 (var);

	if (state != IRCIUM_WINDOW_STATE_NEW_SERVER) {
		const gchar *txt =
			ircium_connection_get_name (self->curr_connection);
		gtk_entry_set_text (self->new_server_simple_name,
				    txt != NULL ? txt : "");
	}
}

static void
on_connection_address_changed_cb (GObject      *obj,
                                  GParamSpec   *pspec,
                                  IrciumWindow *self)
{
	GVariant *var = g_queue_peek_head (self->state_stack);
	IrciumWindowState state = g_variant_get_int32 (var);

	if (state != IRCIUM_WINDOW_STATE_NEW_SERVER) {
		const gchar *txt =
			ircium_connection_get_address (self->curr_connection);
		gtk_entry_set_text (self->new_server_simple_address,
				    txt != NULL ? txt : "");
	}
}

static void
on_connection_port_changed_cb (GObject      *obj,
                               GParamSpec   *pspec,
                               IrciumWindow *self)
{
	GVariant *var = g_queue_peek_head (self->state_stack);
	IrciumWindowState state = g_variant_get_int32 (var);

	if (state != IRCIUM_WINDOW_STATE_NEW_SERVER) {
		gboolean has_actual_port =
			ircium_connection_is_port_set (self->curr_connection);
		if (has_actual_port) {
			const guint16 port = ircium_connection_get_port
				(self->curr_connection);
			gchar *txt = g_strdup_printf ("%"G_GUINT16_FORMAT,
						      port);
			gtk_entry_set_text (self->new_server_simple_port, txt);
			g_free (txt);
		} else {
			gtk_entry_set_text (self->new_server_simple_port, "");
		}
	}
}

static void
on_connection_tls_changed_cb (GObject      *obj,
                              GParamSpec   *pspec,
                              IrciumWindow *self)
{
	GVariant *var = g_queue_peek_head (self->state_stack);
	IrciumWindowState state = g_variant_get_int32 (var);

	if (state != IRCIUM_WINDOW_STATE_NEW_SERVER) {
		gboolean tls =
			ircium_connection_get_tls (self->curr_connection);
		gtk_switch_set_active (self->new_server_simple_tls, tls);
	}
}

static void
on_connection_nick_changed_cb (GObject      *obj,
                               GParamSpec   *pspec,
                               IrciumWindow *self)
{
	GVariant *var = g_queue_peek_head (self->state_stack);
	IrciumWindowState state = g_variant_get_int32 (var);

	if (state != IRCIUM_WINDOW_STATE_NEW_SERVER) {
		const gchar *txt =
			ircium_connection_get_nickname (self->curr_connection);
		gtk_entry_set_text (self->new_server_simple_nick,
				    txt != NULL ? txt : "");
	}
}

static void
on_new_server_simple_name_changed (IrciumWindow *self,
                                   GtkEditable  *editable)
{
	GtkEntry *name_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (name_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (name_entry));

	guint16 len = gtk_entry_get_text_length (name_entry);

	if (should_matter) {
		if (len == 0) {
			ircium_connection_set_name (self->curr_connection,
						    NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (name_entry);
		ircium_connection_set_name (self->curr_connection, txt);
	}
}

gboolean
ircium_window_is_network_settings_panel_visible (IrciumWindow *self)
{
	GVariant *curr_state = g_queue_peek_head (self->state_stack);
	g_return_val_if_fail (curr_state != NULL, FALSE);
	IrciumWindowState state = g_variant_get_int32 (curr_state);
	return state == IRCIUM_WINDOW_STATE_SERVER_OPTIONS;
}

static void
on_new_server_simple_address_changed (IrciumWindow *self,
                                      GtkEditable  *editable)
{
	GtkEntry *address_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (address_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (address_entry));

	guint16 len = gtk_entry_get_text_length (address_entry);

	if (should_matter) {
		if (len == 0) {
			ircium_connection_set_address (self->curr_connection,
						       NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (address_entry);
		ircium_connection_set_address (self->curr_connection, txt);
	}
}

IrciumWindowState
ircium_window_get_state (IrciumWindow *self)
{
	GVariant *curr_state = g_queue_peek_head (self->state_stack);
	g_return_val_if_fail (curr_state != NULL, FALSE);
	IrciumWindowState state = g_variant_get_int32 (curr_state);
	return state;
}

static void
on_new_server_simple_tls_changed (IrciumWindow *self,
                                  GParamSpec   *pspec,
                                  GObject      *obj)
{
	GtkSwitch *tls_switch = GTK_SWITCH (obj);
	gboolean should_matter =
		ircium_window_get_state (self) ==
		IRCIUM_WINDOW_STATE_NEW_SERVER;
	gboolean tls = gtk_switch_get_active (tls_switch);
	if (should_matter)
		ircium_connection_set_tls (self->curr_connection,
					   tls);
}

static void
on_new_server_simple_nick_changed (IrciumWindow *self,
                                   GtkEditable  *editable)
{
	GtkEntry *nickname_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (nickname_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (nickname_entry));

	guint16 len = gtk_entry_get_text_length (nickname_entry);

	if (should_matter) {
		if (len == 0) {
			ircium_connection_set_nickname (self->curr_connection,
							NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (nickname_entry);
		ircium_connection_set_nickname (self->curr_connection, txt);
	}
}

static void
on_connection_minimum_changed_cb (GObject      *obj,
                                  GParamSpec   *pspec,
                                  IrciumWindow *self)
{
	gboolean is_viable =
		ircium_connection_is_minimum_viable (self->curr_connection);
	gtk_widget_set_sensitive (GTK_WIDGET (self->new_server_create),
				  is_viable);
}

static gboolean
on_window_delete_event (IrciumWindow *win,
                        GdkEvent     *event,
                        gpointer      user_data)
{
	if (win->conn_cancellable != NULL &&
	    !g_cancellable_is_cancelled (win->conn_cancellable)) {
		g_cancellable_cancel (win->conn_cancellable);
	}
	return FALSE;
}

static void
ircium_window_unregister_session_handlers (IrciumWindow *self,
                                           gboolean      is_dispose)
{
	if (is_dispose) return;
	if (self->handlers.session_buffers_changed &&
	    ircium_session_get_buffer_list (self->session)) {
		g_signal_handler_disconnect
			(self->session,
			 self->handlers.session_buffers_changed);
		self->handlers.session_buffers_changed = 0;
	}
}

static void
buffers_changed_cb (GListModel *list,
                    guint       position,
                    guint       removed,
                    guint       added,
                    gpointer    user_data)
{
	IrciumWindow *self = user_data;
	GList *stack_children =
		gtk_container_get_children (GTK_CONTAINER (self->buffer_stack));
	while (removed--) {
		IrciumBufferView *view =
			g_list_nth (stack_children, position)->data;
		gtk_container_remove (GTK_CONTAINER (self->buffer_stack),
				      GTK_WIDGET (view));
	}
	for (gsize i = 0; i < added; ++i) {
		IrciumBuffer *buf = g_list_model_get_item (list, position + i);
		IrciumBufferView *view = ircium_buffer_view_new (buf,
								 self->session);
		gchar *title = ircium_buffer_get_title (buf);
		gtk_stack_add_titled (self->buffer_stack,
				      GTK_WIDGET (view),
				      title,
				      title);
	}
}

static void
set_topic (IrciumWindow *win,
           IrciumBuffer *buf);

static void
on_topic_change_cb (GObject    *obj,
                    GParamSpec *pspec,
                    gpointer    user_data)
{
	IrciumWindow *win = user_data;
	IrciumBuffer *buf = (IrciumBuffer *) obj;
	set_topic (win, buf);
}

static void
on_buffer_stack_visible_child_notify (GObject    *obj,
                                      GParamSpec *pspec,
                                      gpointer    user_data)
{
	IrciumWindow *win = user_data;
	GtkStack *stack = (GtkStack *) obj;

	GtkWidget *child = gtk_stack_get_visible_child (stack);
	IrciumBufferView *view = (IrciumBufferView *) child;
	g_return_if_fail (IRCIUM_IS_BUFFER_VIEW (view));
	IrciumBuffer *buf = ircium_buffer_view_get_buffer (view);

	if (win->active_buffer != NULL) {
		if (win->handlers.buffer_topic_changed != 0) {
			g_signal_handler_disconnect
				(win->active_buffer,
				 win->handlers.buffer_topic_changed);
			win->handlers.buffer_topic_changed = 0;
		}
		g_clear_object (&win->active_buffer);
	}

	win->active_buffer = g_object_ref (buf);

	gtk_toggle_button_set_active (win->show_topic, FALSE);
	gtk_widget_set_visible (GTK_WIDGET (win->show_topic),
				IRCIUM_IS_CHANNEL_BUFFER (buf));

	gboolean has_topic = ircium_buffer_has_topic (win->active_buffer);
	if (has_topic) {
		win->handlers.buffer_topic_changed =
			g_signal_connect (win->active_buffer,
					  "notify::topic",
					  G_CALLBACK (on_topic_change_cb),
					  win);
		set_topic (win, win->active_buffer);
	}
	gtk_widget_set_visible (GTK_WIDGET (win->show_users),
				IRCIUM_IS_CHANNEL_BUFFER (buf));
	if (IRCIUM_IS_CHANNEL_BUFFER (buf)) {
		IrciumChannelBuffer *channel_buf = IRCIUM_CHANNEL_BUFFER (buf);
		GListModel *users =
			ircium_channel_buffer_get_users (channel_buf);
		ircium_user_list_popover_set_list (win->user_list_popover,
						   users);
	}
	if (IRCIUM_IS_CHANNEL_BUFFER (buf)) {
		gchar *name = g_strdup_printf ("%s - %s",
					       ircium_connection_get_name
						(win->curr_connection),
					       ircium_buffer_get_title (buf));
		gtk_window_set_title (GTK_WINDOW (win), name);
		g_free (name);

		gtk_header_bar_set_title (win->buffer_view_headerbar,
					  ircium_connection_get_name
						(win->curr_connection));
		gtk_header_bar_set_subtitle (win->buffer_view_headerbar,
					     ircium_buffer_get_title (buf));
	} else {
		gtk_window_set_title (GTK_WINDOW (win),
				      ircium_connection_get_name
					(win->curr_connection));
		gtk_header_bar_set_title (win->buffer_view_headerbar,
					  ircium_connection_get_name
						(win->curr_connection));
	}
	hdy_leaflet_set_visible_child_name (win->chat_leaflet, "buffer-view");
	gtk_widget_set_visible (GTK_WIDGET (win->chat_view_go_back),
				hdy_leaflet_get_fold (win->header_leaflet) ==
				HDY_FOLD_FOLDED);
}

static void
set_topic (IrciumWindow *win,
           IrciumBuffer *buf)
{
	if (!IRCIUM_IS_CHANNEL_BUFFER (buf)) return;
	IrciumChannelBuffer *cb = (IrciumChannelBuffer *) buf;
	gchar *topic = ircium_buffer_get_topic (buf);
	if (*topic == '\0') {
		gtk_label_set_text (win->topic_status, _("No topic set"));
		gtk_label_set_text (win->topic_label, "");
		gtk_label_set_attributes (win->topic_label, NULL);
		gtk_entry_set_text (win->topic_entry, "");
	} else {
		PangoAttrList *list = pango_attr_list_new ();
		gchar *real_topic = ircium_format_parse_code (topic, list);
		gtk_label_set_text (win->topic_label, real_topic);

		gtk_label_set_attributes (win->topic_label, list);
		pango_attr_list_unref (list);

		g_free (real_topic);

		const gchar *setter =
			ircium_channel_buffer_get_topic_setter (cb);
		const GDateTime *set_time =
			ircium_channel_buffer_get_topic_set_time (cb);

		g_autofree gchar *set_time_str = NULL;
		if (set_time != NULL) {
			g_autoptr (GDateTime) local =
				g_date_time_to_local ((GDateTime *) set_time);
			set_time_str = g_date_time_format (local, "%c");
		} else {
			set_time_str = g_strdup ("");
		}
		g_autofree gchar *topic_set =
			g_strdup_printf (_("Topic set by %s on %s"),
					 setter ? setter : "",
					 set_time_str);
		gtk_label_set_text (win->topic_status, topic_set);

		gtk_entry_set_text (win->topic_entry, topic);
	}
}

static void
cert_dialog_cb (GtkDialog *dialog,
                gint       response_id,
                gpointer   user_data);

static gboolean
accept_certificate_cb (GTlsConnection       *conn,
                       GTlsCertificate      *peer_cert,
                       GTlsCertificateFlags  errors,
                       gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *cert_digest = NULL;

	{
		GByteArray* der = NULL;
		g_object_get (peer_cert, "certificate", &der, NULL);
		if (der == NULL) goto out;

		cert_digest = g_compute_checksum_for_data (G_CHECKSUM_SHA512,
							   der->data,
							   der->len);
		g_byte_array_unref (der);
		if (cert_digest == NULL) goto out;
	}

	IrciumWindow *self = user_data;
	for (gsize i = 0; i < self->cert_except_store->len; ++i) {
		gchar *digest = self->cert_except_store->pdata[i];
		if (g_strcmp0 (cert_digest, digest) == 0) {
			ret = TRUE;
			goto out;
		}
	}

	GtkWidget *cert_dialog =
		hdy_dialog_new (GTK_WINDOW (self));
	GtkDialog *dialog = (GtkDialog *) cert_dialog;
	gtk_window_set_title (GTK_WINDOW (dialog), _("Invalid certificate"));
	gtk_dialog_add_buttons (dialog,
				C_("Becaue of invalid cert",
				   "_Disconnect"),
				GTK_RESPONSE_REJECT,
				C_("Invalid cert",
				   "_Accept and Connect"),
				GTK_RESPONSE_ACCEPT,
				NULL);
	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (cert_dialog_cb),
			  self);
	g_object_set_data_full (G_OBJECT (dialog),
				"cert",
				g_strdup (cert_digest), g_free);

	{
		GtkWidget *dialog_headerbar =
			gtk_dialog_get_header_bar (dialog);
		GtkWidget *reject_button =
			gtk_dialog_get_widget_for_response
				(dialog, GTK_RESPONSE_REJECT);

		GValue pack_start = G_VALUE_INIT;
		g_value_init (&pack_start, GTK_TYPE_PACK_TYPE);
		g_value_set_enum (&pack_start, GTK_PACK_START);

		gtk_container_child_set_property
			(GTK_CONTAINER (dialog_headerbar),
			 reject_button,
			 "pack-type",
			 &pack_start);
		g_value_unset (&pack_start);

		gtk_header_bar_set_show_close_button
			(GTK_HEADER_BAR (dialog_headerbar), FALSE);
	}

	GtkVBox *content = (GtkVBox *) gtk_dialog_get_content_area (dialog);
	GtkLabel *info = (GtkLabel *)
		gtk_label_new (_("Unfortunately the server certificate is "
				 "invalid and was rejected."));
	gtk_box_pack_start (GTK_BOX (content),
			    GTK_WIDGET (info),
			    TRUE, TRUE, 0);

	{
		GString *reason_list = g_string_new (NULL);
		if (errors & G_TLS_CERTIFICATE_UNKNOWN_CA) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("The certificate is signed "
						  "by an unknown authority."));
		}
		if (errors & G_TLS_CERTIFICATE_BAD_IDENTITY) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("The certificate doesn't "
						  "match the server's "
						  "identity."));
		}
		if (errors & G_TLS_CERTIFICATE_NOT_ACTIVATED) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("The certificate is not "
						  "active yet."));
		}
		if (errors & G_TLS_CERTIFICATE_EXPIRED) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("The certificate "
						  "has expired."));
		}
		if (errors & G_TLS_CERTIFICATE_REVOKED) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("The certificate "
						  "has been revoked."));
		}
		if (errors & G_TLS_CERTIFICATE_INSECURE) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("The certificate "
						  "uses an insecure "
						  "algorithm."));
		}
		if (errors & G_TLS_CERTIFICATE_GENERIC_ERROR) {
			g_string_append_printf (reason_list,
						"• %s\n",
						_("An error occured "
						  "while validating "
						  "the certificate."));
		}

		GtkLabel *reasons = (GtkLabel *) gtk_label_new (NULL);
		gchar *reason_str = g_string_free (reason_list, FALSE);
		gtk_label_set_markup (reasons, reason_str);
		g_free (reason_str);

		gtk_box_pack_start (GTK_BOX (content),
				    GTK_WIDGET (reasons),
				    TRUE, FALSE, 0);
	}

	{
		GtkWidget *guidance =
			gtk_label_new (_("If you still want to accept "
					 "the certificate, press "
					 "Accept and Connect. "
					 "Otherwise, press Disconnect."));
		GtkLabel *guide = (GtkLabel *) guidance;
		gtk_label_set_line_wrap (guide, TRUE);
		gtk_label_set_line_wrap_mode (guide, PANGO_WRAP_WORD_CHAR);

		gtk_box_pack_start (GTK_BOX (content),
				    guidance,
				    FALSE, FALSE, 0);
	}

	GtkBox *finger_box = (GtkBox *)
		gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);

	GtkLabel *fingerprint_label = (GtkLabel *)
		gtk_label_new (C_("Of the invalid cert",
				  "Fingerprint (SHA-512)"));
	gtk_box_pack_start (finger_box,
			    GTK_WIDGET (fingerprint_label),
			    FALSE, FALSE, 2);

	GtkScrolledWindow *fingerprint_window = (GtkScrolledWindow *)
		gtk_scrolled_window_new (NULL, NULL);
	GtkLabel *fingerprint = (GtkLabel *) gtk_label_new (cert_digest);
	gtk_label_set_selectable (fingerprint, TRUE);

	gtk_container_add (GTK_CONTAINER (fingerprint_window),
			   GTK_WIDGET (fingerprint));

	gtk_box_pack_start (finger_box,
			    GTK_WIDGET (fingerprint_window),
			    TRUE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (content),
			    GTK_WIDGET (finger_box),
			    TRUE, TRUE, 2);

	gtk_widget_show_all (cert_dialog);
out:
	g_free (cert_digest);
	return ret;
}

static void
cert_dialog_cb (GtkDialog *dialog,
                gint       response_id,
                gpointer   user_data)
{
	gchar *digest = NULL;
	IrciumWindow *win = user_data;
	switch (response_id)
	{
	case GTK_RESPONSE_ACCEPT:
		digest = g_object_steal_data (G_OBJECT (dialog), "cert");
		g_ptr_array_add (win->cert_except_store, digest);
		start_connection (win);
		break;
	case GTK_RESPONSE_REJECT:
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	}
	gtk_widget_destroy (GTK_WIDGET (dialog));
	if (response_id == GTK_RESPONSE_ACCEPT) {
		// TODO: Save to the database of accepted certs.
	}
}

static GtkWidget *
create_server_widget (gpointer item,
                      gpointer user_data);

static void
setup_app (IrciumWindow      *self,
           IrciumApplication *app)
{
	GListModel *account_model = ircium_application_get_account_list (app);
	gsize account_num = g_list_model_get_n_items (account_model);

	IrciumWindowState state = account_num > 0 ?
		IRCIUM_WINDOW_STATE_WELCOME_SERVERS :
		IRCIUM_WINDOW_STATE_WELCOME_NOSERVERS;

	ircium_window_change_state (self, state);

	gtk_list_box_bind_model (self->server_list,
				 account_model,
				 create_server_widget,
				 NULL, NULL);
}

static GtkWidget *
create_server_widget (gpointer item,
                      gpointer user_data)
{
	GSettings *account = item;

	IrciumConnection *account_conn =
		ircium_connection_new_from_settings (account);

	GtkWidget *ret =
		gtk_label_new (ircium_connection_get_name (account_conn));
	g_object_set_data_full (G_OBJECT (ret),
				"conn",
				account_conn,
				g_object_unref);

	return ret;
}

static void
on_server_list_row_selected (GtkListBox    *box,
                             GtkListBoxRow *row,
                             gpointer       user_data)
{
	IrciumWindow *win = user_data;

	if (row != NULL) {
		GtkWidget *child = gtk_bin_get_child (GTK_BIN (row));
		IrciumConnection *conn =
			g_object_ref (g_object_get_data (G_OBJECT (child),
							 "conn"));

		set_connection (win, conn);

		gtk_widget_set_sensitive
			(GTK_WIDGET (win->server_list_remove), TRUE);
		gtk_widget_set_sensitive
			(GTK_WIDGET (win->server_list_connect), TRUE);
		gtk_widget_set_sensitive
			(GTK_WIDGET (win->server_list_more_opts), TRUE);
	} else {
		set_connection (win, NULL);

		gtk_widget_set_sensitive
			(GTK_WIDGET (win->server_list_remove), FALSE);
		gtk_widget_set_sensitive
			(GTK_WIDGET (win->server_list_connect), FALSE);
		gtk_widget_set_sensitive
			(GTK_WIDGET (win->server_list_more_opts), FALSE);
	}
}

static void
on_server_list_row_activated (GtkListBox    *box,
                              GtkListBoxRow *row,
                              IrciumWindow  *win)
{
	gtk_button_clicked (win->server_list_connect);
}

static void
on_server_list_connect_clicked (GtkButton    *button,
                                IrciumWindow *win)
{
	start_connection (win);
}

static void
on_server_list_more_opts_clicked (GtkButton    *button,
                                  IrciumWindow *win)
{
	ircium_window_change_state (win, IRCIUM_WINDOW_STATE_SERVER_OPTIONS);
}

static void
on_server_list_remove_clicked (GtkButton    *button,
                               IrciumWindow *win)
{
	IrciumApplication *app = (IrciumApplication *)
		gtk_window_get_application (GTK_WINDOW (win));

	GtkListBoxRow *row = gtk_list_box_get_selected_row (win->server_list);
	GtkWidget *child = gtk_bin_get_child (GTK_BIN (row));
	IrciumConnection *conn =
		g_object_ref (g_object_get_data (G_OBJECT (child), "conn"));
	GSettings *account = ircium_connection_get_settings (conn);
	gtk_list_box_unselect_row (win->server_list, row);

	set_connection (win, NULL);

	ircium_application_remove_account (app, account);
}

static void
on_chat_view_go_back_clicked (IrciumWindow *win,
                              GtkButton    *button)
{
	hdy_leaflet_set_visible_child_name (win->chat_leaflet, "buffer-list");
}

static void
on_topic_edit_toggle_toggled (IrciumWindow    *win,
                              GtkToggleButton *button)
{
	if (gtk_toggle_button_get_active (button)) {
		gtk_stack_set_visible_child_name (win->topic_stack,
						  "topic_edit");
	} else {
		gtk_stack_set_visible_child_name (win->topic_stack,
						  "topic_view");
	}
}

static void
on_show_topic_toggled (IrciumWindow    *win,
                       GtkToggleButton *button)
{
	if (!gtk_toggle_button_get_active (button)) {
		gtk_toggle_button_set_active (win->topic_edit_toggle, FALSE);
	}
}

static void
on_topic_entry_activate (IrciumWindow *win,
                         GtkEntry     *entry)
{
	gtk_toggle_button_set_active (win->topic_edit_toggle, FALSE);
	// We know for a fact that the active buffer is a channel
	// buffer if we're in this situation.
	gchar *curr_channel = ircium_buffer_get_title (win->active_buffer);
	GPtrArray *params = g_ptr_array_new_with_free_func (g_free);

	g_ptr_array_add (params, g_strdup (curr_channel));
	g_ptr_array_add (params, g_strdup (gtk_entry_get_text (entry)));

	IrciumMessage *new_topic = ircium_message_new (NULL,
						       NULL,
						       "TOPIC",
						       params);
	g_ptr_array_unref (params);

	ircium_session_send_message (win->session, new_topic);
}
