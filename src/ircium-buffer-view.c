/* ircium-buffer-view.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-message.h"
#include "ircium-buffer-view.h"
#include "ircium-buffer-private.h"
#include "ircium-full-message-box.h"
#include "ircium-small-message-box.h"

struct _IrciumBufferView
{
	GtkBox parent_instance;

	IrciumBuffer *buf;

	GtkEntry *chat_entry;
	GtkScrolledWindow *scroll_window;
	GtkListBox *chat_view;

	gdouble old_upper;
	gboolean autoscroll;
	GtkAdjustment *vadjust;

	IrciumSession *session;

	gboolean formatting;
};

G_DEFINE_TYPE (IrciumBufferView, ircium_buffer_view, GTK_TYPE_BOX)

enum {
	PROP_0,

	PROP_BUF,
	PROP_SESSION,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void on_vadjust_value_changed (IrciumBufferView *view,
                                      GtkAdjustment    *vadjust);

static void on_vadjust_upper_notify (GObject    *obj,
                                     GParamSpec *pspec,
                                     gpointer    user_data);

static void
on_message_entry_activate (IrciumBufferView *view,
                           GtkEntry         *entry);

IrciumBufferView *
ircium_buffer_view_new (IrciumBuffer  *buf,
                        IrciumSession *session)
{
	return g_object_new (IRCIUM_TYPE_BUFFER_VIEW,
			     "buf", buf,
			     "session", session,
			     NULL);
}

static void
ircium_buffer_view_dispose (GObject *object)
{
	IrciumBufferView *self = (IrciumBufferView *) object;

	g_clear_object (&self->buf);
	g_clear_object (&self->session);

	G_OBJECT_CLASS (ircium_buffer_view_parent_class)->dispose (object);
}

static GtkWidget *
create_view_widget_cb (gpointer item,
                       gpointer user_data);

static void
ircium_buffer_view_constructed (GObject *object)
{
	IrciumBufferView *self = IRCIUM_BUFFER_VIEW (object);

	gtk_list_box_bind_model (self->chat_view,
				 ircium_buffer_get_contents (self->buf),
				 create_view_widget_cb,
				 self, NULL);

	G_OBJECT_CLASS (ircium_buffer_view_parent_class)->constructed (object);
}

static void
ircium_buffer_view_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
	IrciumBufferView *self = IRCIUM_BUFFER_VIEW (object);

	switch (prop_id)
	{
	case PROP_BUF:
		g_value_set_object (value, self->buf);
		break;
	case PROP_SESSION:
		g_value_set_object (value, self->session);
		break;
	default:
		  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_buffer_view_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
	IrciumBufferView *self = IRCIUM_BUFFER_VIEW (object);

	switch (prop_id)
	{
	case PROP_BUF:
		g_clear_object (&self->buf);
		self->buf = g_value_dup_object (value);
		break;
	case PROP_SESSION:
		g_clear_object (&self->session);
		self->session = g_value_dup_object (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static gboolean
on_key_press_event (GtkWidget        *widget,
                    GdkEvent         *event,
                    IrciumBufferView *view);

static void
ircium_buffer_view_class_init (IrciumBufferViewClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	gtk_widget_class_set_template_from_resource
		(widget_class,
		 "/com/gitlab/sham1/Ircium/ircium_buffer_view.ui");

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumBufferView,
					      chat_entry);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumBufferView,
					      scroll_window);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumBufferView,
					      chat_view);
	gtk_widget_class_bind_template_child (widget_class,
					      IrciumBufferView,
					      vadjust);

	gtk_widget_class_bind_template_callback (widget_class,
						 on_message_entry_activate);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_vadjust_value_changed);
	gtk_widget_class_bind_template_callback (widget_class,
						 on_vadjust_upper_notify);

	gtk_widget_class_bind_template_callback (widget_class,
						 on_key_press_event);

	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = ircium_buffer_view_dispose;
	object_class->constructed = ircium_buffer_view_constructed;
	object_class->get_property = ircium_buffer_view_get_property;
	object_class->set_property = ircium_buffer_view_set_property;

	properties[PROP_BUF] =
		g_param_spec_object ("buf",
				     "buf",
				     "The buffer",
				     IRCIUM_TYPE_BUFFER,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_SESSION] =
		g_param_spec_object ("session",
				     "session",
				     "The session",
				     IRCIUM_TYPE_SESSION,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_buffer_view_init (IrciumBufferView *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
	self->autoscroll = TRUE;
	self->old_upper = gtk_adjustment_get_upper (self->vadjust);
}

static gboolean
is_mention (IrciumBufferView *self,
            GtkWidget        *widget,
            IrciumBufferLine *line);

static GtkWidget *
create_view_widget_cb (gpointer item,
                       gpointer user_data)
{
	IrciumBufferLine *line = item;
	IrciumBufferView *self = user_data;

	GtkWidget *ret = NULL;

	if (ircium_buffer_line_is_sender_known (line) &&
	    ircium_buffer_line_is_sender_user (line)) {
		switch (ircium_buffer_line_get_line_type (line)) {
		case IRCIUM_BUFFER_LINE_TYPE_NORMAL:
			if ((ircium_buffer_line_get_flags (line) &
			     IRCIUM_BUFFER_LINE_FLAG_ACTION) != 0) {
				ret = ircium_small_message_box_new (line);
			}
			else if ((ircium_buffer_line_get_flags (line) &
			    IRCIUM_BUFFER_LINE_FLAG_CONTINUATION) == 0) {
				ret = ircium_full_message_box_new (line);
			} else {
				ret = ircium_small_message_box_new (line);
			}
			break;
		default:
			ret = ircium_small_message_box_new (line);
			break;
		}
	}

	if (ret == NULL) {
		ret = ircium_small_message_box_new (line);
	}

	GtkStyleContext *style_ctx = gtk_widget_get_style_context (ret);
	if (is_mention (self, ret, line)) {
		gtk_style_context_add_class (style_ctx, "mention");
	}
	if ((ircium_buffer_line_get_flags (line) &
	     IRCIUM_BUFFER_LINE_FLAG_NOTICE) != 0) {
		gtk_style_context_add_class (style_ctx, "notice");
	}

	gtk_widget_show_all (ret);

	return ret;
}

static void
on_message_entry_activate (IrciumBufferView *view,
                           GtkEntry         *entry)
{
	g_assert (IRCIUM_IS_BUFFER_VIEW (view));
	g_assert (GTK_IS_ENTRY (entry));
	guint16 len = gtk_entry_get_text_length (entry);
	if (len > 0) {
		gchar *msg = g_strdup (gtk_entry_get_text (entry));
		gtk_entry_set_text (entry, "");

		ircium_buffer_handle_entry (view->buf, msg);

		g_free (msg);
	}
}

static void
on_vadjust_value_changed (IrciumBufferView *view,
                          GtkAdjustment    *vadjust)
{
	// We'll set the autoscroll when we're at the bottom.
	gdouble bottom = gtk_adjustment_get_upper (vadjust) -
		gtk_adjustment_get_page_size (vadjust);
	view->autoscroll = gtk_adjustment_get_value (vadjust) == bottom;
}

static void
on_vadjust_upper_notify (GObject    *obj,
                         GParamSpec *pspec,
                         gpointer    user_data)
{
	IrciumBufferView *view = user_data;
	GtkAdjustment *adjustment = (GtkAdjustment *) obj;

	gdouble new_upper = gtk_adjustment_get_upper (adjustment);
	gdouble diff = new_upper - view->old_upper;
	if (diff != 0.0) {
		// If we're autoscrolling, stay in place.
		view->old_upper = new_upper;
		if (view->autoscroll) {
			gdouble new_val =
				gtk_adjustment_get_upper (adjustment) -
				gtk_adjustment_get_page_size (adjustment);
			gtk_adjustment_set_value (adjustment, new_val);
		}
	}
}

IrciumBuffer *
ircium_buffer_view_get_buffer (IrciumBufferView *view)
{
	g_return_val_if_fail (IRCIUM_IS_BUFFER_VIEW (view), NULL);
	return view->buf;
}

static gboolean
is_mention (IrciumBufferView *self,
            GtkWidget        *widget,
            IrciumBufferLine *line)
{
	if (ircium_buffer_line_get_line_type (line) !=
	    IRCIUM_BUFFER_LINE_TYPE_NORMAL) return FALSE;

	const gchar *nick = ircium_session_get_nick (self->session);
	if (IRCIUM_IS_FULL_MESSAGE_BOX (widget)) {
		IrciumFullMessageBox *box = (IrciumFullMessageBox *) widget;
		const gchar *content =
			ircium_full_message_box_get_content (box);
		return strstr (content, nick) != NULL;
	}
	if (IRCIUM_IS_SMALL_MESSAGE_BOX (widget)) {
		IrciumSmallMessageBox *box = (IrciumSmallMessageBox *) widget;
		const gchar *content =
			ircium_small_message_box_get_content (box);
		return strstr (content, nick) != NULL;
	}
	return FALSE;
}

static gboolean
on_key_press_event (GtkWidget        *widget,
                    GdkEvent         *event,
                    IrciumBufferView *view)
{
	GdkEventKey key_event = event->key;
	if ((key_event.state & GDK_CONTROL_MASK) != 0 &&
	    (key_event.keyval == GDK_KEY_f || key_event.keyval == GDK_KEY_F)) {
		view->formatting = !view->formatting;
		return TRUE;
	}

	gboolean ret = FALSE;
	if (view->formatting) {
		GtkEditable *editable = GTK_EDITABLE (widget);
		gint pos = gtk_editable_get_position (editable);
		switch (key_event.keyval)
		{
		case GDK_KEY_B:
		case GDK_KEY_b:
			gtk_editable_insert_text (editable, "\x02", -1, &pos);
			ret = TRUE;
			break;
		case GDK_KEY_I:
		case GDK_KEY_i:
			gtk_editable_insert_text (editable, "\x1D", -1, &pos);
			ret = TRUE;
			break;
		case GDK_KEY_U:
		case GDK_KEY_u:
			gtk_editable_insert_text (editable, "\x1F", -1, &pos);
			ret = TRUE;
			break;
		case GDK_KEY_S:
		case GDK_KEY_s:
			gtk_editable_insert_text (editable, "\x1E", -1, &pos);
			ret = TRUE;
			break;
		case GDK_KEY_M:
		case GDK_KEY_m:
			gtk_editable_insert_text (editable, "\x11", -1, &pos);
			ret = TRUE;
			break;
		case GDK_KEY_C:
		case GDK_KEY_c:
			gtk_editable_insert_text (editable, "\x03", -1, &pos);
			ret = TRUE;
			break;
		case GDK_KEY_R:
		case GDK_KEY_r:
			gtk_editable_insert_text (editable, "\x0F", -1, &pos);
			ret = TRUE;
			break;
		default:
			break;
		}
		gtk_editable_set_position (editable, pos);
		view->formatting = FALSE;
	}
	return ret;
}
