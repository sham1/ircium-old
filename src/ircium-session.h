/* ircium-session.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "ircium-message.h"
#include "ircium-server-buffer.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_SESSION (ircium_session_get_type())

G_DECLARE_FINAL_TYPE (IrciumSession, ircium_session, IRCIUM, SESSION, GObject)

void ircium_session_send_message (IrciumSession *session, IrciumMessage *msg);

IrciumServerBuffer *ircium_session_get_server_buffer (IrciumSession *session);
GListModel *ircium_session_get_buffer_list (IrciumSession *session);

IrciumBuffer *ircium_session_find_buffer (IrciumSession *session,
                                          const gchar   *name);
const IrciumBuffer *ircium_session_get_active_buffer (IrciumSession *session);
void ircium_session_set_active_buffer (IrciumSession *session,
                                       IrciumBuffer  *buffer);

const gchar *ircium_session_get_nick (IrciumSession *session);
void ircium_session_set_nick (IrciumSession *session,
                              const gchar   *nick);

GListModel *ircium_session_get_users (IrciumSession *session);

G_END_DECLS
