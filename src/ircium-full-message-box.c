/* ircium-full-message-box.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-full-message-box.h"
#include "ircium-format.h"

struct _IrciumFullMessageBox
{
	GtkGrid parent_instance;

	IrciumBufferLine *line;

	GtkLabel *sender;
	GtkLabel *time;
	GtkLabel *content;
};

G_DEFINE_TYPE (IrciumFullMessageBox, ircium_full_message_box, GTK_TYPE_GRID)

enum {
	PROP_0,

	PROP_LINE,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
ircium_full_message_box_new (IrciumBufferLine *line)
{
	return g_object_new (IRCIUM_TYPE_FULL_MESSAGE_BOX,
			     "line", line,
			     NULL);
}

static void
ircium_full_message_box_dispose (GObject *object)
{
	IrciumFullMessageBox *self = (IrciumFullMessageBox *)object;

	g_clear_object (&self->line);

	G_OBJECT_CLASS (ircium_full_message_box_parent_class)->dispose (object);
}

static void
ircium_full_message_box_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
	IrciumFullMessageBox *self = IRCIUM_FULL_MESSAGE_BOX (object);

	switch (prop_id)
	{
	case PROP_LINE:
		g_value_set_object (value, self->line);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_full_message_box_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
	IrciumFullMessageBox *self = IRCIUM_FULL_MESSAGE_BOX (object);

	switch (prop_id)
	{
	case PROP_LINE:
		self->line = g_value_dup_object (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_full_message_box_constructed (GObject *object);

static void
ircium_full_message_box_class_init (IrciumFullMessageBoxClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	gtk_widget_class_set_template_from_resource
		(widget_class,
		 "/com/gitlab/sham1/Ircium/ircium_full_message_box.ui");

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumFullMessageBox,
					      sender);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumFullMessageBox,
					      time);

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumFullMessageBox,
					      content);

	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->constructed = ircium_full_message_box_constructed;
	object_class->dispose = ircium_full_message_box_dispose;
	object_class->get_property = ircium_full_message_box_get_property;
	object_class->set_property = ircium_full_message_box_set_property;

	properties[PROP_LINE] =
		g_param_spec_object ("line",
				     "line",
				     "The buffer line",
				     IRCIUM_TYPE_BUFFER_LINE,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_full_message_box_init (IrciumFullMessageBox *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
}

static void
ircium_full_message_box_constructed (GObject *object)
{
	IrciumFullMessageBox *self = (IrciumFullMessageBox *)object;

	const gchar *sender = ircium_buffer_line_get_sender_nick (self->line);
	const GDateTime *send_time = ircium_buffer_line_get_time (self->line);
	g_autoptr (GDateTime) local_send_time =
		g_date_time_to_local ((GDateTime *) send_time);

	gchar *formatted_time = g_date_time_format (local_send_time, "[%X]");
	const gchar *contents = ircium_buffer_line_get_contents (self->line);

	PangoAttrList *list = pango_attr_list_new ();
	gchar *real_contents = ircium_format_parse_code (contents, list);

	gtk_label_set_text (self->time, formatted_time);
	gtk_label_set_text (self->content, real_contents);
	gtk_label_set_text (self->sender, sender);

	gtk_label_set_attributes (self->content, list);
	pango_attr_list_unref (list);

	g_free (real_contents);
	g_free (formatted_time);
	G_OBJECT_CLASS (ircium_full_message_box_parent_class)->constructed
		(object);
}

const gchar *
ircium_full_message_box_get_content (IrciumFullMessageBox *box)
{
	return gtk_label_get_text (box->content);
}
