/* ircium-connection.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "ircium-session.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_CONNECTION (ircium_connection_get_type())

G_DECLARE_FINAL_TYPE (IrciumConnection,
		      ircium_connection,
		      IRCIUM, CONNECTION,
		      GObject)

typedef struct _IrciumConnectionAuthInfo IrciumConnectionAuthInfo;

IrciumConnection *ircium_connection_new (void);

IrciumConnection *ircium_connection_new_from_settings (GSettings *account);

void ircium_connection_connect_async (IrciumConnection    *conn,
                                      GCancellable        *conn_cancellable,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data);

IrciumSession *ircium_connection_connect_finish (IrciumConnection  *conn,
                                                 GAsyncResult      *result,
                                                 GError           **error);

typedef gboolean (*AcceptCertCB) (GTlsConnection       *conn,
                                  GTlsCertificate      *peer_cert,
                                  GTlsCertificateFlags  errors,
                                  gpointer              user_data);

typedef enum {
	IRCIUM_CONNECTION_USER_AUTH_TYPE_NONE,
	IRCIUM_CONNECTION_USER_AUTH_TYPE_SASL,
	IRCIUM_CONNECTION_USER_AUTH_TYPE_NICKSERV,
} IrciumConnectionUserAuthType;

IrciumConnectionUserAuthType ircium_connection_get_user_auth_type
	(IrciumConnection *conn);

gboolean ircium_connection_get_sasl_details (IrciumConnection  *conn,
                                             gchar            **user,
                                             gchar            **pass);
gboolean ircium_connection_get_nickserv_details (IrciumConnection  *conn,
                                                 gchar            **user,
                                                 gchar            **pass);

void ircium_connection_remove_user_auth (IrciumConnection *conn);
void ircium_connection_make_user_auth_sasl (IrciumConnection *conn,
                                            gchar            *user,
                                            gchar            *pass);
void ircium_connection_make_user_auth_nickserv (IrciumConnection *conn,
                                                gchar            *user,
                                                gchar            *pass);

gboolean ircium_connection_get_server_password (IrciumConnection  *conn,
                                                gchar            **pass);
void ircium_connection_set_server_password (IrciumConnection *conn,
                                            const gchar      *pass);
void ircium_connection_set_server_has_password (IrciumConnection *conn);
void ircium_connection_unset_server_has_password (IrciumConnection *conn);

void ircium_connection_set_port (IrciumConnection *conn,
                                 guint16           port);
void ircium_connection_unset_port (IrciumConnection *conn);
gboolean ircium_connection_is_port_set (IrciumConnection *conn);
guint16 ircium_connection_get_port (IrciumConnection *conn);

void ircium_connection_set_name  (IrciumConnection *conn,
                                  const gchar      *name);
const gchar *ircium_connection_get_name (IrciumConnection *conn);

void ircium_connection_set_address  (IrciumConnection *conn,
                                     const gchar      *address);
const gchar *ircium_connection_get_address (IrciumConnection *conn);

void ircium_connection_set_nickname  (IrciumConnection *conn,
                                      const gchar      *nickname);
const gchar *ircium_connection_get_nickname (IrciumConnection *conn);

void ircium_connection_set_username  (IrciumConnection *conn,
                                      const gchar      *username);
const gchar *ircium_connection_get_username (IrciumConnection *conn);

void ircium_connection_set_realname  (IrciumConnection *conn,
                                      const gchar      *realname);
const gchar *ircium_connection_get_realname (IrciumConnection *conn);

void ircium_connection_set_tls (IrciumConnection *conn,
                                gboolean          tls);
gboolean ircium_connection_get_tls (IrciumConnection *conn);

gboolean ircium_connection_is_minimum_viable (IrciumConnection *conn);

void ircium_connection_set_accept_cert_handler
	(IrciumConnection *conn,
	 AcceptCertCB      handler,
	 gpointer          user_data,
	 GDestroyNotify    user_data_free);

GSettings *ircium_connection_get_settings (IrciumConnection *conn);
void ircium_connection_set_settings (IrciumConnection *conn,
                                     GSettings        *account);

G_END_DECLS
