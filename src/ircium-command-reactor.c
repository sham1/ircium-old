/* ircium-command-reactor.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-command-reactor-private.h"
#include "ircium-message-tag.h"
#include "ircium-basic-command-handler.h"

struct _IrciumCommandReactor
{
	GObject parent_instance;

	IrciumSession *session;

	GPtrArray *handlers;

	IrciumCommandHandler **handler_instances;
};

enum {
	BASIC_HANDLER_CLASS,

	N_HANDLER_CLASSES,
};

G_DEFINE_TYPE (IrciumCommandReactor, ircium_command_reactor, G_TYPE_OBJECT)

IrciumCommandReactor *
ircium_command_reactor_new (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	IrciumCommandReactor *reactor =
		g_object_new (IRCIUM_TYPE_COMMAND_REACTOR, NULL);
	reactor->session = session;
	return reactor;
}

static void
ircium_command_reactor_dispose (GObject *object)
{
	IrciumCommandReactor *self = (IrciumCommandReactor *)object;

	if (self->handler_instances != NULL) {
		for (gsize i = 0; i < N_HANDLER_CLASSES; ++i) {
			IrciumCommandHandler *handler =
				self->handler_instances[i];
			ircium_command_handler_unregister_handlers (handler,
								    self);
			g_object_unref (handler);
		}
		g_free (self->handler_instances);
		self->handler_instances = NULL;
	}

	G_OBJECT_CLASS (ircium_command_reactor_parent_class)->dispose (object);
}

static void
ircium_command_reactor_class_init (IrciumCommandReactorClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = ircium_command_reactor_dispose;
}

static void
free_handler (IrciumCommandReactorHandler *handler)
{
	g_object_unref (handler->handler);
	if (handler->user_data_free != NULL)
		handler->user_data_free (handler->user_data);
}

static void
ircium_command_reactor_init (IrciumCommandReactor *self)
{
	self->handlers =
		g_ptr_array_new_with_free_func ((GDestroyNotify) free_handler);

	self->handler_instances =
		g_malloc0_n (N_HANDLER_CLASSES,
			     sizeof (*self->handler_instances));

	self->handler_instances[BASIC_HANDLER_CLASS] =
		IRCIUM_COMMAND_HANDLER (ircium_basic_command_handler_new ());

	for (gsize i = 0; i < N_HANDLER_CLASSES; ++i) {
		IrciumCommandHandler *handler =
		self->handler_instances[i];
		ircium_command_handler_register_handlers (handler, self);
	}
}

void
ircium_command_reactor_react (IrciumCommandReactor *reactor,
                              IrciumMessage        *msg)
{
	gsize i = 0;
	for (; i < reactor->handlers->len; ++i) {
		IrciumCommandReactorHandler *handler =
			reactor->handlers->pdata[i];
		if (handler->predicate (msg, handler->user_data)) {
			break;
		}
	}
	IrciumCommandReactorHandler *handler = reactor->handlers->pdata[i];
	handler->callback (handler->handler, reactor, msg, handler->user_data);
}

static gint
reactor_sort_predicate (const IrciumCommandReactorHandler **a,
			const IrciumCommandReactorHandler **b)
{
	const IrciumCommandReactorHandler *a_handler = *a;
	const IrciumCommandReactorHandler *b_handler = *b;

	if (a_handler->priority < b_handler->priority) return -1;
	if (a_handler->priority > b_handler->priority) return 1;
	return 0;
}

gboolean
ircium_command_reactor_register (IrciumCommandReactor        *reactor,
                                 IrciumCommandReactorHandler *handler)
{
	g_return_val_if_fail (handler != NULL, FALSE);
	g_return_val_if_fail (handler->handler != NULL, FALSE);
	g_return_val_if_fail (handler->predicate != NULL, FALSE);

	g_ptr_array_add (reactor->handlers, handler);
	g_ptr_array_sort (reactor->handlers,
			  (GCompareFunc) reactor_sort_predicate);
	return TRUE;
}

void
ircium_command_reactor_unregister (IrciumCommandReactor        *reactor,
                                   IrciumCommandReactorHandler *handler)
{
	g_return_if_fail (IRCIUM_IS_COMMAND_REACTOR (reactor));
	g_return_if_fail (handler != NULL);
	for (gsize i = 0; i < reactor->handlers->len; ++i) {
		IrciumCommandReactorHandler *iter = reactor->handlers->pdata[i];
		if (iter == handler) {
			g_ptr_array_remove_index (reactor->handlers, i);
			return;
		}
	}
}

IrciumCommandReactorHandler *
ircium_command_new_reactor_handler (gint                     priority,
                                    CommandHandlerPredicate  predicate,
                                    IrciumCommandHandler    *handler,
                                    CommandHandlerCB         callback,
                                    gpointer                 user_data,
                                    GDestroyNotify           user_data_free)
{
	g_return_val_if_fail (predicate != NULL, NULL);
	g_return_val_if_fail (handler != NULL, NULL);
	g_return_val_if_fail (callback != NULL, NULL);

	IrciumCommandReactorHandler *ret = g_malloc0 (sizeof (*ret));

	ret->priority = priority;
	ret->predicate = predicate;
	ret->handler = g_object_ref (handler);
	ret->callback = callback;
	ret->user_data = user_data;
	ret->user_data_free = user_data_free;

	return ret;
}

IrciumSession *
ircium_command_reactor_get_session (IrciumCommandReactor *reactor)
{
	g_return_val_if_fail (IRCIUM_IS_COMMAND_REACTOR (reactor), NULL);
	return reactor->session;
}
