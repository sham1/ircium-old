/* ircium-format.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-format.h"

// TODO: Have better colour code database
#define o(r, g, b)\
	{((r & 0xFF) / (gdouble) 0xFF), \
	 ((g & 0xFF) / (gdouble) 0xFF), \
	 ((b & 0xFF) / (gdouble) 0xFF), \
	}
struct colour {
	gdouble r;
	gdouble g;
	gdouble b;
};

static struct colour colours[99] = {
	o (0xFF, 0xFF, 0xFF),
	o (0x00, 0x00, 0x00),
	o (0x00, 0x00, 0x7F),
	o (0x00, 0x93, 0x00),
	o (0xFF, 0x00, 0x00),
	o (0x7F, 0x00, 0x00),
	o (0x9C, 0x00, 0x9C),
	o (0xFC, 0x7F, 0x00),
	o (0xFF, 0xFF, 0x00),
	o (0x00, 0xFC, 0x00),
	o (0x00, 0x93, 0x93),
	o (0x00, 0xFF, 0xFF),
	o (0x00, 0x00, 0xFC),
	o (0xFF, 0x00, 0xFF),
	o (0x7F, 0x7F, 0x7F),
	o (0xD2, 0xD2, 0xD2),
	o (0xD2, 0xD2, 0xD2),
};
#undef o

static ptrdiff_t
parse_colour (const gchar     *iter,
              PangoAttrList   *list,
              PangoAttribute **foreground,
              PangoAttribute **background,
              gsize            i);

static ptrdiff_t
get_colour_code (const gchar *iter,
                 gint        *colour_num);

gchar *
ircium_format_parse_code (const gchar   *str,
                          PangoAttrList *attrs)
{
	GString *builder = g_string_new (NULL);
	gsize i = 0;

	PangoAttribute *bold_attr = NULL;
	PangoAttribute *italics_attr = NULL;
	PangoAttribute *underline_attr = NULL;
	PangoAttribute *strikethrough_attr = NULL;
	PangoAttribute *monospace_attr = NULL;

	PangoAttribute *foreground_colour_attr = NULL;
	PangoAttribute *background_colour_attr = NULL;

	for (const gchar *iter = str; *iter != '\0'; ++iter) {
		gchar c = *iter;
		switch (c) {
		case 0x02:
			if (bold_attr == NULL) {
				bold_attr = pango_attr_weight_new
					(PANGO_WEIGHT_BOLD);
				bold_attr->start_index = i;
			} else {
				bold_attr->end_index = i;
				pango_attr_list_insert (attrs, bold_attr);
				bold_attr = NULL;
			}
			break;
		case 0x1D:
			if (italics_attr == NULL) {
				italics_attr = pango_attr_style_new
					(PANGO_STYLE_ITALIC);
				italics_attr->start_index = i;
			} else {
				italics_attr->end_index = i;
				pango_attr_list_insert (attrs, italics_attr);
				italics_attr = NULL;
			}
			break;
		case 0x1F:
			if (underline_attr == NULL) {
				underline_attr = pango_attr_underline_new
					(PANGO_UNDERLINE_SINGLE);
				underline_attr->start_index = i;
			} else {
				underline_attr->end_index = i;
				pango_attr_list_insert (attrs, underline_attr);
				underline_attr = NULL;
			}
			break;
		case 0x1E:
			if (strikethrough_attr == NULL) {
				strikethrough_attr =
					pango_attr_strikethrough_new (TRUE);
				strikethrough_attr->start_index = i;
			} else {
				strikethrough_attr->end_index = i;
				pango_attr_list_insert (attrs,
							strikethrough_attr);
				strikethrough_attr = NULL;
			}
			break;
		case 0x11:
			if (monospace_attr == NULL) {
				monospace_attr =
					pango_attr_family_new ("monospace");
				monospace_attr->start_index = i;
			} else {
				monospace_attr->end_index = i;
				pango_attr_list_insert (attrs,
							monospace_attr);
				monospace_attr = NULL;
			}
			break;
		case 0x03:
			iter += parse_colour (iter,
					      attrs,
					      &foreground_colour_attr,
					      &background_colour_attr,
					      i);
			break;
		case 0x16:
			// If someone wants to deal with reverse colour,
			// be my guest.
			break;
		case 0x0F:
			if (bold_attr != NULL) {
				bold_attr->end_index = i;
				pango_attr_list_insert (attrs, bold_attr);
				bold_attr = NULL;
			}
			if (italics_attr != NULL) {
				italics_attr->end_index = i;
				pango_attr_list_insert (attrs, italics_attr);
				italics_attr = NULL;
			}
			if (underline_attr != NULL) {
				underline_attr->end_index = i;
				pango_attr_list_insert (attrs, underline_attr);
				underline_attr = NULL;
			}
			if (strikethrough_attr != NULL) {
				strikethrough_attr->end_index = i;
				pango_attr_list_insert (attrs,
							strikethrough_attr);
				strikethrough_attr = NULL;
			}
			if (monospace_attr != NULL) {
				monospace_attr->end_index = i;
				pango_attr_list_insert (attrs,
							monospace_attr);
				monospace_attr = NULL;
			}

			if (foreground_colour_attr != NULL) {
				foreground_colour_attr->end_index = i;
				pango_attr_list_insert (attrs,
							foreground_colour_attr);
				foreground_colour_attr = NULL;
			}
			if (background_colour_attr != NULL) {
				background_colour_attr->end_index = i;
				pango_attr_list_insert (attrs,
							background_colour_attr);
				background_colour_attr = NULL;
			}
			break;
		default:
			g_string_append_c (builder, c);
			++i;
			break;
		}
	}
	if (bold_attr != NULL) {
		pango_attr_list_insert (attrs, bold_attr);
		bold_attr = NULL;
	}
	if (italics_attr != NULL) {
		pango_attr_list_insert (attrs, italics_attr);
		italics_attr = NULL;
	}
	if (underline_attr != NULL) {
		pango_attr_list_insert (attrs, underline_attr);
		underline_attr = NULL;
	}
	if (strikethrough_attr != NULL) {
		pango_attr_list_insert (attrs, strikethrough_attr);
		strikethrough_attr = NULL;
	}
	if (monospace_attr != NULL) {
		pango_attr_list_insert (attrs, monospace_attr);
		monospace_attr = NULL;
	}

	if (foreground_colour_attr != NULL) {
		pango_attr_list_insert (attrs, foreground_colour_attr);
		foreground_colour_attr = NULL;
	}
	if (background_colour_attr != NULL) {
		pango_attr_list_insert (attrs, background_colour_attr);
		background_colour_attr = NULL;
	}

	return g_string_free (builder, FALSE);
}

static ptrdiff_t
get_colour_code (const gchar *iter,
                 gint        *colour_num)
{
	++iter;
	gchar c = *iter;
	ptrdiff_t offset = 0;
	gint ret = *colour_num;
	if (c != '\0' && g_ascii_isdigit (c)) {
		ret = c - '0';

		c = *++iter;

		++offset;
		if (c != '\0' && g_ascii_isdigit (c)) {
			ret = ret * 10 + (c - '0');
			++offset;
		}
		*colour_num = ret;
	}
	return offset;
}

static ptrdiff_t
parse_colour (const gchar     *iter,
              PangoAttrList   *list,
              PangoAttribute **foreground,
              PangoAttribute **background,
              gsize            i)
{
	const gchar *head = iter;

	ptrdiff_t ret = 0;

	gint foreground_index = -1;
	ret += get_colour_code (head, &foreground_index);
	head = iter + ret;
	if (*foreground != NULL) {
		(*foreground)->end_index = i;
		pango_attr_list_insert (list, *foreground);
		*foreground = NULL;
	}
	if (foreground_index == -1) {
		if (*background != NULL) {
			(*background)->end_index = i;
			pango_attr_list_insert (list, *background);
			*background = NULL;
		}
		return ret;
	} else if (foreground_index != 99) {
		struct colour colour = colours[foreground_index];
		guint16 red = colour.r * G_MAXUINT16;
		guint16 green = colour.g * G_MAXUINT16;
		guint16 blue = colour.b * G_MAXUINT16;
		(*foreground) = pango_attr_foreground_new (red, green, blue);
		(*foreground)->start_index = i;
	}

	if (*(head + 1) == ',') {
		++head;
		gint background_index = -1;
		ret += get_colour_code (head, &background_index);
		if (background_index != -1) {
			++ret;
			if (*background != NULL) {
				(*background)->end_index = i;
				pango_attr_list_insert (list, *background);
				(*background) = NULL;
			}
			if (background_index != 99) {
				struct colour colour =
					colours[background_index];

				guint16 red = colour.r * G_MAXUINT16;
				guint16 green = colour.g * G_MAXUINT16;
				guint16 blue = colour.b * G_MAXUINT16;

				(*background) =
					pango_attr_background_new (red,
								   green,
								   blue);
				(*background)->start_index = i;
			}
		}
	}

	return ret;
}
