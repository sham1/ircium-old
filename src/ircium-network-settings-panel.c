/* ircium-network-settings-panel.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-network-settings-panel.h"

struct _IrciumNetworkSettingsPanel
{
	GtkBox parent_instance;

	IrciumWindow *win;
	IrciumConnection *conn;

	gulong win_notify_connection_handler;

	GtkEntry *setting_network_name_entry;

	GtkRevealer *setting_server_tls_notls_revealer;
	GtkRevealer *setting_server_password_revealer;
	GtkEntry *setting_server_address_entry;
	GtkEntry *setting_server_port_entry;
	GtkSwitch *setting_server_tls_switch;
	GtkSwitch *setting_server_tls_starttls;
	GtkSwitch *setting_server_password_switch;
	GtkEntry *setting_server_password_entry;

	GtkEntry *setting_user_nickname_entry;
	GtkEntry *setting_user_username_entry;
	GtkEntry *setting_user_realname_entry;
	GtkRadioButton *setting_user_auth_nothing;
	GtkRadioButton *setting_user_auth_sasl;
	GtkRadioButton *setting_user_auth_nickserv;
	GtkLabel *setting_user_auth_label;

	GtkRevealer *setting_user_auth_details_revealer;
	GtkStack *setting_user_auth_details_stack;

	gulong connection_name_change_handler;
	gulong connection_port_change_handler;
	gulong connection_address_change_handler;
	gulong connection_tls_change_handler;
	gulong connection_pass_change_handler;
	gulong connection_username_change_handler;
	gulong connection_nickname_change_handler;
	gulong connection_realname_change_handler;
};

G_DEFINE_TYPE (IrciumNetworkSettingsPanel,
	       ircium_network_settings_panel,
	       GTK_TYPE_BOX)

static void ircium_network_settings_panel_disconnect_all_signals
	(IrciumNetworkSettingsPanel *self);
static void ircium_network_settings_panel_disconnect_window_signal
	(IrciumNetworkSettingsPanel *self);
static void ircium_network_settings_panel_disconnect_connection_signals
	(IrciumNetworkSettingsPanel *self);

static void ircium_network_settings_panel_connect_connection_signals
	(IrciumNetworkSettingsPanel *self);

static void
ircium_network_settings_panel_dispose (GObject *obj)
{
	IrciumNetworkSettingsPanel *self = (IrciumNetworkSettingsPanel *) obj;

	ircium_network_settings_panel_disconnect_all_signals (self);

	g_clear_object (&self->win);
	g_clear_object (&self->conn);

	G_OBJECT_CLASS (ircium_network_settings_panel_parent_class)->
		dispose (obj);
}

IrciumNetworkSettingsPanel *
ircium_network_settings_panel_new (void)
{
	return g_object_new (IRCIUM_TYPE_NETWORK_SETTINGS_PANEL, NULL);
}

static void
on_setting_server_tls_active_notify (IrciumNetworkSettingsPanel *self,
                                     GParamSpec                 *pspec,
                                     GtkSwitch                  *sw)
{
	gtk_revealer_set_reveal_child
		(self->setting_server_tls_notls_revealer,
		 !gtk_switch_get_active (sw));
	ircium_connection_set_tls (self->conn, gtk_switch_get_active (sw));
}

static void
on_setting_server_password_active_notify (IrciumNetworkSettingsPanel *self,
                                          GParamSpec                 *pspec,
                                          GtkSwitch                  *sw)
{
	gboolean is_active = gtk_switch_get_active (sw);
	gtk_revealer_set_reveal_child
		(self->setting_server_password_revealer, is_active);
	if (is_active) {
		ircium_connection_set_server_has_password (self->conn);
	} else {
		ircium_connection_unset_server_has_password (self->conn);
		gsize len = gtk_entry_get_text_length
			(self->setting_server_password_entry);
		if (len > 0) {
			gtk_entry_set_text (self->setting_server_password_entry,
					    "");
		}
	}
}

static void
on_setting_user_auth_changed (IrciumNetworkSettingsPanel *self,
                              GtkToggleButton            *button)
{
	if (gtk_toggle_button_get_active (button)) {
		if (gtk_toggle_button_get_active (
		    GTK_TOGGLE_BUTTON (self->setting_user_auth_nothing))) {
			gtk_label_set_text (self->setting_user_auth_label,
					    "Nothing");
			gtk_revealer_set_reveal_child
			    (self->setting_user_auth_details_revealer,
			     FALSE);
		}
		if (gtk_toggle_button_get_active (
		    GTK_TOGGLE_BUTTON (self->setting_user_auth_sasl))) {
			gtk_label_set_text (self->setting_user_auth_label,
					    "SASL (PLAIN)");
			gtk_revealer_set_reveal_child
			    (self->setting_user_auth_details_revealer,
			     TRUE);
			gtk_stack_set_visible_child_name
			    (self->setting_user_auth_details_stack,
			     "setting_user_auth_sasl");
		}
		if (gtk_toggle_button_get_active (
		    GTK_TOGGLE_BUTTON (self->setting_user_auth_nickserv))) {
			gtk_label_set_text (self->setting_user_auth_label,
					    "NickServ");
			gtk_revealer_set_reveal_child
			    (self->setting_user_auth_details_revealer,
			     TRUE);
			gtk_stack_set_visible_child_name
			    (self->setting_user_auth_details_stack,
			     "setting_user_auth_nickserv");
		}
	}
}

static void
on_setting_server_port_entry_changed (IrciumNetworkSettingsPanel *self,
                                      GtkEditable                *editable)
{
	GtkEntry *port_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (port_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (port_entry));

	GtkStyleContext *ctx =
		gtk_widget_get_style_context (GTK_WIDGET (port_entry));

	guint16 text_len = gtk_entry_get_text_length (port_entry);
	if (text_len == 0) {
		// Even though we are removing the error style,
		// we will still unset the port.
		if (gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_remove_class (ctx, "error");
		}
		ircium_connection_unset_port (self->conn);
		return;
	}

	const gchar *content = gtk_entry_get_text (port_entry);
	if (!g_utf8_validate (content, -1, NULL)) {
		if (!gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_add_class (ctx, "error");
		}
		return;
	}
	gboolean valid = TRUE;
	glong content_len = g_utf8_strlen (content, -1);
	gulong assumed_port_num = 0;
	for (glong i = 0; i < content_len; ++i) {
		gunichar c = g_utf8_get_char (content);
		content = g_utf8_next_char (content);

		if (!g_unichar_isdigit (c)) {
			valid = FALSE;
			break;
		}
		assumed_port_num =
			assumed_port_num * 10 + g_unichar_digit_value (c);
		// The port will be invalid if the digits give us a larger
		// number than 2^16, since (2^16) - 1 is the largest possible
		// port number because ports are 16-bit unsigned.
		if (assumed_port_num > G_MAXUINT16) {
			valid = FALSE;
			break;
		}
	}
	// Port 0 also cannot be used because it's reserved.
	if (assumed_port_num == 0) valid = FALSE;
	if (valid) {
		if (gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_remove_class (ctx, "error");
		}

		if (should_matter) {
			guint16 port_num = (guint16) assumed_port_num;
			ircium_connection_set_port (self->conn,
						    port_num);
		}
	} else {
		if (!gtk_style_context_has_class (ctx, "error")) {
			gtk_style_context_add_class (ctx, "error");
		}
		// We unset the connection port to indicate an invalid state
		if (should_matter) ircium_connection_unset_port (self->conn);
	}
}

static void
on_setting_network_name_entry_changed (IrciumNetworkSettingsPanel *self,
                                       GtkEditable                *editable);
static void
on_setting_server_address_entry_changed (IrciumNetworkSettingsPanel *self,
                                         GtkEditable                *editable);
static void
on_setting_server_password_entry_changed (IrciumNetworkSettingsPanel *self,
                                          GtkEditable                *editable);
static void
on_setting_user_nickname_entry_changed (IrciumNetworkSettingsPanel *self,
                                        GtkEditable                *editable);
static void
on_setting_user_username_entry_changed (IrciumNetworkSettingsPanel *self,
                                        GtkEditable                *editable);
static void
on_setting_user_realname_entry_changed (IrciumNetworkSettingsPanel *self,
                                        GtkEditable                *editable);

static void
ircium_network_settings_panel_class_init
	(IrciumNetworkSettingsPanelClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	object_class->dispose = ircium_network_settings_panel_dispose;

	GtkWidgetClass *widget_class = (GtkWidgetClass *) klass;

	gtk_widget_class_set_template_from_resource
		(widget_class,
		 "/com/gitlab/sham1/Ircium/ircium_network_settings_panel.ui");

	gtk_widget_class_bind_template_child (widget_class,
					      IrciumNetworkSettingsPanel,
					      setting_network_name_entry);

	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_tls_notls_revealer);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_password_revealer);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_address_entry);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_port_entry);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_tls_switch);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_tls_starttls);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_password_switch);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_server_password_entry);

	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_nickname_entry);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_username_entry);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_realname_entry);

	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_auth_nothing);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_auth_sasl);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_auth_nickserv);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_auth_label);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_auth_details_revealer);
	gtk_widget_class_bind_template_child
		(widget_class,
		 IrciumNetworkSettingsPanel,
		 setting_user_auth_details_stack);

	gtk_widget_class_bind_template_callback
		(widget_class,
		 on_setting_network_name_entry_changed);

	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_server_address_entry_changed);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_server_password_entry_changed);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_user_nickname_entry_changed);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_user_username_entry_changed);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_user_realname_entry_changed);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_server_tls_active_notify);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_server_password_active_notify);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_user_auth_changed);
	gtk_widget_class_bind_template_callback
		(widget_class, on_setting_server_port_entry_changed);
}

static void
ircium_network_settings_panel_init (IrciumNetworkSettingsPanel *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
}

static void
set_initial_fields (IrciumNetworkSettingsPanel *self)
{
	IrciumConnection *conn = self->conn;

	{
		const gchar *txt = ircium_connection_get_name (conn);
		gtk_entry_set_text (self->setting_network_name_entry,
				    txt != NULL ? txt : "");
	}

	{
		const gchar *txt = ircium_connection_get_address (conn);
		gtk_entry_set_text (self->setting_server_address_entry,
				    txt != NULL ? txt : "");
	}

	{
		const guint16 port = ircium_connection_get_port (conn);
		gchar *txt = g_strdup_printf ("%"G_GUINT16_FORMAT, port);
		gtk_entry_set_text (self->setting_server_port_entry,
				    port != 0 ? txt : "");
		g_free (txt);
	}

	{
		const gboolean tls = ircium_connection_get_tls (conn);
		gtk_switch_set_active (self->setting_server_tls_switch, tls);
	}

	{
		gchar *pass_str = NULL;
		const gboolean pass =
			ircium_connection_get_server_password (conn, &pass_str);

		gtk_switch_set_active (self->setting_server_password_switch,
				       pass);
		gtk_revealer_set_reveal_child
			(self->setting_server_password_revealer,
			 pass);

		if (pass) {
			gtk_entry_set_text
				(self->setting_server_password_entry, pass_str);
		}
	}

	{
		const gchar *txt = ircium_connection_get_nickname (conn);
		gtk_entry_set_text (self->setting_user_nickname_entry,
				    txt != NULL ? txt : "");
	}

	{
		const gchar *txt = ircium_connection_get_username (conn);
		gtk_entry_set_text (self->setting_user_username_entry,
				    txt != NULL ? txt : "");
	}

	{
		const gchar *txt = ircium_connection_get_realname (conn);
		gtk_entry_set_text (self->setting_user_realname_entry,
				    txt != NULL ? txt : "");
	}
}

static void
connection_changed_cb (IrciumWindow               *win,
                       GParamSpec                 *pspec,
                       IrciumNetworkSettingsPanel *self)
{
	ircium_network_settings_panel_disconnect_connection_signals (self);
	if (self->conn != NULL) g_object_unref (self->conn);
	self->conn = ircium_window_get_connection (win);
	if (self->conn != NULL) {
		ircium_network_settings_panel_connect_connection_signals (self);
		set_initial_fields (self);
	}
}

static void
ircium_network_settings_panel_disconnect_window_signal
	(IrciumNetworkSettingsPanel *self)
{
	if (self->win_notify_connection_handler != 0) {
		g_signal_handler_disconnect
			(self->win, self->win_notify_connection_handler);
		self->win_notify_connection_handler = 0;
	}
}
static void
ircium_network_settings_panel_disconnect_all_signals
	(IrciumNetworkSettingsPanel *self)
{
	ircium_network_settings_panel_disconnect_window_signal (self);
	ircium_network_settings_panel_disconnect_connection_signals (self);
}

void
ircium_network_settings_panel_set_window (IrciumNetworkSettingsPanel *self,
                                          IrciumWindow               *win)
{
	if (self->win != NULL) {
		g_critical ("%s should be called only once per instance",
			    __func__);

		ircium_network_settings_panel_disconnect_all_signals (self);

		g_object_unref (self->win);
		g_object_unref (self->conn);
	}
	self->win = g_object_ref (win);
	self->conn = ircium_window_get_connection (self->win);
	if (self->conn != NULL)
		ircium_network_settings_panel_connect_connection_signals (self);
	self->win_notify_connection_handler =
		g_signal_connect_object (self->win,
					 "notify::connection",
					 G_CALLBACK (connection_changed_cb),
					 self,
					 0);
}

static void
ircium_network_settings_panel_disconnect_connection_signals
	(IrciumNetworkSettingsPanel *self)
{
	if (self->connection_name_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_name_change_handler);
		self->connection_name_change_handler = 0;
	}
	if (self->connection_address_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_address_change_handler);
		self->connection_address_change_handler = 0;
	}
	if (self->connection_port_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_port_change_handler);
		self->connection_port_change_handler = 0;
	}
	if (self->connection_tls_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_tls_change_handler);
		self->connection_tls_change_handler = 0;
	}
	if (self->connection_pass_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_pass_change_handler);
		self->connection_pass_change_handler = 0;
	}
	if (self->connection_nickname_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_nickname_change_handler);
		self->connection_nickname_change_handler = 0;
	}
	if (self->connection_username_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_username_change_handler);
		self->connection_username_change_handler = 0;
	}
	if (self->connection_realname_change_handler != 0) {
		g_signal_handler_disconnect
			(self->conn,
			 self->connection_realname_change_handler);
		self->connection_realname_change_handler = 0;
	}
}
static void
connection_name_changed_cb (GObject                    *obj,
                            GParamSpec                 *pspec,
                            IrciumNetworkSettingsPanel *self)
{
	gboolean panel_is_in_view =
		ircium_window_is_network_settings_panel_visible (self->win);
	const gchar *txt = ircium_connection_get_name (self->conn);
	if (!panel_is_in_view) {
		gtk_entry_set_text (self->setting_network_name_entry,
				    txt != NULL ? txt : "");
	}
}
static void
connection_port_changed_cb (GObject                    *obj,
                            GParamSpec                 *pspec,
                            IrciumNetworkSettingsPanel *self)
{
	gboolean panel_is_in_view =
		ircium_window_is_network_settings_panel_visible (self->win);
	guint16 port = ircium_connection_get_port (self->conn);
	gboolean port_set = ircium_connection_is_port_set (self->conn);
	if (!panel_is_in_view) {
		if (port_set) {
			gchar *txt =
				g_strdup_printf ("%"G_GUINT16_FORMAT, port);
			gtk_entry_set_text (self->setting_server_port_entry,
					    txt);
			g_free (txt);
		} else {
			gtk_entry_set_text (self->setting_server_port_entry,
					    "");
		}
	}
}
static void
connection_address_changed_cb (GObject                    *obj,
                               GParamSpec                 *pspec,
                               IrciumNetworkSettingsPanel *self)
{
	gboolean panel_is_in_view =
		ircium_window_is_network_settings_panel_visible (self->win);
	const gchar *txt = ircium_connection_get_address (self->conn);
	if (!panel_is_in_view) {
		gtk_entry_set_text (self->setting_server_address_entry,
				    txt != NULL ? txt : "");
	}
}

static void
connection_tls_changed_cb (GObject                    *obj,
                           GParamSpec                 *pspec,
                           IrciumNetworkSettingsPanel *self)
{
	gboolean panel_is_in_view =
		ircium_window_is_network_settings_panel_visible (self->win);
	if (!panel_is_in_view)
		gtk_switch_set_active (self->setting_server_tls_switch,
				       ircium_connection_get_tls (self->conn));
}

static void
connection_pass_changed_cb (GObject                    *obj,
                            GParamSpec                 *pspec,
                            IrciumNetworkSettingsPanel *self)
{
	gboolean panel_is_in_view =
		ircium_window_is_network_settings_panel_visible (self->win);
	if (!panel_is_in_view) {
		gchar *pass = NULL;
		gboolean is_set =
			ircium_connection_get_server_password (self->conn,
							       &pass);

		gtk_switch_set_active
			(self->setting_server_password_switch, is_set);
		gtk_revealer_set_reveal_child
			(self->setting_server_password_revealer, is_set);

		if (pass != NULL && *pass != '\0') {
			gtk_entry_set_text (self->setting_server_password_entry,
					    pass);
		}
	}
}

static void
connection_nickname_changed_cb (GObject                    *obj,
                                GParamSpec                 *pspec,
                                IrciumNetworkSettingsPanel *self)
{
	gboolean panel_is_in_view =
		ircium_window_is_network_settings_panel_visible (self->win);
	const gchar *txt = ircium_connection_get_nickname (self->conn);
	if (!panel_is_in_view) {
		gtk_entry_set_text (self->setting_user_nickname_entry,
				    txt != NULL ? txt : "");
	}
}

static void
ircium_network_settings_panel_connect_connection_signals
	(IrciumNetworkSettingsPanel *self)
{
	// Sanity tests
	if (self->connection_port_change_handler != 0) {
		g_critical ("%s should only be called once per connection",
			    __func__);
		return;
	}
	if (self->conn == NULL) {
		g_critical ("%s should only be called "
			    "when a connection is available",
			    __func__);
		return;
	}

	self->connection_name_change_handler =
		g_signal_connect (self->conn,
				  "notify::name",
				  G_CALLBACK (connection_name_changed_cb),
				  self);
	self->connection_port_change_handler =
		g_signal_connect (self->conn,
				  "notify::port",
				  G_CALLBACK (connection_port_changed_cb),
				  self);
	self->connection_address_change_handler =
		g_signal_connect (self->conn,
				  "notify::hostname",
				  G_CALLBACK (connection_address_changed_cb),
				  self);
	self->connection_tls_change_handler =
		g_signal_connect (self->conn,
				  "notify::tls",
				  G_CALLBACK (connection_tls_changed_cb),
				  self);
	self->connection_pass_change_handler =
		g_signal_connect (self->conn,
				  "notify::pass",
				  G_CALLBACK (connection_pass_changed_cb),
				  self);
/*	self->connection_username_change_handler =
		g_signal_connect (self->conn,
				  "notify::username",
				  G_CALLBACK (connection_username_changed_cb),
				  self);*/
	self->connection_nickname_change_handler =
		g_signal_connect (self->conn,
				  "notify::nickname",
				  G_CALLBACK (connection_nickname_changed_cb),
				  self);
/*	self->connection_realname_change_handler =
		g_signal_connect (self->conn,
				  "notify::realname",
				  G_CALLBACK (connection_realname_changed_cb),
				  self);*/
}

static void
on_setting_network_name_entry_changed (IrciumNetworkSettingsPanel *self,
                                       GtkEditable                *editable)
{
	GtkEntry *name_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		ircium_window_is_network_settings_panel_visible (self->win);

	guint16 len = gtk_entry_get_text_length (name_entry);

	if (should_matter) {
		if (len == 0) {
			ircium_connection_set_name (self->conn, NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (name_entry);
		ircium_connection_set_name (self->conn, txt);
	}
}

static void
on_setting_server_address_entry_changed (IrciumNetworkSettingsPanel *self,
                                         GtkEditable                *editable)
{
	GtkEntry *address_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (address_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (address_entry));

	if (should_matter) {
		guint16 len = gtk_entry_get_text_length (address_entry);
		if (len == 0) {
			ircium_connection_set_address (self->conn, NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (address_entry);
		ircium_connection_set_address (self->conn, txt);
	}
}

static void
on_setting_server_password_entry_changed (IrciumNetworkSettingsPanel *self,
                                          GtkEditable                *editable)
{
	const gchar *pass =
		gtk_entry_get_text (self->setting_server_password_entry);

	gboolean should_matter =
		ircium_window_is_network_settings_panel_visible (self->win);

	if (should_matter) {
		ircium_connection_set_server_password (self->conn, pass);
	}
}

static void
on_setting_user_nickname_entry_changed (IrciumNetworkSettingsPanel *self,
                                        GtkEditable                *editable)
{
	GtkEntry *nick_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (nick_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (nick_entry));

	if (should_matter) {
		guint16 len = gtk_entry_get_text_length (nick_entry);
		if (len == 0) {
			ircium_connection_set_nickname (self->conn, NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (nick_entry);
		ircium_connection_set_nickname (self->conn, txt);
	}
}

static void
on_setting_user_username_entry_changed (IrciumNetworkSettingsPanel *self,
                                        GtkEditable                *editable)
{
	GtkEntry *username_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (username_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (username_entry));

	if (should_matter) {
		guint16 len = gtk_entry_get_text_length (username_entry);
		if (len == 0) {
			ircium_connection_set_username (self->conn, NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (username_entry);
		ircium_connection_set_username (self->conn, txt);
	}
}

static void
on_setting_user_realname_entry_changed (IrciumNetworkSettingsPanel *self,
                                        GtkEditable                *editable)
{
	GtkEntry *realname_entry = GTK_ENTRY (editable);

	gboolean should_matter =
		gtk_widget_is_visible (GTK_WIDGET (realname_entry)) &&
		gtk_widget_is_focus (GTK_WIDGET (realname_entry));

	if (should_matter) {
		guint16 len = gtk_entry_get_text_length (realname_entry);
		if (len == 0) {
			ircium_connection_set_realname (self->conn, NULL);
			return;
		}
		const gchar *txt = gtk_entry_get_text (realname_entry);
		ircium_connection_set_realname (self->conn, txt);
	}
}
