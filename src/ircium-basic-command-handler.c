/* ircium-basic-command-handler.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib/gi18n.h>

#include "ircium-basic-command-handler.h"
#include "ircium-command-reactor-private.h"
#include "ircium-session-private.h"
#include "ircium-channel-buffer.h"

struct _IrciumBasicCommandHandler
{
	IrciumCommandHandler parent_instance;

	IrciumCommandReactorHandler *unknown_handler;
	IrciumCommandReactorHandler *ping_handler;
	IrciumCommandReactorHandler *notice_handler;
	IrciumCommandReactorHandler *privmsg_handler;
	IrciumCommandReactorHandler *generic_numeric_handler;
	IrciumCommandReactorHandler *isupport_handler;
	IrciumCommandReactorHandler *join_handler;
	IrciumCommandReactorHandler *part_handler;
	IrciumCommandReactorHandler *topic_set_handler;
	IrciumCommandReactorHandler *topic_numeric_handler;
	IrciumCommandReactorHandler *no_topic_handler;
	IrciumCommandReactorHandler *topic_setter_handler;

	IrciumCommandReactorHandler *names_handler;

	IrciumCommandReactorHandler *nick_handler;
	IrciumCommandReactorHandler *quit_handler;
	IrciumCommandReactorHandler *kick_handler;
};

G_DEFINE_TYPE (IrciumBasicCommandHandler,
	       ircium_basic_command_handler,
	       IRCIUM_TYPE_COMMAND_HANDLER)

IrciumBasicCommandHandler *
ircium_basic_command_handler_new (void)
{
	return g_object_new (IRCIUM_TYPE_BASIC_COMMAND_HANDLER, NULL);
}

static void
ircium_basic_command_handler_finalize (GObject *object)
{
	IrciumBasicCommandHandler *self = (IrciumBasicCommandHandler *)object;

	g_clear_pointer (&self->unknown_handler, g_free);
	g_clear_pointer (&self->ping_handler, g_free);
	g_clear_pointer (&self->notice_handler, g_free);
	g_clear_pointer (&self->privmsg_handler, g_free);
	g_clear_pointer (&self->generic_numeric_handler, g_free);
	g_clear_pointer (&self->isupport_handler, g_free);
	g_clear_pointer (&self->join_handler, g_free);
	g_clear_pointer (&self->part_handler, g_free);
	g_clear_pointer (&self->topic_set_handler, g_free);
	g_clear_pointer (&self->topic_numeric_handler, g_free);
	g_clear_pointer (&self->no_topic_handler, g_free);
	g_clear_pointer (&self->topic_setter_handler, g_free);
	g_clear_pointer (&self->names_handler, g_free);

	G_OBJECT_CLASS (ircium_basic_command_handler_parent_class)->finalize (object);
}

static void
ircium_basic_command_handler_register_handlers (IrciumCommandHandler *handler,
                                                IrciumCommandReactor *reactor);
static void
ircium_basic_command_handler_unregister_handlers
	(IrciumCommandHandler *handler,
	 IrciumCommandReactor *reactor);

static void
ircium_basic_command_handler_class_init (IrciumBasicCommandHandlerClass *klass)
{
	IrciumCommandHandlerClass *handler_class =
		IRCIUM_COMMAND_HANDLER_CLASS (klass);
	handler_class->register_handlers =
		ircium_basic_command_handler_register_handlers;
	handler_class->unregister_handlers =
		ircium_basic_command_handler_unregister_handlers;

	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ircium_basic_command_handler_finalize;
}

static gboolean
ircium_basic_command_handler_unknown_predicate (IrciumMessage *msg,
                                                gpointer       user_data)
{
	return TRUE;
}

static void
ircium_basic_command_handler_unknown_cb (IrciumCommandHandler *handler,
                                         IrciumCommandReactor *reactor,
                                         IrciumMessage        *msg,
                                         gpointer              user_data)
{
	(void) handler;
	(void) user_data;

	GBytes *msg_bytes = ircium_message_serialize (msg);
	gsize msg_str_len = g_bytes_get_size (msg_bytes) - 2;
	gchar *tmp = g_malloc0_n (msg_str_len + 1, sizeof(*tmp));

	{
		gsize foo = 0;
		guint8 *data = g_bytes_unref_to_data (msg_bytes, &foo);
		msg_bytes = NULL;

		memcpy (tmp, data, msg_str_len);
		g_free (data);
	}

	gchar *escaped_msg = g_strescape (tmp, NULL);
	g_free (tmp);

	g_debug ("Unknown message: %s", escaped_msg);

	IrciumServerBuffer *buffer =
		ircium_session_get_server_buffer
			(ircium_command_reactor_get_session (reactor));
	GDateTime *t = g_date_time_new_now_utc ();
	IrciumMessageSource *source =
		ircium_message_source_new (ircium_message_get_source (msg));
	IrciumBufferLine *line =
		ircium_buffer_line_new_with_sender
			(IRCIUM_BUFFER_LINE_TYPE_NORMAL,
			 0,
			 source,
			 t,
			 escaped_msg);
	g_object_unref (source);
	g_date_time_unref (t);
	ircium_buffer_add_line (IRCIUM_BUFFER (buffer), line);

	g_free (escaped_msg);
}

static gboolean
ircium_basic_command_handler_ping_predicate (IrciumMessage *msg,
                                             gpointer       user_data)
{
	return g_strcmp0 (ircium_message_get_command (msg), "PING") == 0;
}

static void
ircium_basic_command_handler_ping_cb (IrciumCommandHandler *handler,
                                      IrciumCommandReactor *reactor,
                                      IrciumMessage        *msg,
                                      gpointer              user_data)
{
	(void) handler;
	(void) user_data;

	const GPtrArray *ping_params = ircium_message_get_params (msg);

	IrciumMessage *pong_msg =
		ircium_message_new (NULL,
				    NULL,
				    "PONG",
				    (GPtrArray *) ping_params);
	ircium_session_send_message
		(ircium_command_reactor_get_session (reactor),
		 pong_msg);
}

static gboolean
ircium_basic_command_handler_numeric_predicate (IrciumMessage *msg,
                                                gpointer       user_data)
{
	gsize len = strlen (ircium_message_get_command (msg));
	if (len != 3) return FALSE;
	for (const gchar *iter = ircium_message_get_command (msg);
	     *iter != '\0'; ++iter) {
		if (!g_ascii_isdigit (*iter)) return FALSE;
	}
	return TRUE;
}

static void
ircium_basic_command_handler_numeric_cb (IrciumCommandHandler *handler,
                                         IrciumCommandReactor *reactor,
                                         IrciumMessage        *msg,
                                         gpointer              user_data)
{
	(void) handler;
	(void) user_data;
	GString *params = g_string_new (NULL);
	const GPtrArray *params_arr = ircium_message_get_params (msg);
	for (gsize i = 1; i < params_arr->len; ++i) {
		gchar *param = params_arr->pdata[i];
		g_string_append (params, param);
		if (i != (params_arr->len - 1)) {
			g_string_append_c (params, ' ');
		}
	}
	gchar *args = g_string_free (params, FALSE);

	IrciumServerBuffer *buffer =
		ircium_session_get_server_buffer
			(ircium_command_reactor_get_session (reactor));
	GDateTime *t = g_date_time_new_now_utc ();
	IrciumMessageSource *source =
		ircium_message_source_new (ircium_message_get_source (msg));
	IrciumBufferLine *line =
		ircium_buffer_line_new_with_sender
			(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
			 0,
			 source,
			 t,
			 args);
	g_object_unref (source);
	g_date_time_unref (t);
	ircium_buffer_add_line (IRCIUM_BUFFER (buffer), line);

	g_free (args);
}

static gboolean
ircium_basic_command_handler_notice_predicate (IrciumMessage *msg,
                                               gpointer       user_data)
{
	return g_strcmp0 ("NOTICE", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_notice_cb (IrciumCommandHandler *handler,
                                        IrciumCommandReactor *reactor,
                                        IrciumMessage        *msg,
                                        gpointer              user_data)
{
	(void) handler;
	(void) user_data;
	GString *message = g_string_new (NULL);
	const GPtrArray *params_arr = ircium_message_get_params (msg);
	for (gsize i = 1; i < params_arr->len; ++i) {
		gchar *param = params_arr->pdata[i];
		g_string_append (message, param);
		if (i != (params_arr->len - 1)) {
			g_string_append_c (message, ' ');
		}
	}
	gchar *args = g_string_free (message, FALSE);

	IrciumSession *session = ircium_command_reactor_get_session (reactor);

	IrciumServerBuffer *buffer =
		ircium_session_get_server_buffer (session);
	GDateTime *t = g_date_time_new_now_utc ();
	IrciumMessageSource *source =
		ircium_message_source_new (ircium_message_get_source (msg));

	IrciumBufferLineFlag flags = IRCIUM_BUFFER_LINE_FLAG_NOTICE;

	IrciumBufferLine *line =
		ircium_buffer_line_new_with_sender
			(IRCIUM_BUFFER_LINE_TYPE_NORMAL,
			 flags,
			 source,
			 t,
			 args);
	g_object_unref (source);
	g_date_time_unref (t);
	ircium_buffer_add_line (IRCIUM_BUFFER (buffer), line);

	g_free (args);
}

static IrciumBufferLine *
create_line_from_privmsg (IrciumMessageSource *src,
                          const gchar         *msg_txt,
                          const gchar         *nick)
{
	g_return_val_if_fail (IRCIUM_IS_MESSAGE_SOURCE (src), NULL);
	g_return_val_if_fail (msg_txt != NULL, NULL);

	gchar *finished_text;
	IrciumBufferLineType type = IRCIUM_BUFFER_LINE_TYPE_NORMAL;
	IrciumBufferLineFlag flags = 0;

	finished_text = g_strdup (msg_txt);

	GDateTime *t = g_date_time_new_now_utc ();
	IrciumBufferLine *line =
		ircium_buffer_line_new_with_sender (type,
						    flags,
						    src,
						    t,
						    finished_text);
	g_free (finished_text);
	return line;
}

static void handle_ctcp_request (const gchar         *msg_text,
                                 IrciumSession       *session,
                                 const gchar         *target,
                                 IrciumMessageSource *src);

static gboolean
privmsg_to_buffer (IrciumSession       *session,
                   IrciumMessageSource *src,
                   const gchar         *nick,
                   const gchar         *target,
                   const gchar         *msg_txt)
{
	GListModel *buffers = ircium_session_get_buffer_list (session);
	IrciumBuffer *buf = NULL;
	for (gsize i = 0; i < g_list_model_get_n_items (buffers); ++i) {
		IrciumBuffer *tmp = g_list_model_get_item (buffers, i);
		if (IRCIUM_IS_CHANNEL_BUFFER (tmp)) {
			if (g_strcmp0 (ircium_buffer_get_title (tmp),
				       target) == 0) {
				buf = tmp;
			}
		}
	}
	if (buf != NULL) {
		// We will check if this should be a continuation.
		GListModel *prev_msgs = ircium_buffer_get_contents (buf);
		gsize prev_msg_index = g_list_model_get_n_items (prev_msgs) - 1;
		IrciumBufferLine *prev_line =
			g_list_model_get_item (prev_msgs,
					       prev_msg_index);
		gboolean is_continuation = TRUE;
		is_continuation = is_continuation && prev_line != NULL;

		is_continuation = is_continuation &&
			ircium_buffer_line_is_sender_user (prev_line);

		is_continuation = is_continuation &&
			ircium_buffer_line_get_line_type (prev_line) ==
			IRCIUM_BUFFER_LINE_TYPE_NORMAL;

		is_continuation = is_continuation &&
			(ircium_buffer_line_get_flags (prev_line) &
			 ~(IRCIUM_BUFFER_LINE_FLAG_CONTINUATION)) == 0;

		is_continuation = is_continuation &&
			(g_strcmp0
				(ircium_buffer_line_get_sender_nick (prev_line),
				 ircium_message_source_get_nick (src)) == 0);

		IrciumBufferLine *line =
			create_line_from_privmsg (src, msg_txt, nick);

		if (is_continuation) {
			ircium_buffer_line_add_flags
				(line, IRCIUM_BUFFER_LINE_FLAG_CONTINUATION);
		}

		ircium_buffer_add_line (buf, line);
		g_object_unref (line);
		return TRUE;
	}
	return FALSE;
}

static void
handle_ctcp_request (const gchar         *msg_text,
                     IrciumSession       *session,
                     const gchar         *target,
                     IrciumMessageSource *src)
{
	gsize ctcp_req_len = strlen (msg_text);
	gboolean has_last_ctcp_delim = msg_text[ctcp_req_len - 1] == 0x01;
	g_auto (GStrv) command_params = NULL;
	{
		gsize new_len = ctcp_req_len - (has_last_ctcp_delim ? 1 : 0);
		g_autofree gchar *stripped_msg =
			g_malloc0_n (new_len, sizeof (*stripped_msg));
		memcpy (stripped_msg, msg_text + 1, new_len - 1);

		command_params = g_strsplit (stripped_msg, " ", 2);
	}
	const gchar *command = command_params[0];
	const gchar *args =
		g_strv_length (command_params) > 1 && command_params[1] ?
		command_params[1] : NULL;

	g_autofree gchar *folded_command = g_ascii_strup (command, -1);

	// TODO: Handle all commands.
	if (g_strcmp0 (folded_command, "ACTION") == 0) {
		if (G_UNLIKELY (!ircium_message_source_is_user (src))) {
			g_debug ("Something that isn't an user did /me!");
			return;
		}
		IrciumBuffer *buf =
			ircium_session_find_buffer (session, target);
		if (G_UNLIKELY (!IRCIUM_IS_CHANNEL_BUFFER (buf))) {
			g_debug ("/me was not sent to a channel!");
			return;
		}

		const gchar *me_nick = ircium_message_source_get_nick (src);
		GString *me_str = g_string_new (NULL);
		g_string_append (me_str, me_nick);
		if (args != NULL) g_string_append_printf (me_str, " %s", args);
		g_autofree gchar *msg_str = g_string_free (me_str, FALSE);

		GDateTime *t = g_date_time_new_now_utc ();
		IrciumBufferLine *line =
			ircium_buffer_line_new_with_sender
				(IRCIUM_BUFFER_LINE_TYPE_NORMAL,
				 IRCIUM_BUFFER_LINE_FLAG_ACTION,
				 src, t, msg_str);
		g_date_time_unref (t);
		ircium_buffer_add_line (buf, line);

		return;
	} else if (g_strcmp0 (folded_command, "CLIENTINFO") == 0) {
		GPtrArray *return_params =
			g_ptr_array_new_with_free_func (g_free);

		const gchar *sender_nick = ircium_message_source_get_nick (src);
		g_ptr_array_add (return_params, g_strdup (sender_nick));

		GString *return_msg = g_string_new (NULL);

		// Update this string when adding CTCP messages.
		// Keep this in alphabetical order and separate the
		// message types with one U+0020 SPACE.
		const gchar *supported_ctcp =
			"ACTION CLIENTINFO FINGER PING SOURCE TIME";
		g_string_printf (return_msg, "\x01CLIENTINFO %s\x01",
				 supported_ctcp);
		g_ptr_array_add (return_params,
				 g_string_free (return_msg, FALSE));

		IrciumMessage *return_message =
			ircium_message_new (NULL,
					    NULL,
					    "NOTICE",
					    return_params);

		ircium_session_send_message (session, return_message);
	} else if (g_strcmp0 (folded_command, "FINGER") == 0) {
		GPtrArray *return_params =
			g_ptr_array_new_with_free_func (g_free);

		const gchar *sender_nick = ircium_message_source_get_nick (src);
		g_ptr_array_add (return_params, g_strdup (sender_nick));
		g_ptr_array_add (return_params,
				 g_strdup_printf ("\x01FINGER Ircium\x01"));

		IrciumMessage *return_message =
			ircium_message_new (NULL,
					    NULL,
					    "NOTICE",
					    return_params);

		ircium_session_send_message (session, return_message);
	} else if (g_strcmp0 (folded_command, "PING") == 0) {
		GPtrArray *return_params =
			g_ptr_array_new_with_free_func (g_free);

		const gchar *sender_nick = ircium_message_source_get_nick (src);
		g_ptr_array_add (return_params, g_strdup (sender_nick));
		GString *ping_params = g_string_new (NULL);

		for (gsize i = 0; i < g_strv_length (command_params); ++i) {
			gchar *param = command_params[i];
			g_string_append_printf (ping_params, " %s", param);
		}

		gchar *command_params = g_string_free (ping_params, FALSE);
		g_ptr_array_add (return_params,
				 g_strdup_printf ("\x01PING%s\x01",
						  command_params));
		g_free (command_params);

		IrciumMessage *return_message =
			ircium_message_new (NULL,
					    NULL,
					    "NOTICE",
					    return_params);

		ircium_session_send_message (session, return_message);
	} else if (g_strcmp0 (folded_command, "SOURCE") == 0) {
		GPtrArray *return_params =
			g_ptr_array_new_with_free_func (g_free);

		const gchar *sender_nick = ircium_message_source_get_nick (src);
		g_ptr_array_add (return_params, g_strdup (sender_nick));
		GString *ping_params = g_string_new (NULL);

		for (gsize i = 0; i < g_strv_length (command_params); ++i) {
			gchar *param = command_params[i];
			g_string_append_printf (ping_params, " %s", param);
		}

		gchar *command_params = g_string_free (ping_params, FALSE);
		// TODO: Make this configurable somewhere else than the source
		const gchar *repo = "https://gitlab.com/sham1/ircium/";
		g_ptr_array_add (return_params,
				 g_strdup_printf ("\x01SOURCE %s\x01",
						  repo));
		g_free (command_params);

		IrciumMessage *return_message =
			ircium_message_new (NULL,
					    NULL,
					    "NOTICE",
					    return_params);

		ircium_session_send_message (session, return_message);
	} else if (g_strcmp0 (folded_command, "TIME") == 0) {
		GPtrArray *return_params =
			g_ptr_array_new_with_free_func (g_free);

		const gchar *sender_nick = ircium_message_source_get_nick (src);
		g_ptr_array_add (return_params, g_strdup (sender_nick));

		g_autoptr(GDateTime) dt = g_date_time_new_now_utc ();
		gchar *time = g_date_time_format (dt, "%c");

		g_ptr_array_add (return_params,
				 g_strdup_printf ("\x01TIME %s\x01",
						  time));
		g_free (time);

		IrciumMessage *return_message =
			ircium_message_new (NULL,
					    NULL,
					    "NOTICE",
					    return_params);

		ircium_session_send_message (session, return_message);
	} else {
		g_debug ("Unknown CTCP command: %s", folded_command);
	}
}

static gboolean
ircium_basic_command_handler_privmsg_predicate (IrciumMessage *msg,
                                                gpointer       user_data)
{
	return g_strcmp0 ("PRIVMSG", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_privmsg_cb (IrciumCommandHandler *handler,
                                         IrciumCommandReactor *reactor,
                                         IrciumMessage        *msg,
                                         gpointer              user_data)
{
	(void) handler;
	(void) user_data;

	IrciumMessageSource *src =
		ircium_message_source_new (ircium_message_get_source (msg));

	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	const GPtrArray *params = ircium_message_get_params (msg);
	GHashTable *isupport = ircium_session_get_isupport (session);

	const gchar *nick = ircium_session_get_nick (session);

	GString *msg_builder = g_string_new (NULL);
	for (gsize i = 1; i < params->len; ++i) {
		gchar *fragment = params->pdata[i];
		g_string_append (msg_builder, fragment);
		if (i != (params->len - 1)) {
			g_string_append_c (msg_builder, ' ');
		}
	}
	gchar *msg_txt = g_string_free (msg_builder, FALSE);
	gchar *target = g_strdup (params->pdata[0]);

	// Check if the message is actually a CTCP request.
	// If so, handle it specially.
	gboolean is_ctcp = *msg_txt == 0x01;
	if (is_ctcp) {
		handle_ctcp_request (msg_txt, session, target, src);
		return;
	}

	// Let's first try to see if the target of this message is one
	// of our existing buffers.
	// If it's so, return.
	if (privmsg_to_buffer (session, src, nick, target, msg_txt)) {
		g_object_unref (src);
		return;
	}

	gchar *statuses;
	if (g_hash_table_lookup_extended (isupport, "STATUSMSG",
					  NULL, (gpointer *) &statuses)) {
		gsize statuses_len = strlen (statuses);
		gboolean found = FALSE;
		for (gsize i = 0; i < statuses_len; ++i) {
			gchar prefix = statuses[i];
			if (prefix == target[0]) {
				found = TRUE;
				break;
			}
		}
		if (found) {
			gchar *tmp = target;
			target = g_strdup (tmp + 1);
			g_free (tmp);
		}
		// If after stripping the status character from target we
		// find our buffer, we add the message to there and return.
		if (privmsg_to_buffer (session, src, nick, target, msg_txt)) {
			g_object_unref (src);
			return;
		}
	}
	// If the message isn't either of those, it goes to the server buffer
	// for now.
	IrciumBufferLine *line = create_line_from_privmsg (src, msg_txt, nick);

	IrciumServerBuffer *server_buf =
		ircium_session_get_server_buffer (session);
	ircium_buffer_add_line (IRCIUM_BUFFER (server_buf), line);

	g_object_unref (line);

	g_free (msg_txt);
	g_free (target);
	g_object_unref (src);
}

static gboolean
ircium_basic_command_handler_isupport_predicate (IrciumMessage *msg,
                                                 gpointer       user_data)
{
	return g_strcmp0 ("005", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_isupport_cb (IrciumCommandHandler *handler,
                                          IrciumCommandReactor *reactor,
                                          IrciumMessage        *msg,
                                          gpointer              user_data)
{
	(void) handler;
	(void) user_data;

	const GPtrArray *params = ircium_message_get_params (msg);
	GString *msg_form = g_string_new (NULL);

	GHashTable *isupport = ircium_session_get_isupport
		(ircium_command_reactor_get_session (reactor));

	for (gsize i = 1; i < (params->len - 1); ++i) {
		gchar *param = params->pdata[i];
		g_string_append (msg_form, param);
		g_string_append_c (msg_form, ' ');
		// If the token is negative, we shall remove it.
		if (param[0] == '-') {
			gchar *real_param = (param + 1);
			g_hash_table_remove (isupport, real_param);
		} else {
			g_auto (GStrv) real_param;
			real_param = g_strsplit (param, "=", 2);

			ircium_session_set_isupport_val (isupport,
							 real_param[0],
							 real_param[1]);
		}
	}
	g_string_append (msg_form, params->pdata[params->len - 1]);

	gchar *msg_str = g_string_free (msg_form, FALSE);
	IrciumServerBuffer *buffer =
		ircium_session_get_server_buffer
			(ircium_command_reactor_get_session (reactor));
	GDateTime *t = g_date_time_new_now_utc ();
	IrciumMessageSource *source =
		ircium_message_source_new (ircium_message_get_source (msg));
	IrciumBufferLine *line =
		ircium_buffer_line_new_with_sender
			(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
			 0,
			 source,
			 t,
			 msg_str);
	g_object_unref (source);
	g_date_time_unref (t);
	ircium_buffer_add_line (IRCIUM_BUFFER (buffer), line);

	g_free (msg_str);
}

static gboolean
ircium_basic_command_handler_join_predicate (IrciumMessage *msg,
                                             gpointer       user_data)
{
	return g_strcmp0 ("JOIN", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_join_cb (IrciumCommandHandler *handler,
                                      IrciumCommandReactor *reactor,
                                      IrciumMessage        *msg,
                                      gpointer              user_data)
{
	(void) handler;
	(void) user_data;
	g_autoptr (IrciumMessageSource) src = NULL;
	src = ircium_message_source_new (ircium_message_get_source (msg));
	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	gchar *channel_name = ircium_message_get_params (msg)->pdata[0];
	if (!ircium_message_source_is_user (src)) {
		// A server shouldn't be joining channels!
		g_debug ("Server %s tried to join channel %s! "
			 "This shouldn't happen!",
			 ircium_message_source_get_server_host (src),
			 channel_name);
		return;
	}
	const gchar *nick = ircium_message_source_get_nick (src);
	GListModel *list = ircium_session_get_buffer_list (session);
	// We know for a fact that the channel parameter of JOIN
	// is always a singular thing because this is coming from the
	// server to the client.
	// Is it us joining a server.
	if (g_strcmp0 (nick, ircium_session_get_nick (session)) == 0) {
		// Do we have a buffer here already?
		// If not, create a new channel buffer.
		IrciumBuffer *buf = NULL;
		for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
			IrciumBuffer *tmp = g_list_model_get_item (list, i);
			if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
			// The buffer title is also the channel name.
			gchar *chan_name = ircium_buffer_get_title (tmp);
			// TODO: Casefold here just in case.
			if (g_strcmp0 (chan_name, channel_name) == 0) {
				buf = tmp;
				break;
			}
		}
		if (G_UNLIKELY (buf != NULL)) {
			g_debug ("We tried to join a channel we're already in");
			return;
		} else {
			IrciumChannelBuffer *channel_buf =
				ircium_channel_buffer_new (channel_name);

			ircium_session_add_buffer (session,
						   IRCIUM_BUFFER (channel_buf));

			g_object_unref (channel_buf);
		}
	} else {
		// First, we'll find out if we know about this user already
		// (i.e. we share channels)

		IrciumUser *this_user =
			ircium_session_find_user_nick (session, nick);

		if (this_user == NULL) {
			this_user = ircium_user_new (session, nick);
			ircium_session_add_user (session, this_user);
			g_object_unref (this_user);
		}

		// Get the current channel buffer.
		IrciumChannelBuffer *buf = NULL;
		for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
			IrciumBuffer *tmp = g_list_model_get_item (list, i);
			if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
			// The buffer title is also the channel name.
			gchar *chan_name = ircium_buffer_get_title (tmp);
			// TODO: Casefold here just in case.
			if (g_strcmp0 (chan_name, channel_name) == 0) {
				buf = IRCIUM_CHANNEL_BUFFER (tmp);
				break;
			}
		}
		if (G_UNLIKELY (buf == NULL)) {
			g_debug ("We were informed about someone entering "
				 "a channel which we're not in.");
			return;
		}

		ircium_channel_buffer_add_user (buf, this_user);

		/// TRANSLATORS: The string on this format string is the
		/// nickname of the user that joined the channel.
		gchar *msg_text =
			g_strdup_printf (_("%s has joined the channel"), nick);
		GDateTime *t = g_date_time_new_now_utc ();
		IrciumMessageSource *source =
			ircium_message_source_new (ircium_message_get_source
						   (msg));
		IrciumBufferLine *line =
			ircium_buffer_line_new_with_sender
				(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
				 IRCIUM_BUFFER_LINE_FLAG_JOIN,
				 source,
				 t,
				 msg_text);
		g_object_unref (source);
		g_date_time_unref (t);
		ircium_buffer_add_line (IRCIUM_BUFFER (buf), line);
		g_object_unref (line);
		g_free (msg_text);
	}
}

static gboolean
ircium_basic_command_handler_part_predicate (IrciumMessage *msg,
                                             gpointer       user_data)
{
	return g_strcmp0 ("PART", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_part_cb (IrciumCommandHandler *handler,
                                      IrciumCommandReactor *reactor,
                                      IrciumMessage        *msg,
                                      gpointer              user_data)
{
	(void) handler;
	(void) user_data;
	g_autoptr (IrciumMessageSource) src = NULL;
	src = ircium_message_source_new (ircium_message_get_source (msg));
	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	gchar *channel_name = ircium_message_get_params (msg)->pdata[0];
	if (!ircium_message_source_is_user (src)) {
		// A server shouldn't be parting channels!
		g_debug ("Server %s tried to part from channel %s! "
			 "This shouldn't happen!",
			 ircium_message_source_get_server_host (src),
			 channel_name);
		return;
	}
	const gchar *nick = ircium_message_source_get_nick (src);
	GListModel *list = ircium_session_get_buffer_list (session);
	// Are we the ones parting?
	if (g_strcmp0 (nick, ircium_session_get_nick (session)) == 0) {
		// Do we have a buffer here already?
		// If so, close it.
		IrciumBuffer *buf = NULL;
		for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
			IrciumBuffer *tmp = g_list_model_get_item (list, i);
			if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
			// The buffer title is also the channel name.
			gchar *chan_name = ircium_buffer_get_title (tmp);
			// TODO: Casefold here just in case.
			if (g_strcmp0 (chan_name, channel_name) == 0) {
				buf = tmp;
				break;
			}
		}
		if (G_UNLIKELY (buf == NULL)) {
			g_debug ("We tried to part from a channel we were "
				 "not in!");
			return;
		} else {
			ircium_session_remove_buffer (session, buf);
		}
	} else {
		IrciumUser *this_user = ircium_session_find_user_nick (session,
								       nick);

		// Get the current channel buffer.
		IrciumChannelBuffer *buf = NULL;
		for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
			IrciumBuffer *tmp = g_list_model_get_item (list, i);
			if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
			// The buffer title is also the channel name.
			gchar *chan_name = ircium_buffer_get_title (tmp);
			// TODO: Casefold here just in case.
			if (g_strcmp0 (chan_name, channel_name) == 0) {
				buf = IRCIUM_CHANNEL_BUFFER (tmp);
				break;
			}
		}
		if (G_UNLIKELY (buf == NULL)) {
			g_debug ("We were informed about someone parting from "
				 "a channel which we're not in.");
			return;
		}

		ircium_channel_buffer_remove_user (buf, this_user);
		ircium_session_garbage_collect_users (session);

		const GPtrArray *params = ircium_message_get_params (msg);
		gchar *leave_reason = params->len > 1 ? params->pdata[1] : "";

		/// TRANSLATORS: The first string on this format string is the
		/// nickname of the user that left the channel.
		/// The second one is the reason.
		gchar *msg_text =
			g_strdup_printf
				(_("%s has left the channel (Reason: %s)"),
				 nick, leave_reason);
		GDateTime *t = g_date_time_new_now_utc ();
		IrciumMessageSource *source =
			ircium_message_source_new (ircium_message_get_source
						   (msg));
		IrciumBufferLine *line =
			ircium_buffer_line_new_with_sender
				(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
				 IRCIUM_BUFFER_LINE_FLAG_QUIT,
				 source,
				 t,
				 msg_text);
		g_object_unref (source);
		g_date_time_unref (t);
		ircium_buffer_add_line (IRCIUM_BUFFER (buf), line);
		g_object_unref (line);
		g_free (msg_text);
	}
}

static gboolean
ircium_basic_command_handler_topic_set_predicate (IrciumMessage *msg,
                                                  gpointer       user_data)
{
	return g_strcmp0 (ircium_message_get_command (msg), "TOPIC") == 0;
}

static void
ircium_basic_command_handler_topic_set_cb (IrciumCommandHandler *handler,
                                           IrciumCommandReactor *reactor,
                                           IrciumMessage        *msg,
                                           gpointer              user_data)
{
	g_autoptr (IrciumMessageSource) source = NULL;
	source = ircium_message_source_new (ircium_message_get_source (msg));

	const GPtrArray *params = ircium_message_get_params (msg);
	gchar *channel = params->pdata[0];
	gchar *topic = params->pdata[1];

	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	GListModel *list = ircium_session_get_buffer_list (session);

	IrciumBuffer *buf = NULL;
	for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
		IrciumBuffer *tmp = g_list_model_get_item (list, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
		// The buffer title is also the channel name.
		gchar *chan_name = ircium_buffer_get_title (tmp);
		// TODO: Casefold here just in case.
		if (g_strcmp0 (chan_name, channel) == 0) {
			buf = tmp;
			break;
		}
	}
	if (G_UNLIKELY (buf == NULL)) {
		g_debug ("We were informed of a topic change on a channel "
			 "we are not in!");
		return;
	} else {
		const gchar *setter_nick =
			ircium_message_source_get_nick (source);
		GDateTime *set_time = g_date_time_new_now_utc ();
		ircium_buffer_set_topic_metadata (buf, setter_nick, set_time);
		ircium_buffer_set_topic (buf, topic);
		g_date_time_unref (set_time);
	}
}

static gboolean
ircium_basic_command_handler_topic_predicate (IrciumMessage *msg,
                                              gpointer       user_data)
{
	return g_strcmp0 (ircium_message_get_command (msg), "332") == 0;
}

static void
ircium_basic_command_handler_topic_cb (IrciumCommandHandler *handler,
                                       IrciumCommandReactor *reactor,
                                       IrciumMessage        *msg,
                                       gpointer              user_data)
{
	const GPtrArray *params = ircium_message_get_params (msg);
	gchar *channel = params->pdata[1];
	gchar *topic = params->pdata[2];

	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	GListModel *list = ircium_session_get_buffer_list (session);

	IrciumBuffer *buf = NULL;
	for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
		IrciumBuffer *tmp = g_list_model_get_item (list, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
		// The buffer title is also the channel name.
		gchar *chan_name = ircium_buffer_get_title (tmp);
		// TODO: Casefold here just in case.
		if (g_strcmp0 (chan_name, channel) == 0) {
			buf = tmp;
			break;
		}
	}
	if (G_UNLIKELY (buf == NULL)) {
		g_debug ("We were informed of a topic change on a channel "
			 "we are not in!");
		return;
	} else {
		ircium_buffer_set_topic (buf, topic);
	}
}

static gboolean
ircium_basic_command_handler_no_topic_predicate (IrciumMessage *msg,
                                                 gpointer       user_data)
{
	return g_strcmp0 (ircium_message_get_command (msg), "331") == 0;
}

static void
ircium_basic_command_handler_no_topic_cb (IrciumCommandHandler *handler,
                                          IrciumCommandReactor *reactor,
                                          IrciumMessage        *msg,
                                          gpointer              user_data)
{
	const GPtrArray *params = ircium_message_get_params (msg);
	gchar *channel = params->pdata[1];

	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	GListModel *list = ircium_session_get_buffer_list (session);

	IrciumBuffer *buf = NULL;
	for (gsize i = 0; i < g_list_model_get_n_items (list); ++i) {
		IrciumBuffer *tmp = g_list_model_get_item (list, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (tmp)) continue;
		// The buffer title is also the channel name.
		gchar *chan_name = ircium_buffer_get_title (tmp);
		// TODO: Casefold here just in case.
		if (g_strcmp0 (chan_name, channel) == 0) {
			buf = tmp;
			break;
		}
	}
	if (G_UNLIKELY (buf == NULL)) {
		g_debug ("We were informed of a topic on a channel "
			 "we are not in!");
		return;
	} else {
		ircium_buffer_set_topic (buf, "");
	}
}

static gboolean
ircium_basic_command_handler_topic_setter_predicate (IrciumMessage *msg,
                                                     gpointer       user_data)
{
	return g_strcmp0 (ircium_message_get_command (msg), "333") == 0;
}

static void
ircium_basic_command_handler_topic_setter_cb (IrciumCommandHandler *handler,
                                              IrciumCommandReactor *reactor,
                                              IrciumMessage        *msg,
                                              gpointer              user_data)
{
	const GPtrArray *params = ircium_message_get_params (msg);
	gchar *channel = params->pdata[1];
	g_autofree gchar *setter = g_strdup (params->pdata[2]);
	{
		g_autoptr (IrciumMessageSource) setter_source =
			ircium_message_source_new (setter);
		// The setter could either be a simple nick or a full
		// <nick>!<user>@<host>.
		// Make sure we only get the nick in either case for
		// better user experience.
		if (ircium_message_source_is_user (setter_source)) {
			const gchar *nick =
				ircium_message_source_get_nick (setter_source);

			g_free (setter);
			setter = g_strdup (nick);
		}
	}

	gchar *stamp_start = params->pdata[3];
	gchar *stamp_end = NULL;
	gint64 timestamp = g_ascii_strtoll (params->pdata[3], &stamp_end, 10);

	goffset stamp_diff = stamp_end - stamp_start;
	if (stamp_diff != strlen (stamp_start)) {
		g_debug ("Whole UNIX timestamp was not parsed: "
			 "%"G_GOFFSET_FORMAT", %"G_GOFFSET_FORMAT,
			 stamp_diff,
			 strlen (stamp_start));
		return;
	}

	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	IrciumChannelBuffer *buf = NULL;
	GListModel *buffers = ircium_session_get_buffer_list (session);
	for (gsize i = 0; i < g_list_model_get_n_items (buffers); ++i) {
		IrciumBuffer *b = g_list_model_get_item (buffers, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (b)) continue;
		// TODO: Casefold just in case.
		if (g_strcmp0 (ircium_buffer_get_title (b), channel) == 0) {
			buf = IRCIUM_CHANNEL_BUFFER (b);
			break;
		}
	}
	if (G_UNLIKELY (buf == NULL)) {
		g_debug ("We're being informed about the topic of a channel "
			 "we're not a part of");
		return;
	}
	g_autoptr (GDateTime) set_time =
		g_date_time_new_from_unix_utc (timestamp);

	ircium_buffer_set_topic_metadata (IRCIUM_BUFFER (buf),
					  setter, set_time);
}

static gboolean
ircium_basic_command_handler_names_predicate (IrciumMessage *msg,
                                              gpointer       user_data)
{
	const gchar *command = ircium_message_get_command (msg);
	return g_strcmp0 (command, "353") == 0 ||
	       g_strcmp0 (command, "366") == 0;
}

static void
ircium_basic_command_handler_names_cb (IrciumCommandHandler *handler,
                                       IrciumCommandReactor *reactor,
                                       IrciumMessage        *msg,
                                       gpointer              user_data)
{
	const gchar *command = ircium_message_get_command (msg);
	// We don't do anything with the "End of /NAMES list" other than
	// detect and cosume it.
	if (g_strcmp0 (command, "366") == 0) return;

	IrciumSession *session = ircium_command_reactor_get_session (reactor);

	const GPtrArray *params = ircium_message_get_params (msg);
	g_assert (params->len == 4);

	gchar *channel = params->pdata[2];
	gchar *names = params->pdata[3];

	IrciumChannelBuffer *buf = NULL;
	GListModel *buffers = ircium_session_get_buffer_list (session);
	for (gsize i = 0; i < g_list_model_get_n_items (buffers); ++i) {
		IrciumBuffer *b = g_list_model_get_item (buffers, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (b)) continue;
		// TODO: Casefold just in case.
		if (g_strcmp0 (ircium_buffer_get_title (b), channel) == 0) {
			buf = IRCIUM_CHANNEL_BUFFER (b);
			break;
		}
	}
	if (G_UNLIKELY (buf == NULL)) {
		g_debug ("We're being informed about the nick list of "
			 "a channel we're not a part of");
		return;
	}

	g_auto (GStrv) prefixed_nicks = g_strsplit (names, " ", -1);
	GHashTable *isupport = ircium_session_get_isupport (session);

	gchar *isupport_prefix = g_hash_table_lookup (isupport, "PREFIX");

	for (gsize i = 0; i < g_strv_length (prefixed_nicks); ++i) {
		gchar *nick = prefixed_nicks[i];
		if (nick == NULL || *nick == '\0') continue;
		// We will have to remove the prefix if there is any.
		gssize prefix_index = -1;
		gchar prefix_mode = '\0';
		gchar *real_nick = nick;
		if (isupport_prefix != NULL) {
			gchar *prefixes = g_strrstr (isupport_prefix, ")");
			if (prefixes++ == NULL) {
				g_debug ("ISUPPORT PREFIX [%s] is incorrect",
					 isupport_prefix);
			} else {
				gsize j = 0;
				for (gchar *needle = prefixes;
				     *needle != '\0'; ++needle) {
					if (*nick == *needle) {
						prefix_index = j;
						break;
					}
					++j;
				}

				if (prefix_index != -1) {
					++real_nick;
					gchar *modes =
						strstr (isupport_prefix, "(");
					++modes;
					prefix_mode = modes[prefix_index];
				}
			}
		}
		IrciumUser *assoc_user =
			ircium_session_find_user_nick (session, real_nick);
		if (assoc_user == NULL) {
			assoc_user = ircium_user_new (session, real_nick);
			ircium_session_add_user (session, assoc_user);
		}
		IrciumChannelUser *user =
			ircium_channel_buffer_find_user (buf, assoc_user);
		if (user == NULL) {
			ircium_channel_buffer_add_user (buf, assoc_user);
			user = ircium_channel_buffer_find_user (buf,
								assoc_user);
		}
		if (prefix_index != -1) {
			ircium_channel_user_set_mode (user, prefix_mode);
		}
	}
}

static gboolean
ircium_basic_command_handler_nick_predicate (IrciumMessage *msg,
                                             gpointer       user_data)
{
	return g_strcmp0 ("NICK", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_nick_cb (IrciumCommandHandler *handler,
                                      IrciumCommandReactor *reactor,
                                      IrciumMessage        *msg,
                                      gpointer              user_data)
{
	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	g_autoptr(IrciumMessageSource) source =
		ircium_message_source_new (ircium_message_get_source (msg));
	if (!ircium_message_source_is_user (source)) {
		g_debug ("Server [%s] tried to change its nick!",
			 ircium_message_source_get_server_host (source));
		return;
	}
	IrciumUser *user =
		ircium_session_find_user_nick (
			session,
			ircium_message_source_get_nick (source)
	);
	if (user == NULL) {
		g_debug ("We were informed about a nick change of a user "
			 "we don't know!");
		return;
	}
	const GPtrArray *params = ircium_message_get_params (msg);
	const gchar *new_nick = params->pdata[0];
	gboolean is_self = ircium_session_get_self (session) == user;

	GListModel *buffers = ircium_session_get_buffer_list (session);

	gchar *msg_text = NULL;
	if (is_self) {
		msg_text = g_strdup_printf (
			_("You have changed your nick to %s"),
			new_nick);
	} else {
		const gchar *old_nick = ircium_message_source_get_nick (source);
		msg_text = g_strdup_printf (
			_("%s has change their nick to %s"),
			old_nick, new_nick);
	}
	GDateTime *t = g_date_time_new_now_utc ();
	IrciumBufferLine *line =
	ircium_buffer_line_new_with_sender
		(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
		 0,
		 source,
		 t,
		 msg_text);
	g_date_time_unref (t);
	for (gsize i = 0; i < g_list_model_get_n_items (buffers); ++i) {
		IrciumBuffer *buf = g_list_model_get_item (buffers, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (buf)) continue;

		IrciumChannelBuffer *cb = IRCIUM_CHANNEL_BUFFER (buf);
		if (ircium_channel_buffer_find_user (cb, user) != NULL) {
			ircium_buffer_add_line (IRCIUM_BUFFER (buf), line);
		}
	}
	g_object_unref (line);
	g_free (msg_text);

	ircium_user_set_nick (user, new_nick);
}

static gboolean
ircium_basic_command_handler_quit_predicate (IrciumMessage *msg,
                                             gpointer       user_data)
{
	return g_strcmp0 ("QUIT", ircium_message_get_command (msg)) == 0;
}

static void
ircium_basic_command_handler_quit_cb (IrciumCommandHandler *handler,
                                      IrciumCommandReactor *reactor,
                                      IrciumMessage        *msg,
                                      gpointer              user_data)
{
	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	g_autoptr(IrciumMessageSource) source =
		ircium_message_source_new (ircium_message_get_source (msg));

	if (!ircium_message_source_is_user (source)) {
		g_debug ("A server tried to quit the server‽");
		return;
	}

	const gchar *nick = ircium_message_source_get_nick (source);

	const GPtrArray *params = ircium_message_get_params (msg);
	IrciumUser *user = ircium_session_find_user_nick (session, nick);
	if (user == NULL) {
		g_debug ("We were informed of an unknown user quitting");
		return;
	}

	const gchar *quit_reason = params->pdata[0];
	gchar *msg_text =
		g_strdup_printf (_("%s has quit the server (%s)"),
				 nick, quit_reason);

	GDateTime *t = g_date_time_new_now_utc ();
	IrciumBufferLine *line =
	ircium_buffer_line_new_with_sender
		(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
		 0,
		 source,
		 t,
		 msg_text);
	g_date_time_unref (t);

	GListModel *buffers = ircium_session_get_buffer_list (session);
	for (gsize i = 0; i < g_list_model_get_n_items (buffers); ++i) {
		IrciumBuffer *buffer = g_list_model_get_item (buffers, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (buffer)) continue;
		IrciumChannelBuffer *cb = IRCIUM_CHANNEL_BUFFER (buffer);
		if (ircium_channel_buffer_find_user (cb, user) != NULL) {
			ircium_channel_buffer_remove_user (cb, user);
			ircium_buffer_add_line (buffer, line);
		}
	}
	g_object_unref (line);
	g_free (msg_text);

	ircium_session_garbage_collect_users (session);
}

static gboolean
ircium_basic_command_handler_kick_predicate (IrciumMessage *msg,
                                             gpointer       user_data)
{
	return g_strcmp0 ("KICK", ircium_message_get_command (msg)) == 0;
}

static gchar *
get_kick_text (gboolean     kicker_known,
               gboolean     kicker_self,
               gboolean     kicked_self,
               const gchar *kicker_nick,
               gchar       *kick_target,
               gchar       *kick_reason);
static void
ircium_basic_command_handler_kick_cb (IrciumCommandHandler *handler,
                                      IrciumCommandReactor *reactor,
                                      IrciumMessage        *msg,
                                      gpointer              user_data)
{
	IrciumSession *session = ircium_command_reactor_get_session (reactor);
	g_autoptr(IrciumMessageSource) kicker_source =
		ircium_message_source_new (ircium_message_get_source (msg));

	const GPtrArray *params = ircium_message_get_params (msg);

	gchar *kick_channel = params->pdata[0];
	gchar *kick_target = params->pdata[1];
	gchar *kick_reason = params->len == 3 ? params->pdata[2] : NULL;

	IrciumUser *kicked =
		ircium_session_find_user_nick (session, kick_target);
	if (kicked == NULL) {
		g_debug ("We're being informed about a kick of"
			 "an unknown user");
		return;
	}

	if (kicker_source == NULL) {
		g_debug ("Server hasn't sent us the kicker!");
		return;
	}

	gboolean kicker_self = FALSE;
	gboolean kicker_known = FALSE;
	const gchar *kicker_nick = NULL;
	if (ircium_message_source_is_user (kicker_source)) {
		kicker_nick =
			ircium_message_source_get_nick (kicker_source);
		IrciumUser *kicker =
			ircium_session_find_user_nick (session, kicker_nick);
		if (kicker != NULL) {
			kicker_known = TRUE;
			kicker_self =
				ircium_session_get_self (session) == kicker;
		}
	}

	gboolean kicked_self = ircium_session_get_self (session) == kicked;

	gchar *msg_text = get_kick_text (kicker_known,
					 kicker_self,
					 kicked_self,
					 kicker_nick,
					 kick_target,
					 kick_reason);

	GDateTime *t = g_date_time_new_now_utc ();

	IrciumBufferLine *line =
		ircium_buffer_line_new_with_sender
			(IRCIUM_BUFFER_LINE_TYPE_COMMAND,
			 0,
			 kicker_source,
			 t,
			 msg_text);
	g_date_time_unref (t);

	GListModel *buffers = ircium_session_get_buffer_list (session);
	for (gsize i = 0; i < g_list_model_get_n_items (buffers); ++i) {
		IrciumBuffer *buf = g_list_model_get_item (buffers, i);
		if (!IRCIUM_IS_CHANNEL_BUFFER (buf)) continue;
		IrciumChannelBuffer *cbuf = IRCIUM_CHANNEL_BUFFER (buf);
		// TODO: Casefold just in case
		if (g_strcmp0 (ircium_buffer_get_title (buf),
			       kick_channel) == 0) {
			ircium_buffer_add_line (buf, line);
			ircium_channel_buffer_remove_user (cbuf, kicked);
			break;
		}
	}
	ircium_session_garbage_collect_users (session);

	g_object_unref (line);
	g_free (msg_text);
}

static void
ircium_basic_command_handler_init (IrciumBasicCommandHandler *self)
{
	self->unknown_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_LOWEST_PRIORITY,
		 ircium_basic_command_handler_unknown_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_unknown_cb,
		 NULL, NULL);

	self->ping_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_ping_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_ping_cb,
		 NULL, NULL);

	self->notice_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_notice_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_notice_cb,
		 NULL, NULL);

	self->privmsg_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_privmsg_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_privmsg_cb,
		 NULL, NULL);

	self->generic_numeric_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_LOWEST_PRIORITY - 10,
		 ircium_basic_command_handler_numeric_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_numeric_cb,
		 NULL, NULL);

	self->isupport_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY + 10,
		 ircium_basic_command_handler_isupport_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_isupport_cb,
		 NULL, NULL);

	self->join_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_join_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_join_cb,
		 NULL, NULL);

	self->part_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_part_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_part_cb,
		 NULL, NULL);

	self->topic_set_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_topic_set_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_topic_set_cb,
		 NULL, NULL);

	self->topic_numeric_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_topic_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_topic_cb,
		 NULL, NULL);

	self->no_topic_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_no_topic_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_no_topic_cb,
		 NULL, NULL);

	self->topic_setter_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_topic_setter_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_topic_setter_cb,
		 NULL, NULL);

	self->names_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY + 10,
		 ircium_basic_command_handler_names_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_names_cb,
		 NULL, NULL);

	self->nick_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_nick_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_nick_cb,
		 NULL, NULL);

	self->quit_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_quit_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_quit_cb,
		 NULL, NULL);

	self->kick_handler = ircium_command_new_reactor_handler
		(IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY,
		 ircium_basic_command_handler_kick_predicate,
		 IRCIUM_COMMAND_HANDLER (self),
		 ircium_basic_command_handler_kick_cb,
		 NULL, NULL);
}

static void
ircium_basic_command_handler_register_handlers (IrciumCommandHandler *handler,
                                                IrciumCommandReactor *reactor)
{
	IrciumBasicCommandHandler *self = (IrciumBasicCommandHandler *) handler;
	g_return_if_fail (IRCIUM_IS_BASIC_COMMAND_HANDLER (self));
	g_return_if_fail (IRCIUM_IS_COMMAND_REACTOR (reactor));
	ircium_command_reactor_register (reactor, self->unknown_handler);
	ircium_command_reactor_register (reactor, self->ping_handler);
	ircium_command_reactor_register (reactor, self->notice_handler);
	ircium_command_reactor_register (reactor, self->privmsg_handler);
	ircium_command_reactor_register (reactor,
					 self->generic_numeric_handler);
	ircium_command_reactor_register (reactor, self->isupport_handler);
	ircium_command_reactor_register (reactor, self->join_handler);
	ircium_command_reactor_register (reactor, self->part_handler);
	ircium_command_reactor_register (reactor, self->topic_set_handler);
	ircium_command_reactor_register (reactor, self->topic_numeric_handler);
	ircium_command_reactor_register (reactor, self->no_topic_handler);
	ircium_command_reactor_register (reactor, self->topic_setter_handler);

	ircium_command_reactor_register (reactor, self->names_handler);
	ircium_command_reactor_register (reactor, self->nick_handler);
	ircium_command_reactor_register (reactor, self->quit_handler);
	ircium_command_reactor_register (reactor, self->kick_handler);
}

static void
ircium_basic_command_handler_unregister_handlers (IrciumCommandHandler *handler,
                                                  IrciumCommandReactor *reactor)
{
	IrciumBasicCommandHandler *self = (IrciumBasicCommandHandler *) handler;
	g_return_if_fail (IRCIUM_IS_BASIC_COMMAND_HANDLER (self));
	g_return_if_fail (IRCIUM_IS_COMMAND_REACTOR (reactor));
	ircium_command_reactor_unregister (reactor, self->unknown_handler);
	ircium_command_reactor_unregister (reactor, self->ping_handler);
	ircium_command_reactor_unregister (reactor, self->notice_handler);
	ircium_command_reactor_unregister (reactor, self->privmsg_handler);
	ircium_command_reactor_unregister (reactor,
					   self->generic_numeric_handler);
	ircium_command_reactor_unregister (reactor, self->isupport_handler);
	ircium_command_reactor_unregister (reactor, self->join_handler);
	ircium_command_reactor_unregister (reactor, self->part_handler);
	ircium_command_reactor_unregister (reactor, self->topic_set_handler);
	ircium_command_reactor_unregister (reactor,
					   self->topic_numeric_handler);
	ircium_command_reactor_unregister (reactor, self->no_topic_handler);
	ircium_command_reactor_unregister (reactor, self->topic_setter_handler);

	ircium_command_reactor_unregister (reactor, self->names_handler);
	ircium_command_reactor_unregister (reactor, self->nick_handler);
	ircium_command_reactor_unregister (reactor, self->quit_handler);
	ircium_command_reactor_unregister (reactor, self->kick_handler);
}

static gchar *
get_kick_text (gboolean     kicker_known,
               gboolean     kicker_self,
               gboolean     kicked_self,
               const gchar *kicker_nick,
               gchar       *kick_target,
               gchar       *kick_reason)
{
	if (kicker_known && kicker_self && kicked_self && kick_reason) {
		return g_strdup_printf (
			_("You have kicked yourself (Reason: %s)"),
			kick_reason);
	}
	if (kicker_known && kicker_self && kicked_self) {
		return g_strdup (_("You have kicked yourself"));
	}

	if (kicker_known && kicker_self && kick_reason) {
		return g_strdup_printf (
			_("You have kicked %s (Reason: %s)"),
			kick_target, kick_reason);
	}

	if (kicker_known && kicker_self) {
		return g_strdup_printf (_("You have kicked %s"), kick_target);
	}

	if (kicker_known && kicked_self && kick_reason) {
		return g_strdup_printf (_("%s has kicked you (Reason: %s)"),
					kicker_nick, kick_reason);
	}

	if (kicker_known && kicked_self) {
		return g_strdup_printf (_("%s has kicked you"), kicker_nick);
	}

	if (kicker_known && kick_reason) {
		return g_strdup_printf (_("%s has kicked %s (Reason: %s)"),
					kicker_nick, kick_target, kick_reason);
	}

	if (kicker_known) {
		return g_strdup_printf (_("%s has kicked %s"),
					kicker_nick, kick_target);
	}

	if (kick_reason) {
		return g_strdup_printf (_("%s has been kicked (Reason: %s)"),
					kick_target, kick_reason);
	} else {
		return g_strdup_printf (_("%s has been kicked"),
					kick_target);
	}
}
