/* ircium-message-source.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define IRCIUM_TYPE_MESSAGE_SOURCE (ircium_message_source_get_type())

G_DECLARE_FINAL_TYPE (IrciumMessageSource,
		      ircium_message_source,
		      IRCIUM, MESSAGE_SOURCE,
		      GObject)

IrciumMessageSource *ircium_message_source_new (const gchar *source);

gboolean ircium_message_source_is_user (const IrciumMessageSource *self);

const gchar *ircium_message_source_get_nick (const IrciumMessageSource *self);
const gchar *ircium_message_source_get_user (const IrciumMessageSource *self);
const gchar *ircium_message_source_get_host (const IrciumMessageSource *self);

const gchar *
ircium_message_source_get_server_host (const IrciumMessageSource *self);

G_END_DECLS
