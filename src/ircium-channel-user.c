/* ircium-channel-user.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-channel-user.h"

struct _IrciumChannelUser
{
	GObject parent_instance;

	IrciumUser *user;

	gchar *nick;

	gchar *modes;
};

G_DEFINE_TYPE (IrciumChannelUser, ircium_channel_user, G_TYPE_OBJECT)

enum {
	PROP_0,

	PROP_USER,
	PROP_NICK,
	PROP_MODES,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumChannelUser *
ircium_channel_user_new (IrciumUser *assoc_user)
{
	return g_object_new (IRCIUM_TYPE_CHANNEL_USER,
			     "user", assoc_user,
			     NULL);
}

static void
ircium_channel_user_finalize (GObject *object)
{
	IrciumChannelUser *self = (IrciumChannelUser *)object;

	g_free (self->nick);
	g_free (self->modes);

	G_OBJECT_CLASS (ircium_channel_user_parent_class)->finalize (object);
}

static void
ircium_channel_user_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
	IrciumChannelUser *self = IRCIUM_CHANNEL_USER (object);

	switch (prop_id)
	{
	case PROP_USER:
		g_value_set_object (value, self->user);
		break;
	case PROP_NICK:
		g_value_set_string (value, self->nick);
		break;
	case PROP_MODES:
		g_value_set_string (value, self->modes);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_channel_user_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
	IrciumChannelUser *self = IRCIUM_CHANNEL_USER (object);

	switch (prop_id)
	{
	case PROP_USER:
		g_clear_object (&self->user);
		self->user = g_value_dup_object (value);

		g_object_bind_property (self->user, "nick",
					self, "nick",
					G_BINDING_BIDIRECTIONAL);
		g_clear_pointer (&self->nick, g_free);
		self->nick = g_strdup (ircium_user_get_nick (self->user));
		break;
	case PROP_NICK:
		g_clear_pointer (&self->nick, g_free);
		self->nick = g_value_dup_string (value);
		break;
	case PROP_MODES:
		g_clear_pointer (&self->modes, g_free);
		self->modes = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_channel_user_class_init (IrciumChannelUserClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ircium_channel_user_finalize;
	object_class->get_property = ircium_channel_user_get_property;
	object_class->set_property = ircium_channel_user_set_property;

	properties[PROP_USER] =
		g_param_spec_object ("user",
				     "user",
				     "user",
				     IRCIUM_TYPE_USER,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_NICK] =
		g_param_spec_string ("nick",
				     "nick",
				     "nick",
				     "",
				     G_PARAM_READWRITE);

	properties[PROP_MODES] =
		g_param_spec_string ("modes",
				     "modes",
				     "modes",
				     "",
				     G_PARAM_READWRITE);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_channel_user_init (IrciumChannelUser *self)
{
}

void
ircium_channel_user_set_mode (IrciumChannelUser *user,
                              gchar              mode)
{
	if (ircium_channel_user_has_mode (user, mode)) return;

	gchar *tmp = user->modes;
	user->modes = g_strdup_printf ("%s%c", tmp, mode);
	g_free (tmp);
}

void
ircium_channel_user_unset_mode (IrciumChannelUser *user,
                                gchar              mode)
{
	if (!ircium_channel_user_has_mode (user, mode)) return;

	GString *str = g_string_new (NULL);
	for (gchar *iter = user->modes; *iter != '\0'; ++iter) {
		gchar c = *iter;
		if (c != mode) g_string_append_c (str, c);
	}
}

gboolean
ircium_channel_user_has_mode (IrciumChannelUser *user,
                              gchar              mode)
{
	gboolean found = FALSE;
	for (gchar *iter = user->modes; iter && *iter != '\0'; ++iter) {
		if (*iter == mode) {
			found = TRUE;
			break;
		}
	}
	return found;
}

/**
 * ircium_channel_user_get_modes:
 *
 * @user: the user
 *
 * Returns: (transfer none): The user's modes on the current channel.
 *
 * Since: 0.4
 */
gchar *
ircium_channel_user_get_modes (IrciumChannelUser *user)
{
	return user->modes;
}

IrciumUser *
ircium_channel_user_get_user (IrciumChannelUser *user)
{
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_USER (user), NULL);
	return user->user;
}

const gchar *
ircium_channel_user_get_nick (IrciumChannelUser *user)
{
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_USER (user), NULL);
	return user->nick;
}
