/* ircium-command-reactor-private.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "ircium-command-reactor.h"
#include "ircium-command-handler.h"

G_BEGIN_DECLS

#define IRCIUM_COMMAND_REACTOR_HANDLER_LOWEST_PRIORITY (G_MAXINT)
#define IRCIUM_COMMAND_REACTOR_HANDLER_COMMAND_PRIORITY (0)
#define IRCIUM_COMMAND_REACTOR_HANDLER_HIGHEST_PRIORITY (G_MININT)

typedef gboolean (*CommandHandlerPredicate) (IrciumMessage *msg,
                                             gpointer       user_data);
typedef void (*CommandHandlerCB) (IrciumCommandHandler *handler,
                                  IrciumCommandReactor *reactor,
                                  IrciumMessage        *msg,
                                  gpointer              user_data);

typedef struct {
	gint priority; // Lower means higher priortity;
	CommandHandlerPredicate predicate;
	IrciumCommandHandler *handler;
	CommandHandlerCB callback;
	gpointer user_data;
	GDestroyNotify user_data_free;
} IrciumCommandReactorHandler;

gboolean ircium_command_reactor_register (IrciumCommandReactor        *reactor,
                                          IrciumCommandReactorHandler *handler);
void ircium_command_reactor_unregister (IrciumCommandReactor        *reactor,
                                        IrciumCommandReactorHandler *handler);

IrciumCommandReactorHandler *
ircium_command_new_reactor_handler (gint                     priority,
                                    CommandHandlerPredicate  predicate,
                                    IrciumCommandHandler    *handler,
                                    CommandHandlerCB         callback,
                                    gpointer                 user_data,
                                    GDestroyNotify           user_data_free);

IrciumSession *
ircium_command_reactor_get_session (IrciumCommandReactor *reactor);

G_END_DECLS
