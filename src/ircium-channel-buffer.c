/* ircium-channel-buffer.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-channel-buffer.h"
#include "ircium-buffer-private.h"

struct _IrciumChannelBuffer
{
	IrciumBuffer parent_instance;

	gchar *name;
	gchar *topic;
	gchar *topic_setter;
	GDateTime *topic_set_time;

	GListStore *channel_store;

	GListStore *users;

	IrciumSession *session;
};

G_DEFINE_TYPE (IrciumChannelBuffer, ircium_channel_buffer, IRCIUM_TYPE_BUFFER)

enum {
	PROP_0,
	PROP_CHANNAME,
	PROP_TOPIC,
	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumChannelBuffer *
ircium_channel_buffer_new (gchar *name)
{
	return g_object_new (IRCIUM_TYPE_CHANNEL_BUFFER,
			     "channame", name,
			     "topic", "",
			     NULL);
}

static void
ircium_channel_buffer_dispose (GObject *object)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *)object;

	g_clear_object (&self->channel_store);
	g_clear_object (&self->users);

	G_OBJECT_CLASS (ircium_channel_buffer_parent_class)->dispose (object);
}

static void
ircium_channel_buffer_finalize (GObject *object)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *)object;

	g_clear_pointer (&self->name, g_free);
	g_clear_pointer (&self->topic, g_free);

	g_clear_pointer (&self->topic_setter, g_free);
	g_clear_pointer (&self->topic_set_time, g_date_time_unref);

	G_OBJECT_CLASS (ircium_channel_buffer_parent_class)->finalize (object);
}

static void
ircium_channel_buffer_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
	IrciumChannelBuffer *self = IRCIUM_CHANNEL_BUFFER (object);

	switch (prop_id)
	{
	case PROP_CHANNAME:
		g_value_set_string (value, self->name);
		break;
	case PROP_TOPIC:
		g_value_set_string (value, self->topic);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_channel_buffer_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
	IrciumChannelBuffer *self = IRCIUM_CHANNEL_BUFFER (object);

	switch (prop_id)
	{
	case PROP_CHANNAME:
		self->name = g_value_dup_string (value);
		break;
	case PROP_TOPIC:
		self->topic = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}
static GListModel *ircium_channel_buffer_get_contents (IrciumBuffer *buffer);
static gchar *ircium_channel_buffer_get_title (IrciumBuffer *buffer);
static void ircium_channel_buffer_add_line (IrciumBuffer     *buffer,
                                            IrciumBufferLine *line);
static void ircium_channel_buffer_handle_entry (IrciumBuffer *buffer,
                                                gchar        *entry_text);
static void ircium_channel_buffer_set_session (IrciumBuffer *buffer,
                                               gpointer      session);
static gboolean ircium_channel_buffer_has_topic (IrciumBuffer *buffer);
static gchar *ircium_channel_buffer_get_topic (IrciumBuffer *buffer);
static void ircium_channel_buffer_set_topic (IrciumBuffer *buffer,
                                             gchar        *topic);
static void ircium_channel_buffer_set_topic_metadata (IrciumBuffer *buffer,
                                                      const gchar  *nick,
                                                      GDateTime    *time);

static void
ircium_channel_buffer_class_init (IrciumChannelBufferClass *klass)
{
	IrciumBufferClass *buffer_class = (IrciumBufferClass *) klass;

	buffer_class->get_contents = ircium_channel_buffer_get_contents;
	buffer_class->get_title = ircium_channel_buffer_get_title;
	buffer_class->add_line = ircium_channel_buffer_add_line;
	buffer_class->handle_entry = ircium_channel_buffer_handle_entry;
	buffer_class->set_session = ircium_channel_buffer_set_session;
	buffer_class->has_topic = ircium_channel_buffer_has_topic;
	buffer_class->get_topic = ircium_channel_buffer_get_topic;
	buffer_class->set_topic = ircium_channel_buffer_set_topic;
	buffer_class->set_topic_metadata =
		ircium_channel_buffer_set_topic_metadata;

	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = ircium_channel_buffer_dispose;
	object_class->finalize = ircium_channel_buffer_finalize;
	object_class->get_property = ircium_channel_buffer_get_property;
	object_class->set_property = ircium_channel_buffer_set_property;

	properties[PROP_CHANNAME] =
		g_param_spec_string ("channame",
				     "Channel name",
				     "The name of the channel",
				     "",
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);
	properties[PROP_TOPIC] =
		g_param_spec_string ("topic",
				     "Channel topic",
				     "The topic of the channel, if any",
				     "",
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static GListModel *
ircium_channel_buffer_get_contents (IrciumBuffer *buffer)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *) buffer;
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_BUFFER (self), NULL);
	return G_LIST_MODEL (self->channel_store);
}

static gchar *
ircium_channel_buffer_get_title (IrciumBuffer *buffer)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *) buffer;
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_BUFFER (self), NULL);
	return self->name;
}

static void
ircium_channel_buffer_init (IrciumChannelBuffer *self)
{
	self->channel_store = g_list_store_new (IRCIUM_TYPE_BUFFER_LINE);
	self->users = g_list_store_new (IRCIUM_TYPE_CHANNEL_USER);
}

static void
ircium_channel_buffer_add_line (IrciumBuffer     *buffer,
                                IrciumBufferLine *line)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *) buffer;
	g_return_if_fail (IRCIUM_IS_CHANNEL_BUFFER (self));
	g_list_store_append (self->channel_store, line);
}

static gchar **split_msg (gsize  nub_len,
                          gchar *entered_text);

static void echo_messages (IrciumChannelBuffer  *self,
                           gchar               **messages);

static void
ircium_channel_buffer_handle_entry (IrciumBuffer *buffer,
                                    gchar        *entered_text)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *) buffer;
	g_return_if_fail (IRCIUM_IS_CHANNEL_BUFFER (self));
	if (ircium_buffer_is_input_line_command (entered_text)) {
		gchar **cmd =
			(gchar **) ircium_buffer_parse_input_line_command
				(entered_text);
		gboolean was_common =
			ircium_buffer_common_command_executor
				((const gchar **) cmd, self->session);
		if (!was_common) {
			gchar *cmd_str = g_utf8_strdown (*cmd, -1);
			gchar *args = cmd[1];
			gboolean found = FALSE;
			if (g_strcmp0 (cmd_str, "part") == 0) {
				found = TRUE;
				gchar *reason = g_strdup (args ? args : "");
				GPtrArray *params =
					g_ptr_array_new_with_free_func (g_free);
				g_ptr_array_add (params, g_strdup (self->name));
				g_ptr_array_add (params, reason);
				IrciumMessage *msg =
					ircium_message_new (NULL,
							    NULL,
							    "PART",
							    params);
				ircium_session_send_message (self->session,
							     msg);
				g_ptr_array_unref (params);
			}
			if (!found) {
				// TODO: Inform user about available
				// channel buffer commands.
			}
			g_free (cmd_str);
		}
		g_strfreev (cmd);
	} else {
		gchar *msg_txt = g_strdup (entered_text);
		ircium_buffer_collapse_slash (&msg_txt);
		// We must split the message if the size goes over the
		// acceptiable threshold.
		gchar *message_nub = g_strdup_printf ("PRIVMSG %s :",
						      self->name);
		gsize nub_len = strlen (message_nub);

		gchar **splits = split_msg (nub_len, msg_txt);
		for (gsize i = 0; i < g_strv_length (splits); ++i) {
			gchar *s = splits[i];
			GPtrArray *params =
				g_ptr_array_new_with_free_func (g_free);
			g_ptr_array_add (params, g_strdup (self->name));
			g_ptr_array_add (params, g_strdup (s));
			IrciumMessage *msg = ircium_message_new (NULL,
								 NULL,
								 "PRIVMSG",
								 params);
			g_ptr_array_unref (params);
			ircium_session_send_message (self->session, msg);
		}
		// TODO: check if `echo-message` capability is enabled
		// and if so, do not add these messages like this.
		echo_messages (self, splits);

		g_strfreev (splits);

		g_free (message_nub);
		g_free (msg_txt);
	}
}

static void
echo_messages (IrciumChannelBuffer  *self,
               gchar               **messages)
{
	const gchar *nick = ircium_session_get_nick (self->session);

	GListModel *channel_store = G_LIST_MODEL (self->channel_store);
	guint items = g_list_model_get_n_items (channel_store);

	IrciumBufferLine *prev_line = items > 0 ?
	g_list_model_get_item (channel_store, items - 1) : NULL;

	gboolean is_continuation = TRUE;
	is_continuation = is_continuation && prev_line != NULL;

	is_continuation = is_continuation &&
		ircium_buffer_line_is_sender_user (prev_line);

	is_continuation = is_continuation &&
		ircium_buffer_line_get_line_type (prev_line) ==
		IRCIUM_BUFFER_LINE_TYPE_NORMAL;

	is_continuation = is_continuation &&
		(ircium_buffer_line_get_flags (prev_line) &
		 ~(IRCIUM_BUFFER_LINE_FLAG_CONTINUATION)) == 0;

	is_continuation = is_continuation &&
		(g_strcmp0
		 (ircium_buffer_line_get_sender_nick (prev_line),
		  nick) == 0);

	GDateTime *t = g_date_time_new_now_utc ();
	for (gsize i = 0; i < g_strv_length (messages); ++i) {
		gchar *s = messages[i];
		IrciumBufferLineFlag new_flags =
			is_continuation ?
			IRCIUM_BUFFER_LINE_FLAG_CONTINUATION :
			0;

		IrciumBufferLine *line =
			ircium_buffer_line_new_with_nick
				(IRCIUM_BUFFER_LINE_TYPE_NORMAL, new_flags,
				 nick, t, s);
		g_list_store_append (self->channel_store, line);
		g_object_unref (line);
		is_continuation = TRUE;
	}
	g_date_time_unref (t);
}

static void
ircium_channel_buffer_set_session (IrciumBuffer *buffer,
                                   gpointer      session)
{
	IrciumChannelBuffer *self = (IrciumChannelBuffer *) buffer;
	g_return_if_fail (IRCIUM_IS_CHANNEL_BUFFER (self));
	self->session = session;
}

static gunichar
get_current_char (gchar **rune_segment);

static gchar **
split_msg (gsize  nub_len,
           gchar *entered_text)
{
	// We could be crude about this, splitting
	// too long messages in the middle of words.
	// However, I want to be able to break the messages
	// up at whitespace.
	//
	// However, I don't want the split up string to be too short to
	// be useful, so if the length of the split up chunk is less than
	// half of the currently operated strig's length, we just
	// split in the middle of the word. This heuristic should produce
	// good enough results.
	//
	// XXX: This would be better if we had facilities to deal with
	// grapheme clusters instead, but what can you do…
	gsize text_len = strlen (entered_text);
	GPtrArray *tmp = g_ptr_array_new ();
	while (nub_len + text_len > 510) {
		gchar *pivot = entered_text + 510;
		// Align pivot on the current codepoint if
		// we're in the middle of one.
		get_current_char (&pivot);
		gchar *suitable = pivot;
		gboolean found_suitable = TRUE;
		for (;; --suitable) {
			gunichar c = get_current_char (&suitable);
			ptrdiff_t diff = suitable - entered_text;
			if (diff < 205) {
				found_suitable = FALSE;
				break;
			}
			if (g_unichar_isgraph (c)) {
				break;
			}
		}

		if (found_suitable) {
			pivot = suitable;
		}

		// The pivot is always aligned on the boundary of
		// a UTF-8 codepoint.
		gchar *next_pivot = g_utf8_next_char (pivot);
		gchar *chunk = get_string_between ((guint8 *) entered_text,
						   (guint8 *) pivot);
		entered_text = next_pivot;
		g_ptr_array_add (tmp, chunk);
		text_len = strlen (entered_text);
	}
	g_ptr_array_add (tmp, g_strdup (entered_text));
	gchar **ret = g_malloc0_n (tmp->len + 1, sizeof (*ret));

	for (gsize i = 0; i < tmp->len; ++i) {
		ret[i] = g_strdup (tmp->pdata[i]);
		g_free (tmp->pdata[i]);
	}
	g_ptr_array_unref (tmp);

	return ret;
}

static gunichar
get_current_char (gchar **rune_segment)
{
	gchar *pos = *rune_segment;
	while ((((*pos) & 0xFF) & (0xC0)) == 0x80) {
		// Decrement to the first byte of the UTF-8 sequence.
		--pos;
	}
	gunichar c = g_utf8_get_char (pos);
	*rune_segment = pos;
	return c;
}

static gboolean
ircium_channel_buffer_has_topic (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_BUFFER (buffer), FALSE);
	return TRUE;
}

static gchar *
ircium_channel_buffer_get_topic (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_BUFFER (buffer), NULL);
	IrciumChannelBuffer *channel_buffer = (IrciumChannelBuffer *) buffer;
	return channel_buffer->topic;
}

static void
ircium_channel_buffer_set_topic (IrciumBuffer *buffer,
                                 gchar        *topic)
{
	g_return_if_fail (IRCIUM_IS_CHANNEL_BUFFER (buffer));
	IrciumChannelBuffer *channel_buffer = (IrciumChannelBuffer *) buffer;
	g_clear_pointer (&channel_buffer->topic, g_free);
	channel_buffer->topic = g_strdup (topic);
	g_object_notify_by_pspec (G_OBJECT (channel_buffer),
				  properties[PROP_TOPIC]);
}

GListModel *
ircium_channel_buffer_get_users (IrciumChannelBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_BUFFER (buffer), NULL);
	return G_LIST_MODEL (buffer->users);
}

IrciumChannelUser *
ircium_channel_buffer_find_user (IrciumChannelBuffer *buffer,
                                 IrciumUser          *user)
{
	g_return_val_if_fail (IRCIUM_IS_CHANNEL_BUFFER (buffer), NULL);
	GListModel *users = G_LIST_MODEL (buffer->users);

	IrciumChannelUser *found_user = NULL;

	for (size_t i = 0; i < g_list_model_get_n_items (users); ++i) {
		IrciumChannelUser *u = g_list_model_get_item (users, i);
		if (ircium_channel_user_get_user (u) == user) {
			found_user = g_object_ref (u);
			break;
		}
	}

	return found_user;
}

void
ircium_channel_buffer_remove_user (IrciumChannelBuffer *buffer,
                                   IrciumUser          *user)
{
	g_assert (IRCIUM_IS_CHANNEL_BUFFER (buffer));
	GListModel *users = G_LIST_MODEL (buffer->users);

	gssize index = -1;
	for (gsize i = 0; i < g_list_model_get_n_items (users); ++i) {
		IrciumChannelUser *u = g_list_model_get_item (users, i);

		if (ircium_channel_user_get_user (u) == user) {
			index = i;
			break;
		}
	}

	if (index == -1) return;

	g_list_store_remove (buffer->users, index);
}

void
ircium_channel_buffer_add_user (IrciumChannelBuffer *buffer,
                                IrciumUser          *user)
{
	g_assert (IRCIUM_IS_CHANNEL_BUFFER (buffer));
	GListModel *users = G_LIST_MODEL (buffer->users);

	gssize index = -1;
	for (gsize i = 0; i < g_list_model_get_n_items (users); ++i) {
		IrciumChannelUser *u = g_list_model_get_item (users, i);

		if (ircium_channel_user_get_user (u) == user) {
			index = i;
			break;
		}
	}

	if (index != -1) return;

	IrciumChannelUser *new_user = ircium_channel_user_new (user);
	g_list_store_append (buffer->users, new_user);
	g_object_unref (new_user);
}

static void
ircium_channel_buffer_set_topic_metadata (IrciumBuffer *buffer,
                                          const gchar  *nick,
                                          GDateTime    *time)
{
	g_return_if_fail (IRCIUM_IS_CHANNEL_BUFFER (buffer));
	IrciumChannelBuffer *self = (IrciumChannelBuffer *) buffer;

	g_clear_pointer (&self->topic_set_time, g_date_time_unref);
	g_free (self->topic_setter);

	self->topic_set_time = g_date_time_ref (time);
	self->topic_setter = g_strdup (nick);

	g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TOPIC]);
}

const gchar *
ircium_channel_buffer_get_topic_setter (IrciumChannelBuffer *self)
{
	return self->topic_setter;
}

const GDateTime *
ircium_channel_buffer_get_topic_set_time (IrciumChannelBuffer *self)
{
	return self->topic_set_time;
}
