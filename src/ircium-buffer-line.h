/* ircium-buffer-line.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "ircium-message-source.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_BUFFER_LINE (ircium_buffer_line_get_type())

G_DECLARE_FINAL_TYPE (IrciumBufferLine,
		      ircium_buffer_line,
		      IRCIUM, BUFFER_LINE,
		      GObject)

typedef enum {
	IRCIUM_BUFFER_LINE_TYPE_NORMAL,
	IRCIUM_BUFFER_LINE_TYPE_COMMAND,

	N_IRCIUM_BUFFER_LINE_TYPES,
} IrciumBufferLineType;

typedef enum {
	IRCIUM_BUFFER_LINE_FLAG_NOTICE = (1 << 0),

	IRCIUM_BUFFER_LINE_FLAG_JOIN = (1 << 1),
	IRCIUM_BUFFER_LINE_FLAG_QUIT = (1 << 2),
	IRCIUM_BUFFER_LINE_FLAG_MOTD = (1 << 3),

	IRCIUM_BUFFER_LINE_FLAG_CONTINUATION = (1 << 4),

	IRCIUM_BUFFER_LINE_FLAG_ACTION = (1 << 5),
} IrciumBufferLineFlag;

IrciumBufferLine *
ircium_buffer_line_new_with_sender (IrciumBufferLineType       type,
                                    IrciumBufferLineFlag       flags,
                                    const IrciumMessageSource *sender,
                                    const GDateTime           *time_received,
                                    const gchar               *content);

IrciumBufferLine *
ircium_buffer_line_new_with_nick (IrciumBufferLineType  type,
                                  IrciumBufferLineFlag  flags,
                                  const gchar          *sender,
                                  const GDateTime      *time_received,
                                  const gchar          *content);

const gboolean ircium_buffer_line_is_sender_known (IrciumBufferLine *line);
const gboolean ircium_buffer_line_is_sender_user (IrciumBufferLine *line);
const gchar *ircium_buffer_line_get_sender_nick (IrciumBufferLine *line);

const GDateTime *ircium_buffer_line_get_time (IrciumBufferLine *line);
const gchar *ircium_buffer_line_get_contents (IrciumBufferLine *line);

const IrciumBufferLineType
ircium_buffer_line_get_line_type (IrciumBufferLine *line);

const IrciumBufferLineFlag
ircium_buffer_line_get_flags (IrciumBufferLine *line);

void ircium_buffer_line_add_flags (IrciumBufferLine     *line,
                                   IrciumBufferLineFlag  new_flags);

void ircium_buffer_line_remove_flags (IrciumBufferLine     *line,
                                      IrciumBufferLineFlag  flags);

G_END_DECLS
