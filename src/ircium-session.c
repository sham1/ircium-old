/* ircium-session.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-session-private.h"
#include "ircium-message.h"
#include "ircium-command-reactor.h"
#include "ircium-buffer-private.h"

#include "ircium-channel-buffer.h"

#include <stdio.h>

#define MAX_BYTES_TO_READ (4096)
#define READ_PRIORITY (G_PRIORITY_DEFAULT - 10)

struct _IrciumSession
{
	GObject parent_instance;

	IrciumConnection *conn;

	guint pending_data_handler;

	GByteArray *pending_data;
	gboolean has_new_data;
	gboolean is_reading;

	GPtrArray *received_message_queue;
	GPtrArray *send_message_queue;

	gboolean can_handle_tags;

	IrciumCommandReactor *reactor;

	IrciumServerBuffer *server_buffer;
	GListStore *buffers;

	IrciumBuffer *active_buffer;

	GHashTable *isupport;

	IrciumUser *self_user;
	GListStore *user_list;
};

G_DEFINE_TYPE (IrciumSession, ircium_session, G_TYPE_OBJECT)

static GInputStream *current_input_from_connection (IrciumSession *self);
static GOutputStream *current_output_from_connection (IrciumSession *self);
static void on_data_received (GObject       *obj,
                              GAsyncResult  *res,
                              IrciumSession *self);

enum {
	PROP_0,

	PROP_CONN,
	PROP_ACTIVE_BUFFER,
	PROP_NICK,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumSession *
ircium_session_new (IrciumConnection *conn)
{
	g_return_val_if_fail (IRCIUM_IS_CONNECTION (conn), NULL);
	const gchar *nick = ircium_connection_get_nickname (conn);
	IrciumSession *self = g_object_new (IRCIUM_TYPE_SESSION,
					    "conn", conn,
					    "nick", nick,
					    NULL);
	return self;
}

static void
ircium_session_dispose (GObject *object)
{
	IrciumSession *self = (IrciumSession *)object;

	g_clear_object (&self->conn);
	g_clear_pointer (&self->pending_data, g_byte_array_unref);
	g_clear_pointer (&self->send_message_queue, g_ptr_array_unref);
	g_clear_pointer (&self->received_message_queue, g_ptr_array_unref);
	g_clear_object (&self->reactor);
	g_clear_object (&self->server_buffer);
	g_clear_object (&self->buffers);
	g_clear_object (&self->user_list);
	g_clear_object (&self->self_user);

	G_OBJECT_CLASS (ircium_session_parent_class)->finalize (object);
}

static void
ircium_session_finalize (GObject *object)
{
	IrciumSession *self = (IrciumSession *)object;

	if (self->pending_data_handler != 0) {
		g_source_remove (self->pending_data_handler);
		self->pending_data_handler = 0;
	}
	g_clear_pointer (&self->isupport, g_hash_table_destroy);

	G_OBJECT_CLASS (ircium_session_parent_class)->finalize (object);
}

static void
ircium_session_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
	IrciumSession *self = IRCIUM_SESSION (object);

	switch (prop_id)
	{
	case PROP_CONN:
		g_value_set_object (value, self->conn);
		break;
	case PROP_ACTIVE_BUFFER:
		g_value_set_object (value, self->active_buffer);
		break;
	case PROP_NICK: {
		const gchar *nick = ircium_user_get_nick (self->self_user);
		g_value_set_string (value, nick);
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_session_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
	IrciumSession *self = IRCIUM_SESSION (object);

	switch (prop_id)
	{
	case PROP_CONN:
		self->conn = g_value_dup_object (value);
		break;
	case PROP_ACTIVE_BUFFER:
		// All buffers are in the `buffers`-list anyhow.
		// No need to dup this.
		self->active_buffer = g_value_get_object (value);
		break;
	case PROP_NICK: {
		const gchar *nick = g_value_get_string (value);
		if (self->self_user == NULL) {
			self->self_user = ircium_user_new (self, nick);
		} else {
			ircium_user_set_nick (self->self_user, nick);
		}
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static gboolean handle_pending_data_cb (IrciumSession *self);

static void
ircium_session_constructed (GObject *object) {
	IrciumSession *self = IRCIUM_SESSION (object);

	self->pending_data_handler =
		g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
				 G_SOURCE_FUNC (handle_pending_data_cb),
				 g_object_ref (self),
				 g_object_unref);

	g_list_store_append (self->user_list, self->self_user);

	// TODO: Do connection handshake correctly with capability negotiation.
	// Mock stuff for now.
	const gchar *nick = ircium_user_get_nick (self->self_user);
	gboolean has_user = ircium_connection_get_username (self->conn) != NULL;
	gboolean has_real = ircium_connection_get_realname (self->conn) != NULL;
	const gchar *user =
		has_user ? ircium_connection_get_username (self->conn) : nick;
	const gchar *real =
		has_real ? ircium_connection_get_realname (self->conn) : nick;

	gchar *password = NULL;
	gboolean has_password =
		ircium_connection_get_server_password (self->conn, &password);
	if (has_password) {
		GPtrArray *pass_params =
			g_ptr_array_new_with_free_func (g_free);
		g_ptr_array_add (pass_params, g_strdup (password));
		IrciumMessage *pass_msg =
			ircium_message_new (NULL, NULL, "PASS", pass_params);
		g_ptr_array_add (self->send_message_queue, pass_msg);
	}

	GPtrArray *nick_params = g_ptr_array_new_full (1, g_free);
	g_ptr_array_add (nick_params, g_strdup (nick));
	IrciumMessage *nick_msg =
		ircium_message_new (NULL, NULL, "NICK", nick_params);
	g_ptr_array_unref (nick_params);
	g_ptr_array_add (self->send_message_queue, nick_msg);

	GPtrArray *user_params = g_ptr_array_new_full (4, g_free);
	g_ptr_array_add (user_params, g_strdup (user));
	g_ptr_array_add (user_params, g_strdup ("0"));
	g_ptr_array_add (user_params, g_strdup ("*"));
	g_ptr_array_add (user_params, g_strdup (real));
	IrciumMessage *user_msg =
		ircium_message_new (NULL, NULL, "USER", user_params);
	g_ptr_array_unref (user_params);
	g_ptr_array_add (self->send_message_queue, user_msg);

	G_OBJECT_CLASS (ircium_session_parent_class)->constructed (object);
}

static void
ircium_session_class_init (IrciumSessionClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->constructed = ircium_session_constructed;
	object_class->finalize = ircium_session_finalize;
	object_class->dispose = ircium_session_dispose;
	object_class->get_property = ircium_session_get_property;
	object_class->set_property = ircium_session_set_property;

	properties[PROP_CONN] = g_param_spec_object
		("conn", "conn", "The connection of the session",
		 IRCIUM_TYPE_CONNECTION,
		 G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
	properties[PROP_ACTIVE_BUFFER] = g_param_spec_object
		("active-buffer",
		 "active-buffer",
		 "The currently active buffer",
		 IRCIUM_TYPE_BUFFER,
		 G_PARAM_READWRITE);
	properties[PROP_NICK] =
		g_param_spec_string
			("nick",
			 "nick",
			 "The user's current nickname",
			 "",
			 G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
default_isupport (GHashTable *table) {
#define o(key,val)\
	g_hash_table_insert (table, g_strdup (#key), g_strdup (#val));

	o (CHANTYPES, #)
	o (PREFIX, (ov)@+)

#undef o
}

void
ircium_session_set_isupport_val (GHashTable *table,
                                 gchar      *param,
                                 gchar      *value)
{
	// This macro sets the value for things that are empty.
#define o(key, val)\
	if (value == NULL && g_strcmp0 (param, #key) == 0) value = #val;
	o (EXCEPTS, e)
	o (INVEX, I)
#undef o
	g_hash_table_replace (table, g_strdup (param), g_strdup (value));
}

static void
ircium_session_init (IrciumSession *self)
{
	self->pending_data = g_byte_array_new ();
	self->send_message_queue =
		g_ptr_array_new_with_free_func (g_object_unref);
	self->received_message_queue =
		g_ptr_array_new_with_free_func (g_object_unref);

	self->reactor = ircium_command_reactor_new (self);

	self->server_buffer = ircium_server_buffer_new ();
	ircium_buffer_set_session (IRCIUM_BUFFER (self->server_buffer), self);
	self->buffers = g_list_store_new (IRCIUM_TYPE_BUFFER);
	g_list_store_append (self->buffers, self->server_buffer);
	self->active_buffer = IRCIUM_BUFFER (self->server_buffer);

	self->isupport = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
						g_free);
	default_isupport (self->isupport);

	self->user_list = g_list_store_new (IRCIUM_TYPE_USER);
}

static void
parse_pending_data (IrciumSession *self);

static gboolean
handle_pending_data_cb (IrciumSession *self)
{
	GCancellable *conn_cancellable =
		ircium_connection_get_conn_cancellable (self->conn);
	gboolean is_cancelled =
		!G_IS_CANCELLABLE (conn_cancellable) ? TRUE :
		g_cancellable_is_cancelled (conn_cancellable);

	if (!self->is_reading && !is_cancelled) {
		self->is_reading = TRUE;
		g_input_stream_read_bytes_async
			(current_input_from_connection (self),
			 MAX_BYTES_TO_READ,
			 READ_PRIORITY,
			 ircium_connection_get_conn_cancellable (self->conn),
			 (GAsyncReadyCallback) on_data_received,
			 self);
	}
	if (self->has_new_data) {
		self->has_new_data = FALSE;
		parse_pending_data (self);
	}

	if (self->received_message_queue->len > 0) {
		IrciumMessage *msg =
			g_object_ref (self->received_message_queue->pdata[0]);
		g_ptr_array_remove_index (self->received_message_queue, 0);

		ircium_command_reactor_react (self->reactor, msg);

		g_object_unref (msg);
	}

	if (self->send_message_queue->len > 0) {
		IrciumMessage *msg =
			g_object_ref (self->send_message_queue->pdata[0]);
		g_ptr_array_remove_index (self->send_message_queue, 0);

		GBytes *bytes = ircium_message_serialize (msg);

		gsize len = 0;
		guint8 *data = g_bytes_unref_to_data (bytes, &len);
		g_autoptr (GError) error = NULL;
		gboolean write_success =
			g_output_stream_write_all
				(current_output_from_connection (self),
				 data,
				 len,
				 NULL,
				 ircium_connection_get_conn_cancellable
					 (self->conn),
				 &error);

		if (!write_success && error->code != G_IO_ERROR_CANCELLED) {
			// TODO: Signal that the connection seems to have failed
			g_debug ("Error writing to connection: %s",
				 error->message);
			GCancellable *conn_cancellable =
				ircium_connection_get_conn_cancellable
					(self->conn);
			if (!g_cancellable_is_cancelled (conn_cancellable)) {
				g_cancellable_cancel (conn_cancellable);
			}
		}

		g_object_unref (msg);
	}
	if (is_cancelled) self->pending_data_handler = 0;
	return is_cancelled ? G_SOURCE_REMOVE : G_SOURCE_CONTINUE;
}

static void
message_from_pending_stub (IrciumSession *self,
                           gchar         *stub)
{
	gchar *msg_str = g_strdup_printf ("%s\r\n", stub);

	gchar *escaped_msg =
	g_strescape (msg_str, NULL);

	IrciumMessage *msg = ircium_message_parse (self->pending_data,
						   self->can_handle_tags);
	if (msg == NULL) {
		g_debug ("Malformed message received: %s", escaped_msg);
	} else {
		g_ptr_array_add (self->received_message_queue,
				 msg);
	}
	g_free (escaped_msg);
	g_free (msg_str);

}

static void
parse_pending_data (IrciumSession *self)
{
	// Now we have to find whatever messages from this queue.
	// There may be multiple.
	guint8 *head = self->pending_data->data;
	for (gssize i = 0; i < self->pending_data->len; ++i) {
		guint8 *iter = self->pending_data->data + i;
		// We're at an \n. We may or may not have an \r there.
		// Let's check and react accordingly.
		// There should be a \r there, but there might not be.
		if (*iter == '\n') {
			gchar *stub = NULL;
			if (*(iter - 1) == '\r') {
				stub = get_string_between (head, iter - 2);
			} else {
				stub = get_string_between (head, iter - 1);
			}
			message_from_pending_stub (self, stub);
			g_free (stub);

			g_byte_array_remove_range (self->pending_data,
						   0, i + 1);
			i = -1;
			continue;
		}
		// If instead the next character is \r, we can check for that.
		// If it's followed by \n, we're in luck,
		// and can just proceed to go around the loop again.
		//
		// If it's not, do as above to frame the message
		// (stupid non-compliant servers.)
		if (*iter == '\r') {
			if (*(iter + 1) == '\n') continue;
			gchar *stub = get_string_between (head, iter - 1);
			message_from_pending_stub (self, stub);
			g_free (stub);

			g_byte_array_remove_range (self->pending_data,
						   0, i + 1);
			i = -1;
			continue;
		}
	}
}

static GInputStream *
current_input_from_connection (IrciumSession *self)
{
	GIOStream *stream = ircium_connection_get_current_stream (self->conn);
	return g_io_stream_get_input_stream (stream);
}

static GOutputStream *
current_output_from_connection (IrciumSession *self)
{
	GIOStream *stream = ircium_connection_get_current_stream (self->conn);
	return g_io_stream_get_output_stream (stream);
}

static void
on_data_received (GObject       *obj,
                  GAsyncResult  *res,
                  IrciumSession *self)
{
	GInputStream *istream = (GInputStream *) obj;
	GError *error = NULL;

	self->is_reading = FALSE;

	GBytes *recv_bytes = g_input_stream_read_bytes_finish (istream,
							       res,
							       &error);
	if (recv_bytes == NULL) {
		// If our error was that we've been cancelled, then there
		// is nothing to worry about. That is indeed to be expected.
		if (error->code == G_IO_ERROR_CANCELLED) {
			return;
		}
		// TODO: Signal that the connection seems to have failed.
		g_debug ("Error reading from connection: %s",
			 error->message);
		GCancellable *conn_cancellable =
			ircium_connection_get_conn_cancellable (self->conn);
		if (!g_cancellable_is_cancelled (conn_cancellable)) {
			g_cancellable_cancel (conn_cancellable);
		}
		return;
	}

	// If we have actually gotten new data, append it
	// to the queue of data.
	if (g_bytes_get_size (recv_bytes) > 0) {
		gsize size = 0;
		guint8 *data = g_bytes_unref_to_data (recv_bytes, &size);
		g_byte_array_append (self->pending_data, data, size);
		g_free (data);

		self->has_new_data = TRUE;
	}
}

/**
 * ircium_session_send_message:
 * @session: Our current session
 * @msg: (transfer full): the message to send
 */
void
ircium_session_send_message (IrciumSession *session,
                             IrciumMessage *msg)
{
	g_ptr_array_add (session->send_message_queue, msg);
}

IrciumServerBuffer *
ircium_session_get_server_buffer (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	return session->server_buffer;
}

IrciumBuffer *
ircium_session_find_buffer (IrciumSession *session,
                            const gchar   *name)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	g_return_val_if_fail (name != NULL && *name != '\0', NULL);

	IrciumBuffer *buf = NULL;

	GListModel *buffers = G_LIST_MODEL (session->buffers);
	gsize n_items = g_list_model_get_n_items (buffers);

	for (gsize i = 0; i < n_items; ++i) {
		IrciumBuffer *tmp = g_list_model_get_item (buffers, i);
		if (g_strcmp0 (ircium_buffer_get_title (tmp), name) == 0) {
			buf = tmp;
			break;
		}
	}
	return buf;
}

GListModel *
ircium_session_get_buffer_list (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	return G_LIST_MODEL (session->buffers);
}

const IrciumBuffer *
ircium_session_get_active_buffer (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	return session->active_buffer;
}

void
ircium_session_set_active_buffer (IrciumSession *session,
                                  IrciumBuffer  *buffer)
{
	g_return_if_fail (IRCIUM_IS_SESSION (session));
	// We have to make sure that the buffer is actually
	// one we have.
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));

	IrciumBuffer *newBuffer = NULL;

	for (gsize index = 0; index <
	     g_list_model_get_n_items (G_LIST_MODEL (session->buffers));
	     ++index) {
		gpointer item =
		     g_list_model_get_item (G_LIST_MODEL (session->buffers),
					    index);
		if (item == buffer) {
			newBuffer = item;
			break;
		}
	}

	g_return_if_fail (newBuffer != NULL);

	if (newBuffer == session->active_buffer) return;

	session->active_buffer = newBuffer;
	g_object_notify_by_pspec (G_OBJECT (session),
				  properties[PROP_ACTIVE_BUFFER]);
}

GHashTable *
ircium_session_get_isupport (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	return session->isupport;
}

const gchar *
ircium_session_get_nick (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	return ircium_user_get_nick (session->self_user);
}

void
ircium_session_set_nick (IrciumSession *session,
                         const gchar   *nick)
{
	g_return_if_fail (IRCIUM_IS_SESSION (session));
	g_return_if_fail (nick != NULL);
	ircium_user_set_nick (session->self_user, nick);
}

static gssize
ircium_session_buffer_exists (IrciumSession *session,
                              IrciumBuffer  *buffer)
{
	gssize index = -1;
	for (gsize i = 0;
	     i < g_list_model_get_n_items (G_LIST_MODEL (session->buffers));
	     ++i) {
		IrciumBuffer *buf =
			g_list_model_get_item (G_LIST_MODEL (session->buffers),
					       i);
		if (buf == buffer) {
			index = i;
			break;
		}
	}
	return index;
}

void
ircium_session_add_buffer (IrciumSession *session,
                           IrciumBuffer  *buffer)
{
	g_return_if_fail (IRCIUM_IS_SESSION (session));
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	// We check if it exists.
	if (ircium_session_buffer_exists (session, buffer) != -1) return;
	ircium_buffer_set_session (buffer, session);
	g_list_store_append (session->buffers, buffer);
}

void
ircium_session_remove_buffer (IrciumSession *session,
                              IrciumBuffer  *buffer)
{
	g_return_if_fail (IRCIUM_IS_SESSION (session));
	g_return_if_fail (IRCIUM_IS_BUFFER (buffer));
	gssize i = ircium_session_buffer_exists (session, buffer);
	if (i != -1) {
		g_list_store_remove (session->buffers, i);
	}
}

GListModel *
ircium_session_get_users (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);

	return G_LIST_MODEL (session->user_list);
}

IrciumUser *
ircium_session_get_self (IrciumSession *session)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);

	return session->self_user;
}

IrciumUser *
ircium_session_find_user_nick (IrciumSession *session,
                               const gchar   *nick)
{
	g_return_val_if_fail (IRCIUM_IS_SESSION (session), NULL);
	if (nick == NULL || *nick == '\0') return NULL;

	GListModel *user_list = G_LIST_MODEL (session->user_list);

	gssize user_index = -1;
	for (gsize i = 0; i < g_list_model_get_n_items (user_list); ++i) {
		IrciumUser *tmp = g_list_model_get_item (user_list, i);
		if (g_strcmp0 (nick, ircium_user_get_nick (tmp)) == 0) {
			user_index = i;
			break;
		}
	}
	return user_index != -1 ?
		g_list_model_get_item (user_list, user_index) :
		NULL;
}

void
ircium_session_add_user (IrciumSession *session,
                         IrciumUser    *user)
{
	g_return_if_fail (IRCIUM_IS_SESSION (session));
	g_return_if_fail (IRCIUM_IS_USER (user));

	g_list_store_append (session->user_list, user);
}

void
ircium_session_garbage_collect_users (IrciumSession *session)
{
	g_return_if_fail (IRCIUM_IS_SESSION (session));

	// Find all users that do not have an associated channel user
	// and remove them.
	GListStore *users_store = session->user_list;
	GListModel *users = G_LIST_MODEL (users_store);

	GListModel *buffers = G_LIST_MODEL (session->buffers);

	g_autoptr (GArray) garbage_users =
		g_array_new (FALSE, FALSE, sizeof (gsize));
	for (gsize i = 0; i < g_list_model_get_n_items (users); ++i) {
		IrciumUser *user = g_list_model_get_item (users, i);
		if (user == session->self_user) continue;
		gboolean user_present = FALSE;
		for (gsize j = 0; j < g_list_model_get_n_items (buffers); ++j) {
			IrciumBuffer *buf = g_list_model_get_item (buffers, i);

			if (!IRCIUM_IS_CHANNEL_BUFFER (buf)) continue;

			IrciumChannelBuffer *channel_buf =
				IRCIUM_CHANNEL_BUFFER (buf);

			if (ircium_channel_buffer_find_user (channel_buf, user)
			    != NULL) {
				user_present = TRUE;
				break;
			}
		}

		// We prepend instead of appending because we want to remove
		// users from the list "backwards" in order to avoid having
		// the indices move.
		if (!user_present) g_array_prepend_val (garbage_users, i);
	}
	for (gsize i = 0; i < garbage_users->len; ++i) {
		gsize garbage_index = g_array_index (garbage_users, gsize, i);
		g_list_store_remove (users_store, garbage_index);
	}
}
