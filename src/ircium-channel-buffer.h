/* ircium-channel-buffer.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "ircium-buffer.h"
#include "ircium-channel-user.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_CHANNEL_BUFFER (ircium_channel_buffer_get_type())

G_DECLARE_FINAL_TYPE (IrciumChannelBuffer,
		      ircium_channel_buffer,
		      IRCIUM, CHANNEL_BUFFER,
		      IrciumBuffer)

IrciumChannelBuffer *ircium_channel_buffer_new (gchar *name);

GListModel *ircium_channel_buffer_get_users (IrciumChannelBuffer *buffer);
IrciumChannelUser *ircium_channel_buffer_find_user (IrciumChannelBuffer *buffer,
                                                    IrciumUser          *user);
void ircium_channel_buffer_remove_user (IrciumChannelBuffer *buffer,
                                        IrciumUser          *user);
void ircium_channel_buffer_add_user (IrciumChannelBuffer *buffer,
                                     IrciumUser          *user);

const gchar *ircium_channel_buffer_get_topic_setter (IrciumChannelBuffer *self);
const GDateTime *
ircium_channel_buffer_get_topic_set_time (IrciumChannelBuffer *self);

G_END_DECLS
