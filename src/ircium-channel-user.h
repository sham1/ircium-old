/* ircium-channel-user.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "ircium-user.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_CHANNEL_USER (ircium_channel_user_get_type())

G_DECLARE_FINAL_TYPE (IrciumChannelUser,
		      ircium_channel_user,
		      IRCIUM, CHANNEL_USER,
		      GObject)

IrciumChannelUser *ircium_channel_user_new (IrciumUser *assoc_user);

IrciumUser *ircium_channel_user_get_user (IrciumChannelUser *user);

const gchar *ircium_channel_user_get_nick (IrciumChannelUser *user);

void ircium_channel_user_set_mode (IrciumChannelUser *user,
                                   gchar              mode);
void ircium_channel_user_unset_mode (IrciumChannelUser *user,
                                     gchar              mode);
gboolean ircium_channel_user_has_mode (IrciumChannelUser *user,
                                       gchar              mode);
gchar *ircium_channel_user_get_modes (IrciumChannelUser *user);

G_END_DECLS
