/* ircium-buffer.h
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "ircium-buffer-line.h"

G_BEGIN_DECLS

#define IRCIUM_TYPE_BUFFER (ircium_buffer_get_type())

G_DECLARE_DERIVABLE_TYPE (IrciumBuffer, ircium_buffer, IRCIUM, BUFFER, GObject)

struct _IrciumBufferClass
{
	GObjectClass parent_class;

	GListModel *(*get_contents) (IrciumBuffer *buffer);
	gchar *(*get_title) (IrciumBuffer *buffer);
	void (*add_line) (IrciumBuffer     *buffer,
	                  IrciumBufferLine *line);
	void (*handle_entry) (IrciumBuffer *buffer,
	                      gchar        *entry_text);
	void (*set_session) (IrciumBuffer *buffer,
	                     gpointer      session);
	gboolean (*has_topic) (IrciumBuffer *buffer);
	gchar *(*get_topic) (IrciumBuffer *buffer);
	void (*set_topic) (IrciumBuffer *buffer,
	                   gchar        *topic);
	void (*set_topic_metadata) (IrciumBuffer *buffer,
	                            const gchar  *nick,
	                            GDateTime    *time);
};

GListModel *ircium_buffer_get_contents (IrciumBuffer *buffer);
gchar *ircium_buffer_get_title (IrciumBuffer *buffer);
void ircium_buffer_add_line (IrciumBuffer     *buffer,
                             IrciumBufferLine *line);
void ircium_buffer_handle_entry (IrciumBuffer *buffer,
                                 gchar        *entry_text);
gboolean ircium_buffer_has_topic (IrciumBuffer *buffer);
gchar *ircium_buffer_get_topic (IrciumBuffer *buffer);
void ircium_buffer_set_topic (IrciumBuffer *buffer,
                              gchar        *topic);
void ircium_buffer_set_topic_metadata (IrciumBuffer *buffer,
                                       const gchar  *nick,
                                       GDateTime    *time);

G_END_DECLS
