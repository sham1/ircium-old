/* ircium-connection.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-connection-private.h"
#include "ircium-session-private.h"
#include <libsecret/secret.h>

static const SecretSchema *get_server_pass_schema (void) G_GNUC_CONST;
#define CONNECTION_SERVER_PASS_SCHEMA (get_server_pass_schema ())

struct _IrciumConnectionAuthInfo
{
	gboolean has_pass;

	gchar *pass;
};

typedef struct _IrciumConnectionUserAuthInfo {
	IrciumConnectionUserAuthType type;

	union {
		struct {
			gchar *user;
			gchar *pass;
		} sasl;

		struct {
			gchar *user;
			gchar *pass;
		} nickserv;
	};
} IrciumConnectionUserAuthInfo;

struct _IrciumConnection
{
	GObject parent_instance;

	gchar *network_name;
	gchar *hostname;

	// This will be FALSE
	// when the port is invalid.
	gboolean port_set;
	guint16 port;

	gboolean tls;

	gchar *username;
	gchar *nickname;
	gchar *realname;

	IrciumConnectionAuthInfo auth_info;
	IrciumConnectionUserAuthInfo user_auth_info;

	GIOStream *curr_stream;

	GCancellable *conn_cancellable;

	gboolean minimum_viable;

	AcceptCertCB accept_cert_handler;
	gpointer accept_cert_user_data;
	GDestroyNotify accept_cert_user_data_free;

	GSettings *account_settings;

	gchar *own_uuid;
};

G_DEFINE_TYPE (IrciumConnection,
	       ircium_connection,
	       G_TYPE_OBJECT)

enum {
	PROP_0,

	PROP_NAME,
	PROP_HOSTNAME,
	PROP_PORT,
	PROP_TLS,

	PROP_USERNAME,
	PROP_NICKNAME,
	PROP_REALNAME,

	PROP_MINIMUM,

	PROP_SETTINGS,

	PROP_PASS,

	N_PROPS
};

static GParamSpec *properties [N_PROPS];

IrciumConnection *
ircium_connection_new (void)
{
	return g_object_new (IRCIUM_TYPE_CONNECTION, NULL);
}

IrciumConnection *
ircium_connection_new_from_settings (GSettings *account)
{
	return g_object_new (IRCIUM_TYPE_CONNECTION,
			     "settings", account,
			     NULL);
}

static void
ircium_connection_dispose (GObject *object)
{
	IrciumConnection *self = (IrciumConnection *) object;

	g_clear_object (&self->curr_stream);

	if (self->accept_cert_user_data_free != NULL) {
		g_clear_pointer (&self->accept_cert_user_data,
				 self->accept_cert_user_data_free);
		self->accept_cert_user_data_free = NULL;
	}

	g_clear_object (&self->conn_cancellable);

	g_clear_object (&self->account_settings);

	G_OBJECT_CLASS (ircium_connection_parent_class)->dispose (object);
}

static void
ircium_connection_bind_settings (IrciumConnection *self);

static void
check_minimum_viable (IrciumConnection *conn);

static void
ircium_connection_auth_info_free (IrciumConnectionAuthInfo *info);

static void
ircium_connection_user_auth_info_free (IrciumConnectionUserAuthInfo *info);

static void
ircium_connection_from_settings (IrciumConnection *self);

static void
ircium_connection_finalize (GObject *object)
{
	IrciumConnection *self = (IrciumConnection *) object;

	ircium_connection_auth_info_free (&self->auth_info);
	ircium_connection_user_auth_info_free (&self->user_auth_info);

	g_free (self->network_name);
	g_free (self->hostname);
	g_free (self->username);
	g_free (self->nickname);
	g_free (self->realname);
	g_free (self->own_uuid);

	G_OBJECT_CLASS (ircium_connection_parent_class)->finalize (object);
}

static void
ircium_connection_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
	IrciumConnection *self = IRCIUM_CONNECTION (object);

	switch (prop_id)
	{
	case PROP_NAME:
		g_value_set_string (value, self->network_name);
		break;
	case PROP_HOSTNAME:
		g_value_set_string (value, self->hostname);
		break;
	case PROP_PORT:
		g_value_set_uint (value, self->port);
		break;
	case PROP_TLS:
		g_value_set_boolean (value, self->tls);
		break;
	case PROP_USERNAME:
		g_value_set_string (value, self->username);
		break;
	case PROP_NICKNAME:
		g_value_set_string (value, self->nickname);
		break;
	case PROP_REALNAME:
		g_value_set_string (value, self->realname);
		break;
	case PROP_MINIMUM:
		g_value_set_boolean (value, self->minimum_viable);
		break;
	case PROP_SETTINGS:
		g_value_set_object (value, self->account_settings);
		break;
	case PROP_PASS:
		if (self->auth_info.has_pass) {
			g_value_set_string (value, self->auth_info.pass);
		} else {
			g_value_set_static_string (value, NULL);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
ircium_connection_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
	IrciumConnection *self = IRCIUM_CONNECTION (object);

	switch (prop_id)
	{
	case PROP_NAME:
		g_clear_pointer (&self->network_name, g_free);
		self->network_name = g_value_dup_string (value);
		break;
	case PROP_HOSTNAME:
		g_clear_pointer (&self->hostname, g_free);
		self->hostname = g_value_dup_string (value);
		break;
	case PROP_PORT:
		self->port = g_value_get_uint (value);
		break;
	case PROP_TLS:
		self->tls = g_value_get_boolean (value);
		break;
	case PROP_USERNAME:
		g_clear_pointer (&self->username, g_free);
		self->username = g_value_dup_string (value);
		break;
	case PROP_NICKNAME:
		g_clear_pointer (&self->nickname, g_free);
		self->nickname = g_value_dup_string (value);
		break;
	case PROP_REALNAME:
		g_clear_pointer (&self->realname, g_free);
		self->realname = g_value_dup_string (value);
		break;
	case PROP_SETTINGS:
		g_clear_pointer (&self->account_settings, g_free);
		self->account_settings = g_value_dup_object (value);
		ircium_connection_from_settings (self);
		break;
	case PROP_PASS:
		if (self->auth_info.has_pass) {
			g_clear_pointer (&self->auth_info.pass, g_free);
		}
		self->auth_info.has_pass = TRUE;
		self->auth_info.pass = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
	check_minimum_viable (self);
}

static void
ircium_connection_class_init (IrciumConnectionClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = ircium_connection_dispose;
	object_class->finalize = ircium_connection_finalize;
	object_class->get_property = ircium_connection_get_property;
	object_class->set_property = ircium_connection_set_property;

	properties[PROP_NAME] =
		g_param_spec_string ("name",
				     "name",
				     "Network name",
				     NULL,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT);
	properties[PROP_HOSTNAME] =
		g_param_spec_string ("hostname",
				     "hostname",
				     "Network hostname",
				     NULL,
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT);
	properties[PROP_PORT] =
		g_param_spec_uint ("port",
				   "port",
				   "Network port",
				   0,
				   65536,
				   0,
				   G_PARAM_READWRITE |
				   G_PARAM_CONSTRUCT);
	properties[PROP_TLS] =
		g_param_spec_boolean ("tls",
				      "tls",
				      "Network's TLS setting",
				      FALSE,
				      G_PARAM_READWRITE |
				      G_PARAM_CONSTRUCT);
	properties[PROP_USERNAME] =
		g_param_spec_string ("username",
				     "username",
				     "The username",
				     NULL,
				     G_PARAM_READWRITE
				     | G_PARAM_CONSTRUCT);
	properties[PROP_NICKNAME] =
		g_param_spec_string ("nickname",
				     "nickname",
				     "The nickname",
				     NULL,
				     G_PARAM_READWRITE
				     | G_PARAM_CONSTRUCT);
	properties[PROP_REALNAME] =
		g_param_spec_string ("realname",
				     "realname",
				     "The user's real name",
				     NULL,
				     G_PARAM_READWRITE
				     | G_PARAM_CONSTRUCT);

	properties[PROP_MINIMUM] =
		g_param_spec_boolean
			("minimum",
			 "minimum",
			 "Can the connection work in current state",
			 FALSE,
			 G_PARAM_READABLE);

	properties[PROP_SETTINGS] =
		g_param_spec_object ("settings",
				     "settings",
				     "The settings of connection",
				     G_TYPE_SETTINGS,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

	properties[PROP_PASS] =
		g_param_spec_string ("pass",
				     "pass",
				     "The password of the connection",
				     NULL,
				     G_PARAM_READWRITE);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
ircium_connection_init (IrciumConnection *self)
{
}

static void
ircium_connection_auth_info_free (IrciumConnectionAuthInfo *info)
{
	g_clear_pointer (&info->pass, g_free);
}

static void ircium_connection_connect_cb (GObject      *object,
                                          GAsyncResult *res,
                                          gpointer      user_data);
static void connection_event_cb (GSocketClient      *client,
                                 GSocketClientEvent  event,
                                 GSocketConnectable *connectable,
                                 GIOStream          *connection,
                                 gpointer            user_data);
/**
 * ircium_connection_connect_async:
 *
 *
 *
 */
void
ircium_connection_connect_async (IrciumConnection    *self,
                                 GCancellable        *conn_cancellable,
                                 GCancellable        *cancellable,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
	g_assert (IRCIUM_IS_CONNECTION (self));

	g_autoptr (GTask) task = NULL;
	g_assert (IRCIUM_IS_CONNECTION (self));
	g_assert (conn_cancellable != NULL &&
		  G_IS_CANCELLABLE (conn_cancellable));
	g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

	self->conn_cancellable = g_object_ref (conn_cancellable);

	task = g_task_new (self, cancellable, callback, user_data);
	g_task_set_priority (task, G_PRIORITY_LOW);
	g_task_set_source_tag (task, ircium_connection_connect_async);
	g_task_set_task_data (task, g_object_ref (self), g_object_unref);

	GSocketConnectable *connectable =
		g_network_address_new (self->hostname, self->port);
	GSocketClient *client_socket = g_socket_client_new ();
	g_signal_connect (client_socket,
			  "event",
			  G_CALLBACK (connection_event_cb),
			  self);
	g_socket_client_set_tls (client_socket, self->tls);
	g_socket_client_connect_async (client_socket, connectable,
				       cancellable,
				       ircium_connection_connect_cb,
				       g_steal_pointer (&task));
}

IrciumSession *
ircium_connection_connect_finish (IrciumConnection  *conn,
                                  GAsyncResult      *result,
                                  GError           **error)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return g_task_propagate_pointer (G_TASK (result), error);
}

static void
ircium_connection_connect_cb (GObject      *object,
                              GAsyncResult *result,
                              gpointer      user_data)
{
	GSocketClient *sock_client = (GSocketClient *) object;
	g_autoptr (GTask) task = user_data;

	g_assert (G_IS_SOCKET_CLIENT (sock_client));
	g_assert (G_IS_ASYNC_RESULT (result));
	g_assert (G_IS_TASK (task));

	gboolean is_tls = g_socket_client_get_tls (sock_client);

	IrciumConnection *self = g_task_get_task_data (task);

	g_autoptr (GError) error = NULL;

	GSocketConnection *connection =
		g_socket_client_connect_finish (sock_client, result, &error);
	if (connection == NULL) {
		g_task_return_error (task, g_steal_pointer (&error));
		return;
	}

	if (is_tls) {
		GTcpWrapperConnection *wrap_conn =
			G_TCP_WRAPPER_CONNECTION (connection);
		self->curr_stream =
			g_object_ref (
				g_tcp_wrapper_connection_get_base_io_stream
				      (wrap_conn));
	} else {
		self->curr_stream = G_IO_STREAM (g_object_ref (connection));
	}
	g_object_unref (connection);

	g_task_return_pointer (task, ircium_session_new (self), g_object_unref);
}

static void
connection_event_cb (GSocketClient      *client,
                     GSocketClientEvent  event,
                     GSocketConnectable *connectable,
                     GIOStream          *connection,
                     gpointer            user_data)
{
	IrciumConnection *self = user_data;
	if (event == G_SOCKET_CLIENT_TLS_HANDSHAKING) {
		GTlsClientConnection *conn =
			(GTlsClientConnection *) connection;
		g_signal_connect (conn,
				  "accept-certificate",
				  G_CALLBACK (self->accept_cert_handler),
				  self->accept_cert_user_data);
	}
}

IrciumConnectionUserAuthType
ircium_connection_get_user_auth_type (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->user_auth_info.type;
}

gboolean
ircium_connection_get_sasl_details (IrciumConnection  *conn,
                                    gchar            **user,
                                    gchar            **pass)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	g_return_val_if_fail (conn->user_auth_info.type ==
			      IRCIUM_CONNECTION_USER_AUTH_TYPE_SASL,
			      FALSE);

	*user = conn->user_auth_info.sasl.user;
	*pass = conn->user_auth_info.sasl.pass;

	return TRUE;
}
gboolean
ircium_connection_get_nickserv_details (IrciumConnection  *conn,
                                        gchar            **user,
                                        gchar            **pass)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	g_return_val_if_fail (conn->user_auth_info.type ==
			      IRCIUM_CONNECTION_USER_AUTH_TYPE_NICKSERV,
			      FALSE);

	*user = conn->user_auth_info.nickserv.user;
	*pass = conn->user_auth_info.nickserv.pass;

	return TRUE;
}

void
ircium_connection_remove_user_auth (IrciumConnection *conn)
{
	ircium_connection_user_auth_info_free (&conn->user_auth_info);
	conn->user_auth_info.type = IRCIUM_CONNECTION_USER_AUTH_TYPE_NONE;
}
void
ircium_connection_make_user_auth_sasl (IrciumConnection *conn,
                                       gchar            *user,
                                       gchar            *pass)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	ircium_connection_user_auth_info_free (&conn->user_auth_info);
	conn->user_auth_info.type = IRCIUM_CONNECTION_USER_AUTH_TYPE_SASL;
	conn->user_auth_info.sasl.user = g_strdup (user);
	conn->user_auth_info.sasl.pass = g_strdup (pass);
}
void
ircium_connection_make_user_auth_nickserv (IrciumConnection *conn,
                                           gchar            *user,
                                           gchar            *pass)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	ircium_connection_user_auth_info_free (&conn->user_auth_info);
	conn->user_auth_info.type = IRCIUM_CONNECTION_USER_AUTH_TYPE_NICKSERV;
	conn->user_auth_info.nickserv.user = g_strdup (user);
	conn->user_auth_info.nickserv.pass = g_strdup (pass);
}

static void
ircium_connection_user_auth_info_free (IrciumConnectionUserAuthInfo *info)
{
	switch (info->type) {
	case IRCIUM_CONNECTION_USER_AUTH_TYPE_SASL:
		g_clear_pointer (&info->sasl.user, g_free);
		g_clear_pointer (&info->sasl.pass, g_free);
		break;
	case IRCIUM_CONNECTION_USER_AUTH_TYPE_NICKSERV:
		g_clear_pointer (&info->nickserv.user, g_free);
		g_clear_pointer (&info->nickserv.pass, g_free);
		break;
	default:
		break;
	}
}

static void check_minimum_viable (IrciumConnection *conn);

gboolean
ircium_connection_get_server_password (IrciumConnection  *conn,
                                       gchar            **pass)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	if (conn->auth_info.has_pass == FALSE) {
		return FALSE;
	}
	*pass = conn->auth_info.pass;
	return TRUE;
}

static void
on_password_stored (GObject      *obj,
                    GAsyncResult *result,
                    gpointer      user_data)
{
	g_autoptr (GError) error = NULL;

	secret_password_store_finish (result, &error);
	if (error != NULL) {
		g_debug ("An error occured while saving password: %s",
			 error->message);
	}
}

void
store_pass (gchar       *label,
            const gchar *pass,
            gchar       *uuid)
{
	secret_password_store (CONNECTION_SERVER_PASS_SCHEMA,
			       SECRET_COLLECTION_DEFAULT, label,
			       pass,
			       NULL, on_password_stored, NULL,
			      "uuid", uuid,
			       NULL);
}

void
ircium_connection_set_server_password (IrciumConnection *conn,
                                       const gchar      *pass)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	ircium_connection_auth_info_free (&conn->auth_info);
	conn->auth_info.pass = g_strdup (pass);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_PASS]);

	if (conn->own_uuid != NULL) {
		store_pass (conn->network_name,
			    conn->auth_info.pass,
			    conn->own_uuid);
	}
}

void
ircium_connection_set_server_has_password (IrciumConnection *conn)
{
	g_return_if_fail (IRCIUM_IS_CONNECTION (conn));
	conn->auth_info.has_pass = TRUE;
}

static void
on_connection_server_pass_remove (GObject      *obj,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
	g_autoptr (GError) error = NULL;

	secret_password_clear_finish (result, &error);
	if (error != NULL) {
		g_debug ("An error occured while removing password: %s",
			 error->message);
	}
}

void
ircium_connection_unset_server_has_password (IrciumConnection *conn)
{
	g_return_if_fail (IRCIUM_IS_CONNECTION (conn));
	conn->auth_info.has_pass = FALSE;

	g_clear_pointer (&conn->auth_info.pass, g_free);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_PASS]);

	secret_password_clear (CONNECTION_SERVER_PASS_SCHEMA, NULL,
			       on_connection_server_pass_remove, NULL,
			       "uuid", conn->own_uuid,
			       NULL);
}

void
ircium_connection_set_port (IrciumConnection *conn,
                            guint16           port)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	conn->port = port;
	conn->port_set = TRUE;
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_PORT]);

	check_minimum_viable (conn);
}

void
ircium_connection_unset_port (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	conn->port_set = FALSE;

	check_minimum_viable (conn);
}

gboolean
ircium_connection_is_port_set (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->port_set;
}

guint16
ircium_connection_get_port (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->port;
}

void
ircium_connection_set_name  (IrciumConnection *conn,
                             const gchar      *name)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	if (conn->network_name != NULL) g_free (conn->network_name);
	conn->network_name = g_strdup (name);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_NAME]);

	check_minimum_viable (conn);
}

const gchar *
ircium_connection_get_name (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->network_name;
}

void
ircium_connection_set_address  (IrciumConnection *conn,
                                const gchar      *address)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	if (conn->hostname != NULL) g_free (conn->hostname);
	conn->hostname = g_strdup (address);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_HOSTNAME]);

	check_minimum_viable (conn);
}

const gchar *
ircium_connection_get_address (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->hostname;
}

void
ircium_connection_set_nickname  (IrciumConnection *conn,
                                 const gchar      *nickname)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	if (conn->nickname != NULL) g_free (conn->nickname);
	conn->nickname = g_strdup (nickname);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_NICKNAME]);

	check_minimum_viable (conn);
}
const gchar *
ircium_connection_get_nickname (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->nickname;
}

void
ircium_connection_set_username  (IrciumConnection *conn,
                                 const gchar      *username)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	if (conn->username != NULL) g_free (conn->username);
	conn->username = g_strdup (username);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_USERNAME]);
}
const gchar *
ircium_connection_get_username (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->username;
}

void
ircium_connection_set_realname  (IrciumConnection *conn,
                                 const gchar      *realname)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	if (conn->realname != NULL) g_free (conn->realname);
	conn->realname = g_strdup (realname);
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_REALNAME]);
}
const gchar *
ircium_connection_get_realname (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->realname;
}

void
ircium_connection_set_tls (IrciumConnection *conn,
                           gboolean          tls)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	conn->tls = tls;
	g_object_notify_by_pspec (G_OBJECT (conn), properties[PROP_TLS]);
}
gboolean
ircium_connection_get_tls (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->tls;
}

gboolean
ircium_connection_is_minimum_viable (IrciumConnection *conn)
{
	g_assert (IRCIUM_IS_CONNECTION (conn));

	return conn->minimum_viable;
}

static void
check_minimum_viable (IrciumConnection *conn)
{
	gboolean new_check =
		conn->network_name != NULL &&
		conn->hostname != NULL &&
		conn->port_set && conn->port != 0 &&
		conn->nickname != NULL;

	if (conn->minimum_viable != new_check) {
		conn->minimum_viable = new_check;
		g_object_notify_by_pspec (G_OBJECT (conn),
					  properties[PROP_MINIMUM]);
	}
}

GIOStream *
ircium_connection_get_current_stream (IrciumConnection *conn)
{
	g_return_val_if_fail (IRCIUM_IS_CONNECTION (conn), NULL);
	return conn->curr_stream;
}

GCancellable *
ircium_connection_get_conn_cancellable (IrciumConnection *conn)
{
	g_return_val_if_fail (IRCIUM_IS_CONNECTION (conn), NULL);
	return conn->conn_cancellable;
}

void
ircium_connection_set_accept_cert_handler
	(IrciumConnection *conn,
	 AcceptCertCB      handler,
	 gpointer          user_data,
	 GDestroyNotify    user_data_free)
{
	g_return_if_fail (IRCIUM_IS_CONNECTION (conn));
	g_return_if_fail (handler != NULL);
	conn->accept_cert_handler = handler;
	conn->accept_cert_user_data = user_data;
	conn->accept_cert_user_data_free = user_data_free;
}

static GVariant *
tls_to_enum (const GValue       *value,
             const GVariantType *expected_type,
             gpointer            user_data)
{
	// TODO: Add support for STARTTLS
	gboolean is_tls = g_value_get_boolean (value);
	return g_variant_new_string (is_tls ? "TLS" : "NoTLS");
}

static gboolean
enum_to_tls (GValue   *value,
             GVariant *variant,
             gpointer  user_data)
{
	const gchar *tls_str;

	g_variant_get (variant, "&s", &tls_str);
	if (tls_str == NULL || *tls_str == '\0') return FALSE;

	if (g_strcmp0 (tls_str, "NoTLS") == 0) {
		g_value_set_boolean (value, FALSE);
		return TRUE;
	}

	if (g_strcmp0 (tls_str, "StartTLS") == 0) {
		// TODO: Add STARTTLS support
		return TRUE;
	}

	if (g_strcmp0 (tls_str, "TLS") == 0) {
		g_value_set_boolean (value, TRUE);
		return TRUE;
	}

	return FALSE;
}

static gboolean
ms_to_string (GValue   *value,
              GVariant *variant,
              gpointer  user_data)
{
	const gchar *str;
	GVariant *maybe = g_variant_get_maybe (variant);
	if (maybe == NULL) {
		str = NULL;
	} else {
		g_variant_get (maybe, "&s", &str);
	}

	g_value_set_string (value, str);
	if (maybe != NULL) g_variant_unref (maybe);
	return TRUE;
}

static GVariant *
string_to_ms (const GValue       *value,
              const GVariantType *expected_type,
              gpointer            user_data)
{
	const gchar *str = g_value_get_string (value);
	return g_variant_new ("ms", str);
}

static void
ircium_connection_bind_settings (IrciumConnection *self)
{
	if (self->account_settings == NULL) return;

	GSettings *s = self->account_settings;

	g_settings_bind (s, "visual-name",
			 self, "name",
			 G_SETTINGS_BIND_DEFAULT);

	g_settings_bind (s, "server-address",
			 self, "hostname",
			 G_SETTINGS_BIND_DEFAULT);

	g_settings_bind (s, "server-port",
			 self, "port",
			 G_SETTINGS_BIND_DEFAULT);

	g_settings_bind_with_mapping (s, "server-tls",
				      self, "tls",
				      G_SETTINGS_BIND_DEFAULT,
				      enum_to_tls, tls_to_enum,
				      NULL, NULL);

	g_settings_bind (s, "nickname",
			 self, "nickname",
			 G_SETTINGS_BIND_DEFAULT);

	g_settings_bind_with_mapping (s, "username",
				      self, "username",
				      G_SETTINGS_BIND_DEFAULT,
				      ms_to_string, string_to_ms,
				      NULL, NULL);

	g_settings_bind_with_mapping (s, "realname",
				      self, "realname",
				      G_SETTINGS_BIND_DEFAULT,
				      ms_to_string, string_to_ms,
				      NULL, NULL);
}

GSettings *
ircium_connection_get_settings (IrciumConnection *conn)
{
	g_return_val_if_fail (IRCIUM_IS_CONNECTION (conn), NULL);
	return conn->account_settings;
}

static void
set_setting_props (IrciumConnection *conn);

void
ircium_connection_set_settings (IrciumConnection *conn,
                                GSettings        *account)
{
	g_return_if_fail (IRCIUM_IS_CONNECTION (conn));
	g_return_if_fail (account != NULL);
	g_clear_object (&conn->account_settings);
	conn->account_settings = g_object_ref (account);

	set_setting_props (conn);
	conn->own_uuid = g_object_steal_data (G_OBJECT (account), "uuid");

	g_settings_set (account, "uuid", "ms", conn->own_uuid);

	ircium_connection_bind_settings (conn);

	if (conn->auth_info.has_pass) {
		store_pass (conn->network_name,
			    conn->auth_info.pass,
			    conn->own_uuid);
	}
}

static void
set_setting_props (IrciumConnection *conn)
{
	if (conn->account_settings == NULL) return;

	GSettings *s = conn->account_settings;

	g_settings_set_string (s, "visual-name", conn->network_name);

	g_settings_set_string (s, "server-address", conn->hostname);

	g_settings_set (s, "server-port", "q", conn->port);

	g_settings_set_enum (s, "server-tls", conn->tls ? 2 : 0);

	g_settings_set_string (s, "nickname", conn->nickname);

	g_settings_set (s, "username", "ms", conn->username);

	g_settings_set (s, "realname", "ms", conn->realname);

	g_settings_set (s, "uuid", "ms", conn->own_uuid);
}

static void
on_lookup_password_db (GObject      *source,
                       GAsyncResult *result,
                       gpointer      user_data)
{
	IrciumConnection *conn = user_data;
	g_autoptr (GError) error = NULL;

	gchar *pass = secret_password_lookup_finish (result, &error);
	if (error != NULL) {
		g_debug ("An error occured while looking up password: %s",
			 error->message);
	} else if (pass == NULL) {
		return;
	} else {
		if (conn->auth_info.has_pass) {
			g_clear_pointer (&conn->auth_info.pass, g_free);
		}

		conn->auth_info.has_pass = TRUE;
		conn->auth_info.pass = g_strdup (pass);
		g_object_notify_by_pspec (G_OBJECT (conn),
					  properties[PROP_PASS]);

		secret_password_free (pass);
	}
}

static void
lookup_pass (IrciumConnection *conn)
{
	secret_password_lookup (CONNECTION_SERVER_PASS_SCHEMA,
				NULL, on_lookup_password_db, conn,
				"uuid", conn->own_uuid,
				NULL);
}

static void
ircium_connection_from_settings (IrciumConnection *self)
{
	if (self->account_settings == NULL) return;
	ircium_connection_bind_settings (self);

	gchar *uuid = NULL;
	g_settings_get (self->account_settings, "uuid", "ms", &uuid);
	self->own_uuid = g_strdup (uuid);
	lookup_pass (self);
}

static const SecretSchema *
get_server_pass_schema (void)
{
	static const SecretSchema pass_schema = {
		"com.gitlab.sham1.Ircium.ServerPassword", SECRET_SCHEMA_NONE,
		{
			{ "uuid", SECRET_SCHEMA_ATTRIBUTE_STRING },
			{ NULL, 0 },
		}
	};
	return &pass_schema;
}
