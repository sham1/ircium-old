/* ircium-server-buffer.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ircium-server-buffer.h"
#include "ircium-buffer-private.h"
#include <glib/gi18n.h>

struct _IrciumServerBuffer
{
	IrciumBuffer parent_instance;

	GListStore *server_store;
	IrciumSession *session;
};

G_DEFINE_TYPE (IrciumServerBuffer, ircium_server_buffer, IRCIUM_TYPE_BUFFER)

IrciumServerBuffer *
ircium_server_buffer_new (void)
{
	return g_object_new (IRCIUM_TYPE_SERVER_BUFFER, NULL);
}

static GListModel *ircium_server_buffer_get_contents (IrciumBuffer *buffer);
static gchar *ircium_server_buffer_get_title (IrciumBuffer *buffer);
static void ircium_server_buffer_add_line (IrciumBuffer     *buffer,
                                           IrciumBufferLine *line);
static void ircium_server_buffer_handle_entry (IrciumBuffer *buffer,
                                               gchar        *entered_text);
static void ircium_server_buffer_set_session (IrciumBuffer *buffer,
                                              gpointer      session);
static gboolean ircium_server_buffer_has_topic (IrciumBuffer *buffer);
static gchar *ircium_server_buffer_get_topic (IrciumBuffer *buffer);
static void ircium_server_buffer_set_topic (IrciumBuffer *buffer,
                                            gchar        *topic);
static void ircium_server_buffer_set_topic_metadata (IrciumBuffer *buffer,
                                                     const gchar  *nick,
                                                     GDateTime    *time);

static void
ircium_server_buffer_class_init (IrciumServerBufferClass *klass)
{
	IrciumBufferClass *buffer_class = (IrciumBufferClass *) klass;

	buffer_class->get_contents = ircium_server_buffer_get_contents;
	buffer_class->get_title = ircium_server_buffer_get_title;
	buffer_class->add_line = ircium_server_buffer_add_line;
	buffer_class->handle_entry = ircium_server_buffer_handle_entry;
	buffer_class->set_session = ircium_server_buffer_set_session;
	buffer_class->has_topic = ircium_server_buffer_has_topic;
	buffer_class->get_topic = ircium_server_buffer_get_topic;
	buffer_class->set_topic = ircium_server_buffer_set_topic;
	buffer_class->set_topic_metadata =
		ircium_server_buffer_set_topic_metadata;
}

static void
ircium_server_buffer_init (IrciumServerBuffer *self)
{
	self->server_store = g_list_store_new (IRCIUM_TYPE_BUFFER_LINE);
}

static GListModel *
ircium_server_buffer_get_contents (IrciumBuffer *buffer)
{
	IrciumServerBuffer *self = (IrciumServerBuffer *) buffer;
	g_return_val_if_fail (IRCIUM_IS_SERVER_BUFFER (self), NULL);
	return G_LIST_MODEL (self->server_store);
}

static gchar *
ircium_server_buffer_get_title (IrciumBuffer *buffer)
{
	IrciumServerBuffer *self = (IrciumServerBuffer *) buffer;
	g_return_val_if_fail (IRCIUM_IS_SERVER_BUFFER (self), NULL);
	return _("Server");
}

static void
ircium_server_buffer_add_line (IrciumBuffer     *buffer,
                               IrciumBufferLine *line)
{
	IrciumServerBuffer *self = (IrciumServerBuffer *) buffer;
	g_return_if_fail (IRCIUM_IS_SERVER_BUFFER (self));
	g_list_store_append (self->server_store, line);
}

static void
ircium_server_buffer_handle_entry (IrciumBuffer *buffer,
                                   gchar        *entered_text)
{
	IrciumServerBuffer *self = (IrciumServerBuffer *) buffer;
	g_return_if_fail (IRCIUM_IS_SERVER_BUFFER (self));
	if (ircium_buffer_is_input_line_command (entered_text)) {
		gchar **cmd =
			(gchar **) ircium_buffer_parse_input_line_command
				(entered_text);
		gboolean was_common =
			ircium_buffer_common_command_executor
				((const gchar **) cmd, self->session);
		if (!was_common) {
			// TODO: Deal with a server-buffer specific command,
			// if such a thing exists.
		}
		g_strfreev (cmd);
	} else {
		// TODO: Show user a hint about joining a channel
		// to send messages.
	}
}

static void
ircium_server_buffer_set_session (IrciumBuffer *buffer,
                                  gpointer      session)
{
	IrciumServerBuffer *self = (IrciumServerBuffer *) buffer;
	g_return_if_fail (IRCIUM_IS_SERVER_BUFFER (self));
	self->session = session;
}

static gboolean
ircium_server_buffer_has_topic (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_SERVER_BUFFER (buffer), FALSE);
	return FALSE;
}

static gchar *
ircium_server_buffer_get_topic (IrciumBuffer *buffer)
{
	g_return_val_if_fail (IRCIUM_IS_SERVER_BUFFER (buffer), NULL);
	return NULL;
}

static void
ircium_server_buffer_set_topic (IrciumBuffer *buffer,
                                 gchar        *topic)
{
	g_return_if_fail (IRCIUM_IS_SERVER_BUFFER (buffer));
}

static void ircium_server_buffer_set_topic_metadata (IrciumBuffer *buffer,
                                                     const gchar  *nick,
                                                     GDateTime    *time)
{
	g_return_if_fail (IRCIUM_IS_SERVER_BUFFER (buffer));
}
