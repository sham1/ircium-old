/* test-parser.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>
#include <locale.h>
#include <stdio.h>

#include "ircium-message.h"

typedef struct {
} EmptyFixture;

static void
empty_fixture_set_up (EmptyFixture  *fixture,
                      gconstpointer  user_data)
{}

static void
empty_fixture_tear_down (EmptyFixture  *fixture,
                         gconstpointer  user_data)
{}

static void
message_parse_simple (EmptyFixture  *fixture,
                      gconstpointer  user_data)
{
	const gchar input[] = "@tag1=value1;tag2;vendor1/tag3=value2;"
			     "vendor2/tag4 :irc.example.com "
			     "COMMAND param1 param2 :param3 param3\r\n";
	GBytes *bytes = g_bytes_new_static (input,
					    sizeof(input)/sizeof(*input));
	GByteArray *array = g_bytes_unref_to_array (bytes);

	IrciumMessage *msg = ircium_message_parse (array, TRUE);
	// First we'll check the tags.
	const GPtrArray *tags = ircium_message_get_tags (msg);
	{
		g_assert_cmpuint (4, ==, tags->len);
		IrciumMessageTag *tag1 = tags->pdata[0];
		g_assert_cmpstr ("tag1", ==,
				 ircium_message_tag_get_name (tag1));
		g_assert_true (ircium_message_tag_has_value (tag1));
		g_assert_cmpstr ("value1", ==,
				 ircium_message_tag_get_value (tag1));

		IrciumMessageTag *tag2 = tags->pdata[1];
		g_assert_cmpstr ("tag2", ==,
				 ircium_message_tag_get_name (tag2));
		g_assert_false (ircium_message_tag_has_value (tag2));

		IrciumMessageTag *tag3 = tags->pdata[2];
		g_assert_cmpstr ("vendor1/tag3", ==,
				 ircium_message_tag_get_name (tag3));
		g_assert_true (ircium_message_tag_has_value (tag3));
		g_assert_cmpstr ("value2", ==,
				 ircium_message_tag_get_value (tag3));

		IrciumMessageTag *tag4 = tags->pdata[3];
		g_assert_cmpstr ("vendor2/tag4", ==,
				 ircium_message_tag_get_name (tag4));
		g_assert_false (ircium_message_tag_has_value (tag4));
	}

	// Now the source
	g_assert_cmpstr (ircium_message_get_source (msg), ==,
			 "irc.example.com");
	// The command
	g_assert_cmpstr (ircium_message_get_command (msg), ==, "COMMAND");

	// And the parameters
	const GPtrArray *params = ircium_message_get_params (msg);
	{
		g_assert_cmpuint (3, ==, params->len);
		gchar *param1 = params->pdata[0];
		g_assert_cmpstr ("param1", ==, param1);
		gchar *param2 = params->pdata[1];
		g_assert_cmpstr ("param2", ==, param2);
		gchar *param3 = params->pdata[2];
		g_assert_cmpstr ("param3 param3", ==, param3);
	}

	g_object_unref (msg);
}

static void
message_parse_empty_trailing (EmptyFixture  *fixture,
                              gconstpointer  user_data)
{
	const gchar input[] = "COMMAND  :\r\n";
	GBytes *bytes = g_bytes_new_static (input,
					    (sizeof(input)/sizeof(*input)) - 1);
	GByteArray *array = g_bytes_unref_to_array (bytes);
	IrciumMessage *msg = ircium_message_parse (array, TRUE);

	g_assert_null (ircium_message_get_tags (msg));
	g_assert_null (ircium_message_get_source (msg));
	g_assert_cmpstr (ircium_message_get_command (msg), ==, "COMMAND");

	g_assert_nonnull (ircium_message_get_params (msg));
	g_assert_cmpuint (ircium_message_get_params (msg)->len, ==, 1);
	g_assert_cmpstr (ircium_message_get_params (msg)->pdata[0], ==, "");

	g_object_unref (msg);
}

static void
message_serialize_identity (EmptyFixture  *fixture,
                            gconstpointer  user_data)
{
	// Same test case as above, but now we want to see if we get
	// the same thing back if we serialize the result of our
	// deserialization.
	const gchar input[] = "@tag1=value1;tag2;vendor1/tag3=value2;"
			     "vendor2/tag4 :irc.example.com "
			     "COMMAND param1 param2 :param3 param3\r\n";
	GBytes *bytes = g_bytes_new_static (input,
					    (sizeof(input)/sizeof(*input)) - 1);
	GByteArray *array = g_bytes_unref_to_array (bytes);

	IrciumMessage *msg = ircium_message_parse (array, TRUE);
	// We take the GByteArray from above and recreate the
	// GBytes that created it, so we can do the actual test.
	bytes = g_byte_array_free_to_bytes (array);

	GBytes *serialized_bytes = ircium_message_serialize (msg);

	g_assert_true (g_bytes_equal (bytes, serialized_bytes));

	g_object_unref (msg);
}

int
main (int    argc,
      char **argv)
{
	setlocale (LC_ALL, "");

	g_test_init (&argc, &argv, NULL);

	g_test_add ("/ircium/message_parse_simple", EmptyFixture, NULL,
		    empty_fixture_set_up, message_parse_simple,
		    empty_fixture_tear_down);

	g_test_add ("/ircium/message_parse_empty_trailing", EmptyFixture, NULL,
		    empty_fixture_set_up, message_parse_empty_trailing,
		    empty_fixture_tear_down);

	g_test_add ("/ircium/message_serialize_identity", EmptyFixture, NULL,
		    empty_fixture_set_up, message_serialize_identity,
		    empty_fixture_tear_down);

	return g_test_run ();
}
