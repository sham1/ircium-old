/* test-tags.c
 *
 * Copyright 2019 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>
#include <locale.h>
#include <stdio.h>

#include "ircium-message-tag.h"

typedef struct {
} EmptyFixture;

static void
empty_fixture_set_up (EmptyFixture  *fixture,
                      gconstpointer  user_data)
{}

static void
empty_fixture_tear_down (EmptyFixture  *fixture,
                         gconstpointer  user_data)
{}

static void
tag_escape_identity_test (EmptyFixture  *fixture,
                          gconstpointer  user_data)
{
	const gchar *val = "beep \\\n";
	const gchar *expected_escape = "beep\\s\\\\\\n";

	gchar *escaped = ircium_message_tag_escape_string (val);
	g_assert_cmpstr (escaped, ==, expected_escape);

	gchar *unescaped = ircium_message_tag_unescape_string (escaped);
	g_assert_cmpstr (unescaped, ==, val);

	g_free (unescaped);
	g_free (escaped);
}

static void
tag_unescape_identity_test (EmptyFixture  *fixture,
                            gconstpointer  user_data)
{
	const gchar *val = "beep\\s\\\\\\n";
	const gchar *expected_unescape = "beep \\\n";

	gchar *unescaped = ircium_message_tag_unescape_string (val);
	g_assert_cmpstr (unescaped, ==, expected_unescape);

	gchar *escaped = ircium_message_tag_escape_string (unescaped);
	g_assert_cmpstr (escaped, ==, val);

	g_free (unescaped);
	g_free (escaped);
}

static void
tag_unescape_trailing_test (EmptyFixture  *fixture,
                            gconstpointer  user_data)
{
	const gchar *val = "test\\";
	const gchar *expected_unescape = "test";

	gchar *unescaped = ircium_message_tag_unescape_string (val);

	g_assert_cmpstr (unescaped, ==, expected_unescape);

	g_free (unescaped);
}

static void
tag_unescape_unnecessary_backslash (EmptyFixture  *fixture,
                                    gconstpointer  user_data)
{
	const gchar *val = "value\\1";
	const gchar *expected_unescape = "value1";

	gchar *unescaped = ircium_message_tag_unescape_string (val);

	g_assert_cmpstr (unescaped, ==, expected_unescape);

	g_free (unescaped);
}

static void
tag_escape_special_test (EmptyFixture  *fixture,
                         gconstpointer  user_data)
{
	const gchar *val = "; \\\r\n";
	const gchar *expected_escape = "\\:\\s\\\\\\r\\n";

	gchar *escaped = ircium_message_tag_escape_string (val);
	g_assert_cmpstr (escaped, ==, expected_escape);
	g_free (escaped);
}

int
main (int    argc,
      char **argv)
{
	setlocale (LC_ALL, "");

	g_test_init (&argc, &argv, NULL);

	g_test_add ("/ircium/tag_escape_test", EmptyFixture, NULL,
		    empty_fixture_set_up, tag_escape_identity_test,
		    empty_fixture_tear_down);

	g_test_add ("/ircium/tag_unescape_test", EmptyFixture, NULL,
		    empty_fixture_set_up, tag_unescape_identity_test,
		    empty_fixture_tear_down);

	g_test_add ("/ircium/tag_unescape_trailing_test", EmptyFixture, NULL,
		    empty_fixture_set_up, tag_unescape_trailing_test,
		    empty_fixture_tear_down);

	g_test_add ("/ircium/tag_unescape_unnecessary_backslash",
		    EmptyFixture, NULL, empty_fixture_set_up,
		    tag_unescape_unnecessary_backslash,
		    empty_fixture_tear_down);

	g_test_add ("/ircium/tag_escape_special_test", EmptyFixture, NULL,
		    empty_fixture_set_up, tag_escape_special_test,
		    empty_fixture_tear_down);

	return g_test_run ();
}
